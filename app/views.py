import csv
import hashlib
import json
import logging
import md5
import operator
import os
import pickle
from random import random, choice
import requests
import requests_toolbelt.adapters.appengine
# Use the App Engine Requests adapter. This makes sure that Requests uses
# URLFetch.
requests_toolbelt.adapters.appengine.monkeypatch()
import time
import urllib, urllib2
import zipfile
from StringIO import StringIO
import datetime
from dateutil import tz
from base64 import urlsafe_b64encode, urlsafe_b64decode
from google.appengine.api.datastore_errors import BadValueError, BadKeyError, ReferencePropertyResolveError
from google.appengine.api import memcache, taskqueue, urlfetch, mail
from google.appengine.ext.db import GqlQuery, delete, GeoPt, get, Email, Timeout, \
    Error, InternalError
from google.appengine.ext import db, deferred
from google.appengine.runtime import DeadlineExceededError
from google.appengine.runtime.apiproxy_errors import OverQuotaError
from pyramid.decorator import reify
from pyramid.httpexceptions import HTTPForbidden, HTTPFound, HTTPBadRequest
from pyramid.renderers import render_to_response, render
from pyramid.security import forget, remember, authenticated_userid, \
    has_permission, Denied
from pyramid.url import urlencode
from pyramid.view import view_config
#TODO: Removing experimental memcache that supports obj > 1Mb
#http://klenwell.com/is/AppengineMulticache
#from thirdparty import multicache as memcache
from glineenc import encode_pairs
import gviz_api
from base import BaseView
from config import CHINESESURNAMES, CONDUCTORS_GROUP, DEFAULT_CONG_NAME, DEFAULT_DNC_TEXT, \
    DEFAULT_EMAIL_BODY, DEFAULT_EMAIL_MASTER_MAP_BODY, DEFAULT_FETCH, \
    DEFAULT_LANGUAGES, DEFAULT_REGION, DEFAULT_COUNTRY, EMAIL_MASTER_MAP_BASE_URL, \
    FIELD_WIDGETS, FIELDS_NOT_TO_DISPLAY, URL_MAX_LENGTH, DEV_ADMIN_EMAIL, \
    NZTZ, SECONDVISIT, STREETTYPES
from models import FolcomCong, FolcomUser, Address, Map, MapAssignment, \
    Congregation, UploadedTextFile, TerritoryEmailUpdate, MapPageAssignment
import sys
for attr in ('stdin', 'stdout', 'stderr'):
    setattr(sys, attr, getattr(sys, '__%s__' % attr))
import pdb
import pdfrw

# Load local settings/overrides
try:
    from locals import *
except ImportError:
    pass

logger = logging.getLogger('Folcom')

class FolcomViews(BaseView):

    def __init__(self, request):
        self.request = request
    
    def can_access(self, userid):
        user = self.get_folcom_user(userid)
        if user:
            modelname = ''
            idname = ''
            id = self.request.matchdict['objectid']
            if self.request.matched_route:
                # This is a URL Dispatch page
                if self.request.matched_route.name == 'editaddress':
                    modelname = 'Address'
                    idname = 'address_id'
                elif self.request.matched_route.name == 'downloadmap':
                    modelname = 'Map'
                    idname = 'map_id'
                q = "SELECT * FROM "+modelname+" WHERE "+idname+" = :1"
                results = GqlQuery(q, id).fetch(1)
                if len(results) > 0:
                    if results[0].folcom_cong.fcong_id != user.folcom_cong.fcong_id:
                        return False
        return True

    def delete_item(self, request, modelclass, modelname, propertyname=None):
        """ 
        This method is designed to delete by either model property OR
        key TODO: redundant?
        """
        itemresults = []
        nexturl = request.referer
        if propertyname:
            # Deleting by property
            querystring = "WHERE " + propertyname + " = :1"
            itemresults = modelclass.gql(querystring, request.matchdict['objectid']).fetch(DEFAULT_FETCH)
        else:
            # Deleting by key
            itemresults = modelclass.get([request.matchdict['objectid']]) # Pass in a list so it returns a list
        if len(itemresults) > 0:
            delete(itemresults)
            querystring = {'alert': 'Successfully deleted ' + modelname,
                           'alert_class': 'alert-success'
                           }
            nexturl = self.process_query_string(nexturl, querystring)
        return nexturl

    def process_batch(self, batch_operation):
        """
        Batch process addresses via address_id
        """
        processed = 0
        for item in self.request.params.iteritems():
            # Might be more than 1 selected
            updated = False
            if item[0] == 'batch_process':
                address = Address.gql("WHERE address_id = :1", item[1]).get()
                if not address is None:
                    if batch_operation == 'delete':
                        address.delete()
                    elif batch_operation == 'archive':
                        address.visible = False
                        updated = True
                    elif batch_operation == 'sort':
                        if address.sortorder != float(self.request.params['update_batch_sortorder']):
                            address.sortorder = float(self.request.params['update_batch_sortorder'])
                            updated = True
                    elif batch_operation == 'move':
                        if address.map_number != int(self.request.params['update_batch_map_number']):
                            address.map_number = int(self.request.params['update_batch_map_number'])
                            updated = True
                    elif batch_operation == 'restore':
                        address.visible = True
                        updated = True
                    if updated:
                        address.put()
                    if updated or batch_operation == 'delete':
                        # As we don't set updated to True when deleting items
                        # we have to specifically check for this before updating
                        # counter
                        processed += 1
        return processed

    def decode_geocode_response(self, response):
        unitnumber = ''
        unitletter = ''
        streetnumber = ''
        streetletter = ''
        streetname = ''
        suburb = ''
        addressnumbers = ''
        for component in response['address_components']:
            if 'street_number' in component['types']:
                streetnumber = component['long_name']
            elif 'subpremise' in component['types']:
                unitnumber = component['long_name']
            elif 'route' in component['types']:
                streetname = component['long_name']
            elif 'sublocality' in component['types']:
                suburb = component['long_name']
        if streetnumber.find('-') > -1:
            # Fix issue where street_number contains a dash, e.g. '38A-38'
            dashidx = streetnumber.find('-')
            streetnumber = streetnumber[:dashidx]
        # Separate out the address numbers/letters
        if unitnumber != '' and streetnumber != '':
            addressnumbers = unitnumber + '/' + streetnumber
        else:
            addressnumbers = streetnumber
        unitnumber, unitletter, streetnumber, streetletter = self.get_address_numbers(addressnumbers)
        streetname, streettype = self.get_street_name_type(streetname)
        return unitnumber, unitletter, streetnumber, streetletter, streetname, streettype, suburb

    def display_batch_processed_message(self, last_batch_processed, last_batch_operation):
        if last_batch_processed > 0:
            # TODO: Currently this will only display an alert for the last
            # batch operation
            if last_batch_operation[-1] == 'e':
                last_batch_operation = last_batch_operation + 'd'
            else:
                last_batch_operation = last_batch_operation + 'ed'
            querystring = {'alert': 'Successfully %s %s addresses' % (last_batch_operation, last_batch_processed),
                           'alert_class': 'alert-success'
                           }
            nexturl = self.process_query_string(self.request.referer, querystring)
        return HTTPFound(location=nexturl)
    
    def format_date(self, date):
        if date:
            return date.strftime("%d/%m/%Y")
        else:
            return None

    def get_address_numbers(self, address):
        """
        Note to self: as we uppercase the street/unit letter
        this ensures that searches on these 2 properties
        is case-insensitive
        """
        streetletter = ''
        streetnumber = None
        unitletter = ''
        unitnumber = None
        if address.find('/') > -1:
            # Separate out the unit and street number
            streetnumberindex = address.find('/') + 1
            streetnumber = address[streetnumberindex:]
            if self.is_not_number(streetnumber[-1:]):
                streetletter = streetnumber[-1:]
                streetnumber = streetnumber[:-1]
            unitnumber = address[:streetnumberindex-1]
            if self.is_not_number(unitnumber[-1:]):
                unitletter = unitnumber[-1:]
                unitnumber = unitnumber[:-1]
        elif address != '':
            if address == '*':
                # This allows for wild-card searches
                return (unitnumber, unitletter, address, streetletter)
            streetnumber = address
            if self.is_not_number(streetnumber[-1:]):
                streetletter = streetnumber[-1:]
                streetnumber = streetnumber[:-1]
        if not unitnumber is None:
            try:
                unitnumber = int(unitnumber)
            except ValueError:
                # unitnumber got changed into a string ('')
                # when slicing up the address above so need
                # to change it back to None for GSQL queries
                # to work
                unitnumber = None
        if streetnumber:
            try:
                streetnumber = int(streetnumber)
            except ValueError:
                logging.error("Problem converting streetnumber in address %s" % address)
        if unitnumber == '':
            # Fix for bug where there is only a unit letter
            # i.e. no unit number
            unitnumber = None
        return (unitnumber, unitletter.upper(), streetnumber, streetletter.upper())

    def get_boundary_coords(self, modelname, propname):
        boundaries = []
        names = []
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        q = "SELECT * FROM " + modelname + " WHERE folcom_cong = :1"
        if modelname == 'Map':
            q += ' ORDER BY map_number'
        results = GqlQuery(q, user.folcom_cong).fetch(DEFAULT_FETCH)
        if len(results) > 0:
            for result in results:
                if getattr(result, 'boundary_coords') != []:
                    # We only include items that have a valid boundary_coords value
                    boundary = []                
                    for latlng in getattr(result, 'boundary_coords'):
                        boundary.append([latlng.lat, latlng.lon])
                    boundaries.append(boundary)
                    names.append(getattr(result, propname)) # TODO confirm if passing a number via JSON breaks the JS when doing comparisons, i.e. values on web are all strings
        return {'boundariesCoords': boundaries,
                'names': names
                }

    def get_folcom_user(self, userid):
        user = GqlQuery("SELECT * FROM FolcomUser WHERE user_id = :1", userid).get()
        return user
        # Skipping memcache stuff as is causing errors
        user = memcache.get(userid)
        if user is not None:
            return user
        else:
            user = GqlQuery("SELECT * FROM FolcomUser WHERE user_id = :1", userid).get()
            if not memcache.set(userid, user): #FIXME: Default to 30mins (1800) TODO: is this good?
                logging.error("Memcache could not store %s's Folcom user data" % userid)
            return user

    def get_folcom_cong(self, fcongid):
        fcong = FolcomCong.gql("WHERE fcong_id = :1", fcongid).get()
        if not fcong is None:
            return fcong
        return None

    def get_geocode(self, address):
        url = "https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=AIzaSyDk5qnzS8IK1aA-rb9yupA0t3uJTeXc_Aw" % urllib.quote_plus(address)
        response = json.loads(urlfetch.fetch(url).content)
        if response['status'] == 'OVER_QUERY_LIMIT':
            logging.error("Over query limit")
            return None
            time.sleep(1)
            return self.get_geocode(address)
        elif response['status'] == 'ZERO_RESULTS':
            result = None
        elif response['status'] == 'OK':
            result = response['results'][0]
        elif response['status'] == 'UNKNOWN_ERROR':
            result = None
        else:
            raise Exception(response)
        return result

    def get_latest_map_assignment(self, map, fcong):
        mapassignresults = GqlQuery("SELECT * "
                                    "FROM MapAssignment "
                                    "WHERE map = :1 "
                                    "AND folcom_cong = :2 "
                                    "ORDER BY assigned_date DESC", map, fcong).fetch(DEFAULT_FETCH)
        if len(mapassignresults) > 0:
            return mapassignresults[0]
        return None

    def get_menu_items(self, propname, menuitems):
        menu2values = [('-----------', False)]
        menu3values = [('-----------', '', False)]
        propvalue = None
        if len(menuitems) > 0:
            if self.request.matched_route:
                if self.request.matched_route.name == 'editaddress':
                    addressid = self.request.matchdict['objectid']
                    address = Address.gql("WHERE address_id = :1", addressid).get()
                    if not address is None:
                        propvalue = getattr(address, propname) # for e.g. for mapnames menu, propname = 'map_number'
                        if isinstance(menuitems[0], tuple):
                            # Values in form: field content, field value, selected
                            menu3values += [(menuitem[0], menuitem[1], menuitem[1]==propvalue) for menuitem in menuitems]
                        else:
                            menu2values += [(menuitem, menuitem==propvalue) for menuitem in menuitems]
                else:
                    if isinstance(menuitems[0], tuple):
                        # Values in form: field content, field value, selected
                        menu3values += [(menuitem[0], menuitem[1], False) for menuitem in menuitems]
                    else:
                        menu2values += [(menuitem, False) for menuitem in menuitems]
            else:
                if isinstance(menuitems[0], tuple):
                    # Values in form: field content, field value, selected
                    menu3values += [(menuitem[0], menuitem[1], False) for menuitem in menuitems]
                else:
                    menu2values += [(menuitem, False) for menuitem in menuitems]
        if len(menu3values) > 1:
            return menu3values
        elif len(menu2values) > 1:
            return menu2values
        else:
            return []

    def get_numbers(self, s):
        "Give me all the numbers in a string (value returned as string)"
        mynumber = ''
        for char in s:
            if self.is_not_number(char) is False:
                # In other words it is a number (bit confusing)
                mynumber += char
        return mynumber

    def get_python_date(self, date):
        dateparts = date.split('/')
        if len(dateparts) == 3:
            return datetime.date(int(dateparts[2]), int(dateparts[1]), int(dateparts[0]))
        else:
            return None

    def get_street_name_type(self, street_name_type):
        streetname = ''
        streettype = ''
        streetparts = street_name_type.split()
        if len(streetparts) > 0:
            if len(streetparts) == 1:
                # Some streets don't have a street type
                # as part of their name
                streettype = ''
                streetname = streetparts[0]
            elif streetparts[0] == 'The':
                # Some streets are called 'The Something'
                streettype = ''
                streetname = ' '.join(streetparts)
            else:
                streettype = streetparts.pop()
                # Fixing up the streettype data
                if streettype == 'Plc':
                    streettype = 'Pl'
                elif streettype == 'Av':
                    streettype = 'Ave'
                elif streettype == 'Drv':
                    streettype = 'Dr'
                elif streettype == 'Clse':
                    streettype = 'Cl'
                elif STREETTYPES.get(streettype, None):
                    streettype = STREETTYPES.get(streettype)
                streetname = ' '.join(streetparts)
        return streetname, streettype
   
    def is_not_number(self, s):
        try:
            int(s)
            return False
        except ValueError:
            return True

    def get_map_name_menu_items(self, folcom_cong):
        mapnameresults = GqlQuery("SELECT * FROM Map WHERE folcom_cong = :1 ORDER BY map_number", folcom_cong).fetch(DEFAULT_FETCH)
        if mapnameresults > 0:
            if folcom_cong.map_categories:
                # Adding in map categories support
                mapnames = [(str(map.map_number)+': '+map.map_name+' ('+(map.map_category and map.map_category or 'None')+')', map.map_number) for map in mapnameresults if map.visible is True]
            else:
                mapnames = [(str(map.map_number)+': '+map.map_name, map.map_number) for map in mapnameresults if map.visible is True]
            return self.get_menu_items('map_number', mapnames)
        return []

    def get_maps_checked_out(self, folcom_cong):
        mapassignments = []
        for mapname in self.get_map_name_menu_items(folcom_cong):
            if mapname[1]: # We don't want the first default value which is a boolean False
                mapresults = GqlQuery('SELECT * FROM Map WHERE folcom_cong = :1 AND map_number = :2', folcom_cong, mapname[1]).fetch(1)
                if len(mapresults) > 0:
                    mapassignmentresults = GqlQuery('SELECT * FROM MapAssignment '
                                                    'WHERE folcom_cong = :1 '
                                                    'AND map = :2 '
                                                    'ORDER BY round_number DESC', folcom_cong, mapresults[0]).fetch(1)
                    if len(mapassignmentresults) > 0:
                        if not mapassignmentresults[0].returned_date:
                            # We want maps that are currently checked out
                            mapassignments += mapassignmentresults
        if len(mapassignments) > 0:
            self.sort_by_attr_inplace(mapassignments, 'due_date')
        return mapassignments
    
    def get_maps_overdue(self, maps_checked_out):
        today = datetime.datetime.fromtimestamp(time.time(), NZTZ).date()
        maps_overdue = []
        maps_upcoming = []
        for map_assignment in maps_checked_out:
            if today >= map_assignment.due_date:
                maps_overdue.append(map_assignment)
            else:
                maps_upcoming.append(map_assignment)
        return (maps_overdue, maps_upcoming)

    def get_maps_todo(self, folcom_cong):
        mapassignments = []  
        for mapname in self.get_map_name_menu_items(folcom_cong):
            if mapname[1]: # We don't want the first default value which is a boolean False
                mapresults = GqlQuery('SELECT * FROM Map WHERE folcom_cong = :1 AND map_number = :2', folcom_cong, mapname[1]).fetch(1)
                if len(mapresults) > 0:
                    mapassignmentresults = GqlQuery('SELECT * FROM MapAssignment '
                                                    'WHERE folcom_cong = :1 '
                                                    'AND map = :2 '
                                                    'ORDER BY round_number DESC', folcom_cong, mapresults[0]).fetch(1)
                    if len(mapassignmentresults) > 0:
                        if mapassignmentresults[0].returned_date:
                            # We want maps that aren't currently checked out
                            mapassignments += mapassignmentresults
        if len(mapassignments) > 0:
            self.sort_by_attr_inplace(mapassignments, 'returned_date')
        return mapassignments

    def process_query_string(self, referer_url, query_string):
        """
        Returns a urlencoded query string
        """
        next_url = referer_url
        if referer_url.find('&alert_class') > -1:
            # Remove any existing alert message query strings
            baseurlend = referer_url.find('&alert_class')
            next_url = referer_url[:baseurlend] + '&' + urlencode(query_string)
        else:
            if referer_url.find('?') > -1:
                # Check whether we have an existing query string
                next_url = referer_url + '&' + urlencode(query_string)
            else:
                next_url = referer_url + '?' + urlencode(query_string)
        return next_url

    def shorten_url(self, url):
        json_payload = json.dumps({"longUrl": url})
        headers = {'Content-Type': 'application/json'}
        req = urllib2.Request('https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyDCVp6caqrpQSAJrne4sx5FyupRvYogB-Q', json_payload, headers)
        try:
            resp = urllib2.urlopen(req)
        except urllib2.HTTPError:
            logger.error("There was an error shortening the following url: %s" % url)
            pass
        else:
            json_resp = json.loads(resp.read())
            if json_resp.get('id', None):
                return json_resp['id']
            else:
                return None

    def sort_by_attr_inplace(self, lst, attr):
        lst.sort(key=operator.attrgetter(attr))

    def sort_dict(self, adict):
        keys = adict.keys()
        keys.sort()
        return [(key, adict[key]) for key in keys]

    def split_name(self, name, upper_first=True, upper_after_underscore=True):
        # Shamelessly plagiarised from http://code.activestate.com/recipes/440698-split-string-on-capitalizeduppercase-char/
        upper_first = True
        title = []
        for ch in name:
            if ch.isupper() and not upper_first:
                title.append(' ')
            if upper_first:
                ch = ch.upper()
                upper_first = False
            if ch == '_':
                ch = ' '
                upper_first = upper_after_underscore
            title.append(ch)
        return ''.join(title)

    @reify
    def get_conductors(self):
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        conductors = []
        if user:
            fusers = GqlQuery("SELECT * FROM FolcomUser WHERE folcom_cong = :1 "
                                "ORDER BY name", user.folcom_cong).fetch(DEFAULT_FETCH)
            if fusers > 0:
                for fuser in fusers:
                    if fuser.name:
                        if CONDUCTORS_GROUP in fuser.groups:
                            conductors.append(fuser.name)
        if len(conductors) > 0:
            conductors.insert(0, '------------------')
        return conductors
        
    @reify
    def get_cong_names(self):
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        if user:
            congnameresults = GqlQuery("SELECT * FROM Congregation WHERE folcom_cong = :1 ORDER BY cong_name", user.folcom_cong).fetch(DEFAULT_FETCH)
            if congnameresults > 0:
                congnames = [(cong.cong_name, cong.cong_code) for cong in congnameresults]
                return [('------------------', '')] + congnames
        return []
    
    @reify
    def get_languages(self):
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        if user:
            fcongresults = GqlQuery("SELECT * FROM FolcomCong WHERE fcong_id = :1", user.folcom_cong.fcong_id).fetch(DEFAULT_FETCH)
            if fcongresults > 0:
                return self.get_menu_items('language', [(lang, lang) for lang in fcongresults[0].languages])
        return []

    @reify
    def get_street_types(self):
        sortedstreettypes = self.sort_dict(STREETTYPES)
        return self.get_menu_items('street_type', sortedstreettypes)

    @reify
    def get_map_names(self):
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        if user:
            return self.get_map_name_menu_items(user.folcom_cong)
        return []
    
    @reify
    def is_business(self):
        if self.request.matched_route:
            # Only ever true if viewing 'edit_address'
            if self.request.params.get('business', None):
                # They've clicked the 'Update' button at least once
                businessvalue = self.request.params['business']
                if businessvalue == 'yes':
                    return True
                else:
                    return False
            else:
                # Have just opened up the 'edit_address' page
                addressid = self.request.matchdict['objectid']
                address = Address.gql("WHERE address_id = :1", addressid).get()
                if not address is None:
                    if address.business is True:
                        return True
                    else:
                        return False
        else:
            return False
        
    @reify
    def is_donotcall(self):
        if self.request.matched_route:
            # Only ever true if viewing 'edit_address'
            if self.request.params.get('donotcall', None):
                # They've clicked the 'Update' button at least once
                donotcallvalue = self.request.params['donotcall']
                if donotcallvalue == 'yes':
                    return True
                else:
                    return False
            else:
                # Have just opened up the 'edit_address' page
                addressid = self.request.matchdict['objectid']
                address = Address.gql("WHERE address_id = :1", addressid).get()
                if not address is None:
                    if address.do_not_call is True:
                        return True
                    else:
                        return False
        else:
            return False

    @view_config(renderer='string', name="add", permission='edit')
    def add(self):
        field_data = []
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        updated_item = False
        already_exists = False
        alert = None
        alert_class = None
        item = None
        templates_dir = 'templates/'
        template = templates_dir+'changeform.pt'
        model_name = self.request.subpath[-1]
        if model_name == 'folcom_cong':
            # Only super users can add Folcom Congs
            if not user.super_user:
                return HTTPForbidden()
        model = eval(self.split_name(model_name).replace(' ', ''))
#        return HTTPBadRequest()
        page_header = 'Add ' + self.split_name(model.kind())
        if 'form.submitted' in self.request.params:
            if model_name == 'congregation':
                item, already_exists = create_cong(user.folcom_cong,
                                                   self.request.params['cong_name'],
                                                   self.request.params['cong_code'],
                                                   self.request.params['cong_language'],
                                                   self.request.params['contact_name'],
                                                   self.request.params['contact_email'],
                                                   self.request.params['boundary_coords']
                                                   )
            elif model_name == 'map':
                item, already_exists = create_map(user.folcom_cong,
                                                  self.request.params['map_number'],
                                                  self.request.params['map_name'],
                                                  self.request.params['boundary_coords']
                                                  )
            elif model_name == 'folcom_cong':
                item, already_exists = create_fcong(self.request.params['fcong_name'],
                                                    self.request.params['languages'],
                                                    self.request.params['admin_email'],
                                                    self.request.params['region'],
                                                    self.request.params['country'],
                                                    self.request.params['round_number'],
                                                    self.request.params['other_languages'],
                                                    self.request.params['default_months_finish_map']
                                                    )
                if not already_exists:
                    # At this stage, we don't return any error if admin already exists
                    # as this will only ever be used by super users
                    admin_user, admin_exists = create_user(item,
                                                           self.request.params['admin_email'],
                                                           self.request.params['admin_password'],
                                                           'group:admin',
                                                           False
                                                           )
                    variables = {'password': self.request.params['admin_password'],
                                 'fcong_name': self.request.params['fcong_name'],
                                 }
                    rendered_template = render('templates/welcomeemail.pt', variables)
                    mail.send_mail('noreply@emergetec.com',
                                   to=self.request.params['admin_email'],
                                   bcc=[DEV_ADMIN_EMAIL],
                                   subject='%s - Welcome to Folcom!' % self.request.params['fcong_name'],
                                   body=rendered_template,
                                   html=rendered_template,
                                   )
            elif model_name == 'folcom_user':
                item, already_exists = create_user(user.folcom_cong,
                                                   self.request.params['user_id'],
                                                   self.request.params['password'],
                                                   self.request.params['groups'],
                                                   self.request.params.get('super_user', False)
                                                   )
            if already_exists is True:
                alert_class = 'alert-error'
                item_kind = self.split_name(item.kind()).lower()
                id_field = 'ID' if item_kind == 'folcom user' else 'name'
                alert = 'A %s with that %s already exists. Please enter another %s %s.' % (item_kind, id_field, item_kind, id_field)
        field_data, updated_item = self.process_form_schema(model, item, user, already_exists=already_exists)
        if item and already_exists is False:
            alert_class = 'alert-success'
            alert = 'Successfully added %s' % model.kind()
            return HTTPFound(location=self.request.application_url + '/edit/' + str(item.key()) + '?' + urlencode({'alert': alert, 'alert_class': alert_class}))
        variables = {'field_data': field_data,
                     'view': self,
                     'alert': alert,
                     'alert_class': alert_class,
                     'confirm_box_header': None,
                     'confirm_box_body': None,
                     'has_permission': has_permission,
                     'page_header': page_header,
                     'userid': userid,
                     'is_super_user': getattr(user, 'super_user', False)
                     }
        renderedtemplate = render_to_response(template, variables, request=self.request)
        return renderedtemplate
    
    @view_config(renderer='templates/addaddress.pt', name='add_address', permission='edit')
    def add_view(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        unitnumber = None
        streetnumber = None
        business = False
        donotcall = False
        region = None
        country = None
        mapnumber = None
        sortorder = None
        user = self.get_folcom_user(userid)
        mapboundaries = self.get_boundary_coords('Map', 'map_number')
        if user:
            region = user.folcom_cong.region
            country = user.folcom_cong.country
            fcong = user.folcom_cong
            if 'form.submitted' in self.request.params:
                unitletter = self.request.params.get('unitletter', '')
                streetletter = self.request.params.get('streetletter', '')
                streetname = self.request.params.get('streetname', '')
                streettype = self.request.params.get('streettype', None)
                suburb = self.request.params.get('suburb', '')
                if self.request.params.get('unitnumber', None):
                    unitnumber = int(self.request.params.get('unitnumber'))
                if self.request.params.get('streetnumber', None):
                    streetnumber = int(self.request.params.get('streetnumber'))
                if self.request.params.get('business', None) == 'yes':
                    business = True
                if self.request.params.get('donotcall', None) == 'yes':
                    donotcall = True
                if self.request.params.get('mapname', None):
                    mapnumber = int(self.request.params.get('mapname'))
                if not mapnumber is None:
                    mapaddresses = self.get_map_addresses(mapnumber, fcong)
                    sortorder = self.get_sort_order(mapaddresses, streetname, streettype)
                    if sortorder == 9999.0:
                        # Existing address on same street hasn't been properly sorted yet
                        alert = "Address created successfully. Couldn't automatically set sort order so please click on 'View Map' button below to set sort order."
                    addressexists = self.check_address_exists(mapaddresses,
                                                              unitnumber,
                                                              unitletter,
                                                              streetnumber,
                                                              streetletter,
                                                              streetname,
                                                              streettype,
                                                              suburb
                                                              )
                    if addressexists is False:
                        address = self.add_address(fcong,
                                                   unitnumber,
                                                   unitletter,
                                                   streetnumber,
                                                   streetletter,
                                                   streetname,
                                                   streettype,
                                                   suburb,
                                                   mapnumber,
                                                   self.request.params.get('language', ''),
                                                   business,
                                                   self.request.params.get('businessname', ''),
                                                   self.request.params.get('householdername', ''),
                                                   self.request.params.get('phonenumber', ''),
                                                   donotcall,
                                                   self.request.params.get('remark', ''),
                                                   True,
                                                   sortorder
                                                   )
                        # Redirect to edit view
                        if not address is None:
                            if not alert:
                                alert = 'Address created successfully'
                            querystring = {'alert': alert,
                                           'alert_class': 'alert-success'
                                           }
                            return HTTPFound(location=self.request.route_url('editaddress',
                                                                             objectid=address.address_id,
                                                                             _query=querystring)
                                                                             )
                        else:
                            alert = "Could not create address. Please confirm the address data you entered is correct and try again."
                            alert_class = 'alert-error'
                    else:
                        alert = "This address already exists. If you'd like to edit an existing address, use the 'Edit Address' button on the dashboard."
                        alert_class = 'alert-error'
        return {'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': None,
                'confirm_box_body': None,
                'country': country,
                'has_permission': has_permission,
                'mapname': None,
                'page_header': 'Add Address',
                'region': region,
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False),
                'map_boundaries': json.dumps(mapboundaries),
                }

    @view_config(renderer='templates/admin.pt', name='admin', permission='admin')
    def admin_view(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        if self.request.params.get('alert', None) and self.request.params.get('alert_class', None):
            alert = self.request.params['alert']
            alert_class = self.request.params['alert_class']
        return {'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': None,
                'confirm_box_body': None,
                'folcom_cong_id': user.folcom_cong.key(),
                'has_permission': has_permission,
                'page_header': 'Site Admin',
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False),
                }

    @view_config(renderer='templates/archived.pt', name='archived', permission='admin')
    def archived_view(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        done_batch_run, last_batch_processed, last_batch_operation = self.check_if_items_to_batch_process()
        if done_batch_run:
            return self.display_batch_processed_message(last_batch_processed, last_batch_operation)
        addressresults = Address.gql("WHERE visible = False AND folcom_cong = :1 ORDER BY timestamp DESC", user.folcom_cong).fetch(DEFAULT_FETCH)
        if self.request.params.get('alert', None) and self.request.params.get('alert_class', None):
            # Just deleted/unarchived an address
            alert = self.request.params.get('alert', None)
            alert_class = self.request.params.get('alert_class', None)
        return {'address_results': addressresults,
                'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': "Delete Address?",
                'confirm_box_body': "Are you sure you want to delete this address?",
                'has_permission': has_permission,
                'page_header': 'Archived Addresses',
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False),
                }

    @view_config(renderer="string", name="archive_item", permission='edit')
    def archive_item_view(self):
        # TODO: Confirm if user happy to archive/delete using modal popup
        querystring = None
        nexturl = self.request.referer
        item_key = self.request.subpath[-1]
        item = get(item_key)
        if not item is None:
            item.visible = False
            item.put()
            querystring = {'alert': 'Successfully archived item',
                           'alert_class': 'alert-success'
                           }
        if not querystring is None:
            nexturl = self.process_query_string(nexturl, querystring)
        return HTTPFound(location=nexturl)

    @view_config(renderer="string", route_name="archiveaddress", permission='edit')
    def archiveaddress(self):
        querystring = {'alert': 'Successfully archived address',
                       'alert_class': 'alert-success'
                       }
        if self.request.referer.find('search_address') > -1 or \
           self.request.referer.find('view_map') > -1:
            nexturl = self.process_query_string(self.request.referer, querystring)
        else:
            # Send them back to view_map page by default
            nexturl = self.request.application_url+'/view_map?' + urlencode(querystring)
        addressid = self.request.matchdict['objectid']
        addressresults = GqlQuery("SELECT * FROM Address where address_id = :1", addressid).fetch(1)
        if len(addressresults) > 0:
            address = addressresults[0]
            address.visible = False
            address.put()
        return HTTPFound(location=nexturl)

    @view_config(renderer='json', name='assign_map')
    def assign_map_view(self):
        result = ''
        updated = False
        if 'userid' in self.request.params and \
           'assignee' in self.request.params and \
           'duedate' in self.request.params and \
           'roundnumber' in self.request.params and \
           'mapname' in self.request.params:
            user = self.get_folcom_user(self.request.params.get('userid', ''))
            if user:
                assignee = self.request.params.get('assignee')
                duedate = self.request.params.get('duedate')
                duedate = self.get_python_date(duedate)
                roundnumber = self.request.params.get('roundnumber')
                mapnumber = int(self.request.params.get('mapname'))
                mapresults = GqlQuery("SELECT * FROM Map WHERE map_number = :1 AND folcom_cong = :2", mapnumber, user.folcom_cong).fetch(1)
                if len(mapresults) > 0:
                    if 'update' in self.request.params or 'returneddate' in self.request.params:
                        mapassignment = self.get_latest_map_assignment(mapresults[0], user.folcom_cong)
                        if mapassignment:
                            if 'update' in self.request.params:
                                # Updating an existing map assignment
                                for prop in [('conductor', assignee), ('due_date', duedate)]:
                                    if prop[1] != getattr(mapassignment, prop[0]):
                                        setattr(mapassignment, prop[0], prop[1])
                                        updated = True
                                if updated:
                                    mapassignment.put()
                                    result = 'Successfully updated map assignment'
                            elif 'returneddate' in self.request.params:
                                # Check that there are no map pages still checked out
                                mappages = []
                                mappages = GqlQuery('SELECT * FROM MapPageAssignment '
                                                    'WHERE folcom_cong = :1 '
                                                    'AND map_assignment = :2 '
                                                    'AND returned_date = NULL', user.folcom_cong, mapassignment).fetch(DEFAULT_FETCH)
                                if len(mappages) > 0:
                                    result = 'Map pages still checked out'
                                else:
                                    # Checking in map
                                    returneddate = self.request.params.get('returneddate', '')
                                    returneddate = self.get_python_date(returneddate)
                                    mapassignment.returned_date = returneddate
                                    mapassignment.put()
                                    result = 'Successfully checked in map'
                    else:
                        # Checking out map
                        left_to_do = Address.gql('WHERE folcom_cong = :1 '
                                                 'AND map_number = :2 '
                                                 'AND visible = True '
                                                 'AND do_not_call = False', user.folcom_cong, mapresults[0].map_number).fetch(30000, keys_only=True)
                        mapassignment = MapAssignment(folcom_cong=user.folcom_cong,
                                                      map=mapresults[0],
                                                      conductor=assignee,
                                                      due_date=duedate,
                                                      round_number=int(roundnumber),
                                                      left_to_do=len(left_to_do)
                                                      )
                        mapassignment.put()
                        result = 'Successfully assigned map'
        return {'result': result
                }

    @view_config(renderer='string', name='bulk_addresses_import')
    def bulk_addresses_import(self):
        numimported = 0
        saved_file_id = self.request.params.get('saved_file_id', None)
        if not saved_file_id is None: 
            savedfile = UploadedTextFile.get_by_id(int(saved_file_id))
            if not savedfile is None:
                addresses = eval(savedfile.data)
                lastitem = addresses.pop()
                if lastitem.get('fcong_id', None): # Can't continue if don't have fcongid
                    fcongid = lastitem.get('fcong_id')
                    fcong = self.get_folcom_cong(fcongid)
                    if not fcong is None:
                        for address in addresses:
                            mapnumber = address.get('map_number', '')
                            if mapnumber == '':
                                mapnumber = None # JSON defaults to '' and this will mess with GQL queries
                            else:
                                mapnumber = int(mapnumber)
                            unitletter = address.get('unit_letter', '')
                            unitnumber = address.get('unit_number', '')
                            if unitnumber == '':
                                unitnumber = None
                            else:
                                # Working around issue where unitnumber isn't a number, e.g. B2
                                if self.is_not_number(unitnumber):
                                    oldunitnumber = unitnumber
                                    unitnumber = self.get_numbers(unitnumber)
                                    if unitletter == '':
                                        # If there's an existing unitletter leave it alone
                                        if oldunitnumber.find(unitnumber) > -1:
                                            unitnumberidx = oldunitnumber.find(unitnumber)
                                            if unitnumberidx == 0:
                                                # Unit number is at beginning of string
                                                unitletter = oldunitnumber[len(unitnumber):]
                                            else:
                                                # Is at end of string
                                                unitletter = oldunitnumber[:unitnumberidx]
                                else:
                                    unitnumber = int(unitnumber)
                            streetnumber = address.get('street_number', '')
                            streetletter = address.get('street_letter', '')
                            if streetnumber == '':
                                streetnumber = None
                            else:
                                # Working around issue where streetnumber isn't a number, e.g. B2
                                if self.is_not_number(streetnumber):
                                    oldstreetnumber = streetnumber
                                    streetnumber = self.get_numbers(streetnumber)
                                    if streetletter == '':
                                        # If there's an existing streetletter leave it alone
                                        if oldstreetnumber.find(streetnumber) > -1:
                                            streetnumberidx = oldstreetnumber.find(streetnumber)
                                            if streetnumberidx == 0:
                                                # Street number is at beginning of string
                                                streetletter = oldstreetnumber[len(streetnumber):]
                                            else:
                                                # Is at end of string
                                                streetletter = oldstreetnumber[:streetnumberidx]
                                else:
                                    streetnumber = int(streetnumber)
                            streetname = address.get('street_name', '')
                            streettype = address.get('street_type', '')
                            if streetname == '' and streettype != '':
                                # Working around issue where street_name gets
                                # chucked in street_type variable
                                streetname, streettype = self.get_street_name_type(streettype)
                            else:
                                if streettype != '' and STREETTYPES.get(streettype, None): streettype = STREETTYPES.get(streettype)
                            suburb = address.get('suburb', '')
                            remark = address.get('surname', '')
                            if remark != '' and address.get('firstnames', None): remark += ', ' + address.get('firstnames', '')
                            if remark != '':
                                remark += '. Plz check if Chinese'
                            else:
                                remark = 'Plz check if Chinese'
                            remark = remark.title() # Capitalise the names
                            if not mapnumber is None:
                                mapaddresses = self.get_map_addresses(mapnumber, fcong)
                                sortorder = self.get_sort_order(mapaddresses, streetname, streettype)
                                addressexists = self.check_address_exists(mapaddresses,
                                                                          unitnumber,
                                                                          unitletter,
                                                                          streetnumber,
                                                                          streetletter,
                                                                          streetname,
                                                                          streettype,
                                                                          suburb
                                                                          )
                                if addressexists is False:
                                    address = self.add_address(fcong,
                                                               unitnumber,
                                                               unitletter,
                                                               streetnumber,
                                                               streetletter,
                                                               streetname,
                                                               streettype,
                                                               suburb,
                                                               mapnumber,
                                                               'Ch',
                                                               False,
                                                               '',
                                                               '',
                                                               '',
                                                               False,
                                                               remark,
                                                               True,
                                                               sortorder,
                                                               True
                                                               )
                                    if not address is None:
                                        numimported += 1
#                            else:
#                                # TODO: Experimental functionality
#                                # Add address even if we don't have a map number
#                                # These addresses need to be manually dealt with later
#                                address = self.add_address(fcong,
#                                                           unitnumber,
#                                                           unitletter,
#                                                           streetnumber,
#                                                           streetletter,
#                                                           streetname,
#                                                           streettype,
#                                                           suburb,
#                                                           mapnumber,
#                                                           'Ch',
#                                                           False,
#                                                           '',
#                                                           '',
#                                                           False,
#                                                           remark,
#                                                           True,
#                                                           9999.0,
#                                                           True
#                                                           )
#                                if not address is None:
#                                    numimported += 1
        if numimported > 0:
            logger.info('Successfully bulk imported %s addresses' % numimported)
        else:
            logger.error("Couldn't bulk import any addresses")
        return {'numimported': numimported}

    @view_config(renderer='templates/bulkimported.pt', name='bulk_imported', permission='admin')
    def bulk_imported(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
#        deleted = 0
#        if 'form.submitted' in self.request.params:
#            if self.request.params.get('batch_process', None):
#                deleted = self.process_batch('delete')
        if self.request.params.get('alert', None) and self.request.params.get('alert_class', None):
            # Just deleted/unarchived an address
            alert = self.request.params.get('alert', None)
            alert_class = self.request.params.get('alert_class', None)
#        if deleted > 0:
#            alert = 'Successfully deleted %s addresses' % deleted
#            alert_class = 'alert-success'
        address_results = Address.gql("WHERE imported = True AND visible = True AND folcom_cong = :1 ORDER BY map_number, street_name, street_number", user.folcom_cong).fetch(3000)
        return {'address_results': address_results,
                'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': "Delete Address(es)?",
                'confirm_box_body': "Are you sure you want to delete the selected address(es)?",
                'has_permission': has_permission,
                'page_header': 'Bulk Imported Addresses',
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False),
                }

    @view_config(renderer="string", route_name="deletemapassignment", permission='admin')
    def deletemapassignment_view(self):
        nexturl = self.delete_item(self.request, MapAssignment, 'map assignment')
        return HTTPFound(location=nexturl)

    @view_config(renderer="string", route_name="deleteaddress", permission='admin')
    def delete_view(self):
        nexturl = self.delete_item(self.request, Address, 'address', 'address_id')
        return HTTPFound(location=nexturl)

#    @view_config(renderer="templates/admin.pt", name='deleteall', permission='admin')
#    def deleteall_view(self):
#        alert = None
#        alert_class = None
#        userid = authenticated_userid(self.request)
#        user = self.get_folcom_user(userid)
#        try:
#            q = GqlQuery("SELECT __key__ FROM Address")
#            delete(q)
#            alert = 'Successfully deleted all addresses'
#            alert_class = 'alert-success'
#        except Exception, e:
#            error = repr(e) + '\n'
#            alert = error
#            alert_class = 'alert-error'
#        return {'alert': alert,
#                'alert_class': alert_class,
#                'confirm_box_header': None,
#                'confirm_box_body': None,
#                'has_permission': has_permission,
#                'page_header': 'Site Admin',
#                'userid': userid,
#                'is_super_user': getattr(user, 'super_user', False),
#                }

    @view_config(renderer='string', route_name="downloadmap", permission='admin')
    def downloadmap_view(self):
        alert = None
        alert_class = None
        referer = self.request.application_url + '/view_map' # Default return address to manage map page
        mapname = ''
        mapnumber = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        assignee = ''
        duedate = ''
        if not self.can_access(userid):
            return HTTPForbidden() 
        if self.request.referer:
            referer = self.request.referer
        mapid = self.request.matchdict['objectid']
        mapresults = GqlQuery("SELECT * FROM Map WHERE map_id = :1 AND folcom_cong = :2",
                              mapid, user.folcom_cong).fetch(1)
        if len(mapresults) > 0:
            mapname = mapresults[0].map_name
            mapnumber = mapresults[0].map_number
        if mapname and mapnumber:
            addressresults = GqlQuery("SELECT * FROM Address WHERE map_number = :1 AND folcom_cong = :2 AND visible = True ORDER BY sortorder, street_name, street_number, unit_number",
                                      mapnumber, user.folcom_cong).fetch(DEFAULT_FETCH)
            if len(addressresults) > 0:
                assignee = self.request.params.get('assignee', '')
                duedate = self.request.params.get('duedate', '')
                roundnumber = self.request.params.get('roundnumber', '')
                file = StringIO()
                csvfile = csv.writer(file)
                csvfile.writerow(["Map: " + mapname])
                csvfile.writerow(["Map number: " + str(mapnumber)])
                csvfile.writerow(["Checked out to: " + assignee])
                csvfile.writerow(["Finished by: " + duedate])
                csvfile.writerow(["Round number: " + roundnumber])
                csvfile.writerow(['Remark', 'Unit', 'House', 'Street', 'Suburb', 'Lang', 'Sort Order'])
                for address in addressresults:
                    unit = ''
                    house = ''
                    remark = ''
                    if address.unit_number:
                        unit += str(address.unit_number)
                    if address.unit_letter:
                        unit += address.unit_letter
                    if address.street_number:
                        house += str(address.street_number)
                    if address.street_letter:
                        house += address.street_letter
                    if address.do_not_call:
                        remark += DEFAULT_DNC_TEXT
                    if address.business and address.business_name:
                        if remark != '':
                            remark += ", B:" + address.business_name
                        else:
                            remark = "B:" + address.business_name
                    if address.remark:
                        if remark != '':
                            remark += ", " + address.remark
                        else:
                            remark = address.remark
                    csvfile.writerow([remark.encode('utf-8'),
                                      unit,
                                      house,
                                      address.street_name+" "+address.street_type,
                                      address.suburb,
                                      address.language,
                                      address.sortorder
                                      ]) 
                # send csv to browser
                now = datetime.datetime.now()
                response = self.request.response
                response.content_disposition = 'inline; filename="Map%s.csv"' % now.strftime('%d%m%Y%H%M')
                response.content_type = 'text/x-csv'
                response.content_length = file.len
                response.body = file.getvalue()
                file.close()
                return response    
        return HTTPFound(location=referer)

    @view_config(renderer='templates/editaddress.pt', route_name="editaddress", permission='edit')
    def edit_view(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        unitnumber = None
        unitletter = ''
        streetnumber = None
        streetletter = ''
        streetname = ''
        streettype = ''
        suburb = ''
        mapnumber = None
        congterritory = ''
        language = ''
        business = ''
        businessname = ''
        householdername = ''
        donotcall = ''
        remark = ''
        region = None
        country = None
        otherlanguages = None
        phonenumber = None
        addressid = self.request.matchdict['objectid']
        mapboundaries = self.get_boundary_coords('Map', 'map_number')
        if not self.can_access(userid):
            return HTTPForbidden()
        user = self.get_folcom_user(userid)
        if user:
            region = user.folcom_cong.region
            country = user.folcom_cong.country
            otherlanguages = user.folcom_cong.other_languages
            otherlanguages.sort()
            otherlanguages = ['--------------']+otherlanguages
        # Check to see if address_id exists
        address = GqlQuery("SELECT * FROM Address WHERE address_id = :1", addressid).get()
        if not address is None:
            unitnumber = address.unit_number
            unitletter = address.unit_letter
            streetnumber = address.street_number
            streetletter = address.street_letter
            streetname = address.street_name
            streettype = address.street_type
            suburb = address.suburb
            mapnumber = address.map_number
            language = address.language
            business = address.business
            businessname = address.business_name
            householdername = address.householder_name
            phonenumber = address.phone_number
            donotcall = address.do_not_call
            remark = address.remark
            if 'form.submitted' in self.request.params:
                address_modified = False
                if self.request.params['unitnumber'] != '' and unitnumber != int(self.request.params['unitnumber']):
                    # As 'unit_number' expects a number we can't insert a string into it
                    address.unit_number = unitnumber = int(self.request.params['unitnumber'])
                    address_modified = True
                if self.request.params['unitnumber'] == '':
                    # Dealing with the case where unit number has been deleted
                    address.unit_number = unitnumber = None
                    address_modified = True
                if unitletter != self.request.params['unitletter']:
                    address.unit_letter = unitletter = self.request.params['unitletter']
                    address_modified = True
                if streetnumber != int(self.request.params['streetnumber']):
                    address.street_number = streetnumber = int(self.request.params['streetnumber'])
                    address_modified = True
                if streetletter != self.request.params['streetletter']:
                    address.street_letter = streetletter = self.request.params['streetletter']
                    address_modified = True
                if streetname != self.request.params['streetname']:
                    address.street_name = streetname = self.request.params['streetname']
                    address_modified = True
                if streettype != self.request.params['streettype']:
                    address.street_type = streettype = self.request.params['streettype']
                    address_modified = True
                if suburb != self.request.params['suburb']:
                    address.suburb = suburb = self.request.params['suburb']
                    address_modified = True
                if mapnumber != int(self.request.params['mapname']):
                    address.map_number = mapnumber = int(self.request.params['mapname'])
                    address_modified = True
                if language != self.request.params['language']:
                    address.language = language = self.request.params['language']
                    address_modified = True
                if self.request.params['business'] == 'no' and business != False:
                    address.business = business = False
                    address_modified = True
                elif self.request.params['business'] == 'yes' and business != True:
                    address.business = business = True
                    address_modified = True
                if businessname != self.request.params['businessname']:
                    address.business_name = businessname = self.request.params['businessname']
                    address_modified = True
                if householdername != self.request.params['householdername']:
                    address.householder_name = householdername = self.request.params['householdername']
                    address_modified = True
                if phonenumber != self.request.params['phonenumber']:
                    address.phone_number = phonenumber = self.request.params['phonenumber']
                    address_modified = True
                if self.request.params['donotcall'] == 'no' and donotcall != False:
                    address.do_not_call = donotcall = False
                    address_modified = True
                elif self.request.params['donotcall'] == 'yes' and donotcall != True:
                    address.do_not_call = donotcall = True
                    address_modified = True
                if remark != self.request.params['remark']:
                    address.remark = remark = self.request.params['remark']
                    address_modified = True
                if address_modified:
                    
                    if not unitnumber is None or unitletter != '':
                        # Not all addresses will have a unit number/letter
                        geocode_query = '%s%s/%s%s %s %s' % (unitnumber, unitletter, streetnumber, streetletter, streetname, streettype)
                    else:
                        geocode_query = '%s%s %s %s' % (streetnumber, streetletter, streetname, streettype)
                    geocode_query = geocode_query.strip() + ', ' + suburb + ', ' + user.folcom_cong.region
                    geocode_response = self.get_geocode(geocode_query)
                    if not geocode_response is None:
                        lat = geocode_response["geometry"]["location"]["lat"]
                        lng = geocode_response["geometry"]["location"]["lng"]
                        address.geopt = GeoPt(lat=float(lat), lon=float(lng))
                        
                    address.put()
                    
                alert = 'Successfully updated the address details'
                alert_class = 'alert-success'
        else:
            alert = 'No address exists with that address id'
            alert_class = 'alert-error'
        if self.request.params.get('alert', None) and self.request.params.get('alert_class', None):
            alert = self.request.params['alert']
            alert_class = self.request.params['alert_class']
        return {'addressid': addressid,
                'alert': alert,
                'alert_class': alert_class,
                'business': business,
                'businessname': businessname,
                'confirm_box_header': "Archive Address?",
                'confirm_box_body': "Are you sure you want to archive this address?",
                'congterritory': congterritory,
                'country': country,
                'donotcall': donotcall,
                'has_permission': has_permission,
                'householdername': householdername,
                'language': language,
                'mapname': str(mapnumber),
                'other_languages': otherlanguages,
                'page_header': 'Edit Address',
                'phonenumber': phonenumber,
                'region': region,
                'remark': remark,
                'streetnumber': streetnumber,
                'streetletter': streetletter,
                'streetname': streetname,
                'streettype': streettype,
                'suburb': suburb,
                'timestamp': address and datetime.datetime.fromtimestamp(time.mktime(address.timestamp.timetuple()), NZTZ).strftime("%d/%m/%Y %H:%M") or None,
                'unitnumber': unitnumber,
                'unitletter': unitletter,
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False),
                'map_boundaries': json.dumps(mapboundaries),
                }

    @view_config(renderer='string', name="edit", permission='edit')
    def edit(self):
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        alert = None
        alert_class = None
        page_header = 'Edit Item'
        templates_dir = 'templates/'
        template = templates_dir+'changeform.pt'
        item_key = self.request.subpath[-1]
        item = None
        try:
            item = get(item_key)
        except BadKeyError:
            alert_class = 'alert-error'
            alert = "The item with that key doesn't exist"
        if not item is None:
            kind = item.kind()
            model = eval(kind)
            page_header = 'Edit ' + self.split_name(kind)
            # TODO: Get rid of the custom template checking code?
            template_name = '%sedit%s.pt' % (templates_dir, kind.lower())
            if os.path.isfile(template_name):
                template = template_name
            field_data, updated_item = self.process_form_schema(model, item, user)
            if not updated_item is False:
                item.put()
                alert_class = 'alert-success'
                alert = 'Successfully updated %s' % kind
        if self.request.params.get('alert', None) and self.request.params.get('alert_class', None):
            alert = self.request.params['alert']
            alert_class = self.request.params['alert_class']
        variables = {'field_data': field_data,
                     'view': self,
                     'alert': alert,
                     'alert_class': alert_class,
                     'confirm_box_header': None,
                     'confirm_box_body': None,
                     'has_permission': has_permission,
                     'page_header': page_header,
                     'userid': userid,
                     'is_super_user': getattr(user, 'super_user', False)
                     }
        renderedtemplate = render_to_response(template, variables, request=self.request)
        return renderedtemplate

    def process_form_schema(self, model, item, user, **kwargs):
        """
        Get field values from the object in the db and
        prepare them for display on web form. Also,
        process form after it has been submitted.
        """
        field_data = []
        updated_item = False
        already_exists = kwargs.get('already_exists', None)
        for field_properties in model.form_schema:
            dropdown_values = None
            field_name = field_properties[0]
            property_type = model.fields()[field_name].data_type
            if FIELD_WIDGETS.get(property_type, None) is None or field_name in FIELDS_NOT_TO_DISPLAY:
                # Only include the fields we specifically want
                continue
            if field_name == 'cong_language':
                dropdown_values = user.folcom_cong.other_languages
            if field_name == 'map_category':
                if user.folcom_cong.map_categories:
                    dropdown_values = user.folcom_cong.map_categories
                else:
                    # map_categories could be False as it is not required
                    # If it is False we don't display it
                    continue
            if not dropdown_values is None:
                dropdown_values.sort()
            if item is None:
                # Adding a new item so initialise default values
                if property_type == bool:
                    field_value = False
                elif property_type in [list, GeoPt]:
                    field_value = []
                else:
                    field_value = ''
            else:
                # Editing item
                if field_name == 'boundary_coords':
                    # Map/cong boundary coords
                    field_value = []
                    boundary_coords = getattr(item, field_name)
                    for coord in boundary_coords:
                        field_value.append('%s, %s' % (coord.lat, coord.lon))
                else:
                    field_value = getattr(item, field_name)
            current_value = None
            if ('form.submitted' in self.request.params and self.request.referer.find('/add/') == -1) and not already_exists is True:
                # We don't execute this code if we came from the add view as we've already saved the data
                if property_type in [list]:
                    # Transform the string back into its python data type
                    current_value = self.request.params.get(field_name, '')
                    if current_value == '':
                        current_value = [] # GAE ListProperty/StringListProperty classes must have a default value of [] 
                    else:
                        current_value = self.request.params.get(field_name, '').split('\r\n')
                elif property_type in [bool]:
                    if self.request.params.get(field_name, None) is None:
                        current_value = False
                    else:
                        current_value = True
                elif property_type in [long, int]:
                    current_value = int(self.request.params[field_name]) 
                else:
                    current_value = self.request.params.get(field_name, None)
                if field_value != current_value:
                    if field_name == 'boundary_coords':
                        new_value = []
                        for value in current_value:
                            value = value.split(',')
                            new_value.append(GeoPt(float(value[0]), float(value[1])))
                        setattr(item, field_name, new_value)
                    else:
                        setattr(item, field_name, current_value)
                    updated_item = True
            # Is in format: field_name, field_label, field_value, field_widget, required, dropdown_values
            field_data.append((field_name, field_properties[1], field_value, FIELD_WIDGETS[property_type], field_properties[2], dropdown_values))
        return field_data, updated_item

    @view_config(renderer='json', name='get_map_boundaries')
    def get_map_boundaries_view(self):
        return self.get_boundary_coords('Map', 'map_number')

    @view_config(renderer='json', name='get_cong_boundaries')
    def get_cong_boundaries_view(self):
        return self.get_boundary_coords('Congregation', 'cong_code')
    
    @view_config(renderer='json', name='get_folcomcong_names', permission='admin')
    def get_folcomcong_names(self):
        folcomcongs = GqlQuery("SELECT * FROM FolcomCong ORDER BY fcong_name").fetch(DEFAULT_FETCH)
        return [folcomcong.fcong_name for folcomcong in folcomcongs]
    
    @view_config(renderer='templates/admin.pt', name='generate_test_data', permission='admin')
    def generatetestdata_view(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        streetletters = unitletters = ['A', 'B', None, 'F', None, None, None, 'G']
        unitnumbers = [1, None, 3, None, None, 5, None, 4]
        streetname = 'Carlton'
        streettype = 'Street'
        suburb = 'Hillsborough'
        mapnumber = 5
        congterritory = 'ONHG'
        languages = user.folcom_cong.languages
        remarks = ['Catholic', 'Dog', '', 'Night-shift worker', '', 'Malaysian', 'Kiwi Chinese', '']
        businessnames = [None, 'Orchard', 'Take Aways', None, None, 'Dairy', None, None, None, None]
        sortorder = 1000.0
        for i in range(1, 101):
            # Check if already exists
            business = None
            businessname = choice(businessnames)
            if businessname:
                business = True
            streetletter = choice(streetletters)
            unitnumber = choice(unitnumbers)
            unitletter = choice(unitletters)
            addressresults = GqlQuery("SELECT * FROM Address "
                                      "WHERE street_number = :1 "
                                      "AND street_letter = :2 "
                                      "AND unit_number = :3 "
                                      "AND unit_letter = :4 "
                                      "AND street_name = :5 "
                                      "AND street_type = :6 "
                                      "AND suburb = :7 "
                                      "AND region = :8 "
                                      "AND country = :9 "
                                      "AND business = :10 "
                                      "AND business_name = :11"
                                      "AND visible = True", \
                                      i, streetletter, unitnumber, unitletter, streetname, streettype, \
                                      suburb, user.folcom_cong.region, user.folcom_cong.country, business, \
                                      businessname).fetch(1)
            if len(addressresults) == 0:
                # Doesn't exist
                addressid = generate_id('Address', 'address_id')
                address = Address(folcom_cong=user.folcom_cong,
                                  address_id=addressid,
                                  street_number=i,
                                  street_letter=streetletter,
                                  unit_number=unitnumber,
                                  unit_letter=unitletter,
                                  street_name=streetname,
                                  street_type=streettype,
                                  suburb=suburb,
                                  region=user.folcom_cong.region,
                                  country=user.folcom_cong.country,
                                  map_number=mapnumber,
                                  language=choice(languages),
                                  remark=choice(remarks),
                                  visible=True,
                                  sortorder=sortorder,
                                  business_name=businessname,
                                  business=business
                                  )
                address.put()
                sortorder = sortorder + 1
        alert = 'Successfully generated test data'
        alert_class = 'alert-success'
        return {'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': None,
                'confirm_box_body': None,
                'has_permission': has_permission,
                'page_header': 'Site Admin',
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False),
                }

    @view_config(renderer='templates/main.pt', permission='edit')
    def homepage_view(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        return {'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': None,
                'confirm_box_body': None,
                'has_permission': has_permission,
                'page_header': 'Dashboard - '+ user.folcom_cong.fcong_name,
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False),
                }

    @view_config(renderer='string', name='switch_to_other_folcomcong', permission='admin')
    def switch_to_other_folcomcong(self):
        folcomcong = self.request.params.get('folcomcong', None)
        folcomcongobj = GqlQuery("SELECT * FROM FolcomCong WHERE fcong_name = :1", folcomcong).fetch(1)
        if len(folcomcongobj) > 0:
            headers = remember(self.request, folcomcongobj[0].admin_email)
            return HTTPFound(location=self.request.application_url, headers=headers)
        return HTTPFound(location=self.request.application_url)

    @view_config(renderer='string', name='switch_to_user', permission='admin')
    def switch_to_user(self):
        user = self.request.params.get('user', None)
        userobj = GqlQuery("SELECT * FROM FolcomUser WHERE user_id = :1", user).fetch(1)
        if len(userobj) > 0:
            headers = remember(self.request, userobj[0].user_id)
            return HTTPFound(location=self.request.application_url, headers=headers)
        return HTTPFound(location=self.request.application_url)

    @view_config(renderer='string', name='delete_folcom_account', permission='admin')
    def delete_folcom_account(self):
        
        userid = self.request.params.get('userid', None)
        
        if not userid is None:
            user = GqlQuery("SELECT * FROM FolcomUser WHERE user_id = :1", userid).get()
            fcong = user.folcom_cong
            
            if fcong.fcong_name in ['Auckland East Mandarin', 'Hillcrest Mandarin', 'Chartwell Hindi', 'Hastings Mandarin']:
                
                logger.info("Can't delete {} Folcom account".format(fcong.fcong_name))
                return "Can't delete {} Folcom account".format(fcong.fcong_name)
                
            else:

                for territoryemailupdate in fcong.territoryemailupdate_set.fetch(DEFAULT_FETCH):
                    logger.info('Deleting territoryemailupdate: {}'.format(territoryemailupdate.email_subject))
                    territoryemailupdate.delete()
                for congregation in fcong.congregation_set.fetch(DEFAULT_FETCH):
                    logger.info('Deleting congregation: {}'.format(congregation.cong_name))
                    congregation.delete()
                for address in fcong.address_set.fetch(DEFAULT_FETCH):
                    logger.info('Deleting address: {}'.format(address.address_id))
                    address.delete()
                for mappageassignment in fcong.mappageassignment_set.fetch(DEFAULT_FETCH):
                    logger.info('Deleting map page assignment for: {}'.format(mappageassignment.assignee_email))
                    mappageassignment.delete()
                for mapassignment in fcong.mapassignment_set.fetch(DEFAULT_FETCH):
                    try:
                        logger.info('Deleting map assignment for: {}'.format(mapassignment.map.map_name))
                    except ReferencePropertyResolveError:
                        logger.info('Deleting map assignment')
                    mapassignment.delete()
                for map in fcong.map_set.fetch(DEFAULT_FETCH):
                    logger.info('Deleting map: {}:{}'.format(map.map_number, map.map_name))
                    map.delete()
                for folcomuser in fcong.folcomuser_set.fetch(DEFAULT_FETCH):
                    logger.info('Deleting user: {}'.format(folcomuser.user_id))
                    folcomuser.delete()
                logger.info('Deleting folcom account: {}'.format(fcong.fcong_name))
                fcong.delete()
            
        return {'status':'200 OK'}

    @view_config(renderer='string', name='check_is_super_user')
    def check_is_super_user(self):
        user = self.request.params.get('user', None)
        userobj = GqlQuery("SELECT * FROM FolcomUser WHERE user_id = :1", user).fetch(1)
        if len(userobj) > 0:
            if getattr(userobj[0], 'super_user', False) == True:
                return True
        return False

    @view_config(renderer='templates/login.pt', name='login')
    @view_config(context='pyramid.httpexceptions.HTTPForbidden',
                 renderer='templates/login.pt')
    def login_view(self):
        alert = None
        alert_class = None
        referrer = self.request.url
        login_url = self.request.application_url+'/login'
        if referrer == login_url:
            # never use the login form itself as came_from
            referrer = self.request.application_url
        came_from = self.request.params.get('came_from', referrer)
        login = u''
        password = u''
        if 'form.submitted' in self.request.params and login_url in self.request.url:
            login = self.request.params['login']
            password = self.request.params['password']
            q = GqlQuery("SELECT * FROM FolcomUser WHERE user_id = :1", login)
            results = q.fetch(1)
            if len(results) > 0:
                user = results[0]
                if user.password == password:
                    headers = remember(self.request, login, tokens=('vialoginform',))
                    if CONDUCTORS_GROUP in user.groups:
                        return HTTPFound(location=self.request.application_url+'/my_maps',
                                         headers=headers)
                    else:
                        return HTTPFound(location=came_from,
                                     headers=headers)
            alert = 'You entered an incorrect username or password. Please re-enter your username/password.'
            alert_class = 'alert-error'           
        return {'alert': alert,
                'alert_class': alert_class,
                'came_from': came_from,
                'confirm_box_header': None,
                'confirm_box_body': None,
                'has_permission': has_permission,
                'login': login,
                'page_header': None,
                'password': password,
                'userid': None,
                'is_super_user': False,
                }

    @view_config(renderer='templates/loginold.pt', name='loginold')
    def loginold_view(self):
        alert = None
        alert_class = None
        referrer = self.request.url
        login_url = self.request.application_url+'/loginold'
        if referrer == login_url:
            # never use the login form itself as came_from
            referrer = self.request.application_url
        came_from = self.request.params.get('came_from', referrer)
        login = u''
        password = u''
        if 'form.submitted' in self.request.params and login_url in self.request.url:
            login = self.request.params['login']
            password = self.request.params['password']
            q = GqlQuery("SELECT * FROM FolcomUser WHERE user_id = :1", login)
            results = q.fetch(1)
            if len(results) > 0:
                user = results[0]
                if user.password == password:
                    headers = remember(self.request, login, tokens=('vialoginform',))
                    if CONDUCTORS_GROUP in user.groups:
                        return HTTPFound(location=self.request.application_url+'/my_maps',
                                         headers=headers)
                    else:
                        return HTTPFound(location=came_from,
                                     headers=headers)
            alert = 'You entered an incorrect username or password. Please re-enter your username/password.'
            alert_class = 'alert-error'           
        return {'alert': alert,
                'alert_class': alert_class,
                'came_from': came_from,
                'confirm_box_header': None,
                'confirm_box_body': None,
                'has_permission': has_permission,
                'login': login,
                'page_header': None,
                'password': password,
                'userid': None,
                'is_super_user': False,
                }
    
    @view_config(renderer='templates/logout.pt', name='logout')
    def logout_view(self):
        alert = None
        alert_class = None
        self.request.response_headerlist = forget(self.request)
        return {'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': None,
                'confirm_box_body': None,
                'has_permission': has_permission,
                'page_header': None,
                'userid': None,
                'is_super_user': False,
                }

    @view_config(renderer='templates/mapassignments.pt', name='map_assignments', permission='admin')
    def map_assignments(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        rows_data = []
        column_headings = []
        if 'form.submitted' in self.request.params and 'mapname' in self.request.params:
            column_headings = (('Assigned To', True), ('Round Number', True), ('Date Checked Out', True), ('Date Due', True), ('Date Returned', True)) # 2nd value in list is the column view permission
            map_number = int(self.request.params['mapname'])
            selected_map = Map.gql("WHERE folcom_cong = :1 AND map_number = :2", user.folcom_cong, map_number).get()
            if not selected_map is None:
                results = MapAssignment.gql("WHERE map = :1 ORDER BY round_number", selected_map).fetch(DEFAULT_FETCH)
                for result in results:
                    # This enables us to keep the row values in order
                    rows_data.append({1: result.conductor,
                                      2: result.round_number,
                                      3: result.assigned_date,
                                      4: result.due_date,
                                      5: result.returned_date,
                                      'key': result.key(),
                                      })
        if self.request.params.get('alert', None) and self.request.params.get('alert_class', None):
            alert = self.request.params['alert']
            alert_class = self.request.params['alert_class']
        return {'alert': alert,
                'alert_class': alert_class,
                'column_headings': column_headings,
                'confirm_box_header': "Delete Map Assignment(s)?",
                'confirm_box_body': "Are you sure you want to delete the selected map assignment(s)?",
                'has_permission': has_permission,
                'is_super_user': getattr(user, 'super_user', False),
                'page_header': 'Map Assignments',
                'rows_data': rows_data,
                'userid': userid,
                'model_name': 'Map Assignment',
                }

    @view_config(renderer='templates/mastermap.pt', name='master_map', permission='admin')
    def master_map(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        map_boundaries = self.get_boundary_coords('Map', 'map_number')
        if self.request.params.get('alert', None) and self.request.params.get('alert_class', None):
            alert = self.request.params['alert']
            alert_class = self.request.params['alert_class']
        return {'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': None,
                'confirm_box_body': None,
                'email_master_map_default_body': DEFAULT_EMAIL_MASTER_MAP_BODY,
                'has_permission': has_permission,
                'is_super_user': getattr(user, 'super_user', False),
                'map_boundaries': json.dumps(map_boundaries),
                'page_header': 'Master Territory Map',
                'userid': userid,
                }

    def decode_data(self, enc_str):
        """The inverse of map_pages encoding of the data - extract map number and email address from encrypted string."""
        # Code below is based on http://fi.am/entry/urlsafe-base64-encodingdecoding-in-two-lines/
        unenc_str = urlsafe_b64decode(enc_str.encode('utf-8') + '=' * (4 - len(enc_str) % 4)) # We encode the str as UTF-8 as the value comes in as unicode and b64_decode can't accept this
        existing_hash = unenc_str[:12]
        hash = hashlib.md5(unenc_str[12:]).hexdigest()[:12]
        if hash != existing_hash:
            raise Exception("Bad hash!")
        return unenc_str[12:]

    @view_config(renderer='templates/mappage.pt', route_name='mappage')
    def map_page(self):
        alert = None
        alertclass = None
        addresses = []
        dncs = []
        encodedurl = self.request.matchdict['encodedurl']
        mappage = None
        unenc_str = self.decode_data(encodedurl)
        fcongid_mapnumber, emailaddress = unenc_str.split(':')
        fcongid = fcongid_mapnumber[:6]
        mapnumber = fcongid_mapnumber[6:]

        fcong = FolcomCong.gql('WHERE fcong_id = :1', fcongid).fetch(1)[0]        
        map = Map.gql('WHERE folcom_cong = :1 '
                      'AND map_number = :2', fcong, int(mapnumber)).fetch(1)
        mapassignments = GqlQuery('SELECT * FROM MapAssignment '
                       'WHERE folcom_cong = :1 '
                       'AND map = :2 '
                       'AND returned_date = NULL '
                       'ORDER BY assigned_date DESC', fcong, map[0]).fetch(DEFAULT_FETCH)

        if len(mapassignments) > 0:
            mapassignment = mapassignments[0]
            mappages = GqlQuery('SELECT * FROM MapPageAssignment '
                                'WHERE folcom_cong = :1 '
                                'AND map_assignment = :2 '
                                'AND assignee_email = :3 '
                                'AND returned_date = NULL', fcong, mapassignment, emailaddress).fetch(DEFAULT_FETCH)
            if len(mappages) > 0:
                mappage = mappages[0]
                for addrid in mappage.address_ids:
                    addr = Address.gql('WHERE folcom_cong = :1 '
                                       'AND address_id = :2', fcong, addrid).fetch(1)
                    if len(addr) > 0:
                        addresses.append(addr[0])
                        if addr[0].do_not_call:
                            dncs.append(addr[0].address_id)
        
                if 'check_in' in self.request.params or 'save' in self.request.params or 'add_address' in self.request.params:
                    mappageupdated = False
                    
                    for idx, addrid in enumerate(mappage.address_ids):
                        visitresult = self.request.params.get(addrid, None)
                        if visitresult:
                            if visitresult != mappage.visit_results[idx]: 
                                mappage.visit_results[idx] = visitresult
                                mappageupdated = True
                                if visitresult == 'Delete':
                                    addr = Address.gql('WHERE folcom_cong = :1 '
                                                       'AND address_id = :2', fcong, addrid).fetch(1)
                                    if len(addr) > 0:
                                        addr[0].visible = False
                                        addr[0].put()
                                
                    if 'check_in' in self.request.params:    
                        addresses = Address.gql("WHERE map_number = :1 "
                                                "AND folcom_cong = :2 "
                                                "AND visible = True "
                                                "AND do_not_call = False "
                                                "ORDER BY sortorder, street_name, street_number, unit_number", \
                                                    int(mapnumber), fcong).fetch(DEFAULT_FETCH)
                        mappages = GqlQuery('SELECT * FROM MapPageAssignment '
                                            'WHERE folcom_cong = :1 '
                                            'AND map_assignment = :2 '
                                            'ORDER BY assigned_date', fcong, mapassignment).fetch(DEFAULT_FETCH)
                        left_to_do = self.get_left_to_do(addresses, mapassignment)
                        for result in mappage.visit_results:
                            if result != 'Done' and result != 'Delete' and result != 'Left Tract':
                                left_to_do += 1
                        if mapassignment.left_to_do != left_to_do:
                            mapassignment.left_to_do = left_to_do
                            mapassignment.put()
                    
                    if 'check_in' in self.request.params:
                        mappage.returned_date = datetime.date.today()
                        mappage.put()
                        return HTTPFound(location=self.request.application_url+'/page_checked_in')

                    if mappageupdated:
                        mappage.put()
                        alert = 'Map page saved'
                        alertclass = 'alert-success'
                    
                    if 'add_address' in self.request.params:
                        return HTTPFound(location=self.request.application_url+'/a/'+encodedurl)

        return {'alert': alert,
                'alert_class': alertclass,
                'addresses': addresses,
                'checked_out_to': emailaddress,
                'country': fcong.country,
                'dncs': dncs,
                'fcongid': fcongid,
                'mappage': mappage,
                'page_header': '%s: %s' % (map[0].map_number, map[0].map_name),
                'region': fcong.region,
                }
        
    @view_config(renderer="json", name="get_last_visited_addr")
    def get_last_visited_addr(self):
        visits = []
        addrid = self.request.params['addrid']
        fcongid = self.request.params['fcongid']

        fcong = FolcomCong.gql('WHERE fcong_id = :1', fcongid).fetch(1)[0]
        
        mappageassignments = MapPageAssignment.gql('WHERE folcom_cong = :1 '
                                                   #'AND returned_date > NULL'
                                                   'ORDER BY returned_date DESC', fcong).fetch(DEFAULT_FETCH)
        
        for mappageassignment in mappageassignments:
            if mappageassignment.returned_date == None:
                continue
            if len(visits) == 3:
                break
            if addrid in mappageassignment.address_ids:
                visitresult = mappageassignment.visit_results[mappageassignment.address_ids.index(addrid)]
                visits.append([mappageassignment.returned_date.strftime("%a, %b %e, %Y"), visitresult])
                if visitresult == "Done":
                    break
                
        return {'visits': visits}


    @view_config(renderer='templates/mappagecheckedin.pt', name='page_checked_in')
    def page_checked_in(self):
        return {'alert': None,
                'alert_class': None,
                'page_header': 'Page Checked In'
                }

    @view_config(renderer='templates/mappageaddaddress.pt', route_name='mappageaddaddress')
    def mappageaddaddress(self):
        alert = None
        alertclass = None
        business = False
        businessname = ''
        encodedurl = self.request.matchdict['encodedurl']
        mappage = None
        unitnumber = None

        # TODO: Should factor out the code below into a separate function
        unenc_str = self.decode_data(encodedurl)
        fcongid_mapnumber, emailaddress = unenc_str.split(':')
        fcongid = fcongid_mapnumber[:6]
        mapnumber = int(fcongid_mapnumber[6:])

        fcong = FolcomCong.gql('WHERE fcong_id = :1', fcongid).fetch(1)[0]        
        map = Map.gql('WHERE folcom_cong = :1 '
                      'AND map_number = :2', fcong, mapnumber).fetch(1)
        mapassignments = GqlQuery('SELECT * FROM MapAssignment '
                       'WHERE folcom_cong = :1 '
                       'AND map = :2 '
                       'ORDER BY due_date DESC', fcong, map[0]).fetch(DEFAULT_FETCH)
                       # We assume most recent map assignment is checked out - otherwise couldn't get this far

        if len(mapassignments) > 0:
            mapassignment = mapassignments[0]
            mappages = GqlQuery('SELECT * FROM MapPageAssignment '
                                'WHERE folcom_cong = :1 '
                                'AND map_assignment = :2 '
                                'AND assignee_email = :3 '
                                'AND returned_date = NULL', fcong, mapassignment, emailaddress).fetch(DEFAULT_FETCH)
            if len(mappages) > 0:
                mappage = mappages[0]
                if 'form.submitted' in self.request.params:
                    unitletter = self.request.params.get('unitletter', '')
                    streetletter = self.request.params.get('streetletter', '')
                    streetname = self.request.params.get('streetname', '')
                    streettype = self.request.params.get('streettype', None)
                    suburb = self.request.params.get('suburb', '')
                    if self.request.params.get('unitnumber', None):
                        unitnumber = int(self.request.params.get('unitnumber'))
                    if self.request.params.get('streetnumber', None):
                        streetnumber = int(self.request.params.get('streetnumber'))
                    if self.request.params.get('businessname', None):
                        business = True
                        businessname = self.request.params.get('businessname')
                    mapaddresses = self.get_map_addresses(mapnumber, fcong)
                    sortorder = self.get_sort_order(mapaddresses, streetname, streettype)
                    addressexists = self.check_address_exists(mapaddresses,
                                                              unitnumber,
                                                              unitletter,
                                                              streetnumber,
                                                              streetletter,
                                                              streetname,
                                                              streettype,
                                                              suburb
                                                              )
                    if addressexists is False:
                        address = self.add_address(fcong,
                                                   unitnumber,
                                                   unitletter,
                                                   streetnumber,
                                                   streetletter,
                                                   streetname,
                                                   streettype,
                                                   suburb,
                                                   map[0].map_number,
                                                   'Ch',
                                                   business,
                                                   businessname,
                                                   '',
                                                   '',
                                                   False,
                                                   '',
                                                   True,
                                                   sortorder
                                                   )
                        return HTTPFound(location=self.request.application_url+'/added_address')
                    else:
                        alert = "This address already exists. You can go back to the map page by clicking the 'Go Back' button below"
                        alertclass = 'alert-error'
        return {'alert': alert,
                'alert_class': alertclass,
                'page_header': 'Add Address'
                }

    @view_config(renderer='templates/mappageaddedaddress.pt', name='added_address')
    def added_address(self):
        return {'alert': None,
                'alert_class': None,
                'page_header': 'Added Address'
                }

    @view_config(renderer='templates/mappages.pt', route_name='mappages', permission='can assign map page')
    def map_pages(self):
        alert = None
        alertclass = None
        checkedoutmappages = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        mapassignment = None
        mapnumber = self.request.matchdict['mapnumber']
        mapnumber = int(mapnumber)
        mappages = []
        addressestoassign = []
        startedsecondvisit = False
        
        def email_mappage_link(mapnumber, emailaddress, user):
            host_url = self.request.application_url
            data = '%s:%s' % (user.folcom_cong.fcong_id+str(mapnumber), emailaddress)
            hash = hashlib.md5(data).hexdigest()[:12]
            encodedurl = urlsafe_b64encode(hash+data).strip('=')
            if self.request.application_url in ['http://maps.emergetec.com', 'http://folcom.net', 'https://folcomapp.appspot.com']:
                host_url = 'https://folcom.net'
            mail.send_mail(sender='noreply@emergetec.com',
                           to=emailaddress,
                           #TDKbcc=DEV_ADMIN_EMAIL,
                           subject="[Folcom] Link to map page",
                           body="""Dear Publisher,

Please click on the link below to view the map page:

%s/p/%s

Don't forget to click the "Check In" button when you've finished the map page.

You can also save what you've done by clicking the "Save" button and then reopen the map page by clicking the above link again. 

Kind regards,
%s Territory Servant
""" % (self.request.application_url, encodedurl, user.folcom_cong.fcong_name))
            return

        if self.request.params.get('alert', None) and self.request.params.get('alert_class', None):
            # Just assigned a map page
            alert = self.request.params.get('alert', None)
            alertclass = self.request.params.get('alert_class', None)
        
        if 'Resend' in self.request.params.keys():
            # Request to resend map page link
            emailaddress = self.request.params['Resend']
            email_mappage_link(mapnumber, emailaddress, user)
            alert = 'Resent map page link to %s' % emailaddress
            alertclass ='alert-success'
            
        map = Map.gql('WHERE folcom_cong = :1 '
                      'AND map_number = :2', user.folcom_cong, mapnumber).fetch(1)
        mapassignments = GqlQuery('SELECT * FROM MapAssignment '
                       'WHERE folcom_cong = :1 '
                       'AND map = :2 '
                       'AND conductor = :3 '
                       'AND returned_date = NULL '
                       'ORDER BY assigned_date DESC', user.folcom_cong, map[0], user.name).fetch(DEFAULT_FETCH)

        if len(mapassignments) > 0:
            mapassignment = mapassignments[0]
            
            if 'Delete' in self.request.params.keys():
                emailaddress = self.request.params['Delete']
                mappagestodelete = MapPageAssignment.gql('WHERE map_assignment = :1 '
                                                         'AND assignee_email = :2 '
                                                         'AND returned_date = NULL', mapassignment, emailaddress).fetch(DEFAULT_FETCH)
                if mappagestodelete > 0:
                    mappagetodelete = mappagestodelete[0]  # this assumes you've only checked out 1 map page per address
                    mappagetodelete.delete()
                    
                    addresses = Address.gql("WHERE map_number = :1 "
                                            "AND folcom_cong = :2 "
                                            "AND visible = True "
                                            "AND do_not_call = False "
                                            "ORDER BY sortorder, street_name, street_number, unit_number", \
                                                mapnumber, user.folcom_cong).fetch(DEFAULT_FETCH)
                    addr_to_do = self.get_left_to_do(addresses, mapassignment)
                    mapassignment.left_to_do = addr_to_do
                    mapassignment.put()
                    
                    querystring = {'alert': 'Successfully deleted map page for %s' % emailaddress,
                                   'alert_class': 'alert-success',
                                   }
                    nexturl = self.process_query_string(self.request.current_route_url(), querystring)
                    return HTTPFound(location=nexturl)
            
            checkedoutmappages = GqlQuery('SELECT * FROM MapPageAssignment '
                                          'WHERE folcom_cong = :1 '
                                          'AND map_assignment = :2 '
                                          'AND returned_date = NULL '
                                          'ORDER BY assigned_date', user.folcom_cong, mapassignment).fetch(DEFAULT_FETCH)
            mappages = GqlQuery('SELECT * FROM MapPageAssignment '
                                'WHERE folcom_cong = :1 '
                                'AND map_assignment = :2 '
                                'ORDER BY assigned_date', user.folcom_cong, mapassignment).fetch(DEFAULT_FETCH)

            if 'form.submitted' in self.request.params:
                emailaddress = self.request.params.get('emailaddress', '')
                numaddresses = self.request.params.get('numaddresses', '')
                if emailaddress != '' and numaddresses != '':
                    numaddresses = int(numaddresses)
                    visitresults = [''] * numaddresses
                    addresses = Address.gql("WHERE map_number = :1 "
                                            "AND folcom_cong = :2 "
                                            "AND visible = True "
                                            "AND do_not_call = False "
                                            "ORDER BY sortorder, street_name, street_number, unit_number", \
                                                mapnumber, user.folcom_cong).fetch(DEFAULT_FETCH)
                    if len(mappages) == 0:
                        if numaddresses > len(addresses):
                            numaddresses = len(addresses) # TODO: Present a message indicating couldn't assign required num of addresses
                            alertclass = 'alert-error'
                            alert = "Number of requested addresses exceeds the number of addresses available to assign"
                        for address in addresses[:numaddresses]:
                            if address.timestamp < mapassignment.assigned_date:
                                addressestoassign.append(address.address_id)
                    else:
                        for mappage in mappages:
                            # Check if we've started doing 2nd round visits
                            if 'Not Home' in mappage.visit_results and not mappage.returned_date is None:
                                startedsecondvisit = True
                                break
                        if not startedsecondvisit:
                            # We get the most recent assigned address and then assign 'numaddresses' from there
                            mostrecent_mappage = mappages[-1]
                            lastassignedaddrid = mostrecent_mappage.address_ids[-1]
                            lastassignedaddresses = [addr for addr in addresses if addr.address_id == lastassignedaddrid]
                            lastassignedaddress = None
                            if len(lastassignedaddresses) > 0:
                                lastassignedaddress = lastassignedaddresses[0]
                            if lastassignedaddress:
                                lastaddridx = addresses.index(lastassignedaddress)
                                lastaddridx += 1 # We don't include the last assigned address
                            else:
                                logger.error("Can't locate address with ID:%s as was probably deleted" % lastassignedaddrid)
                            if (lastaddridx + numaddresses) <= len(addresses):
                                for address in addresses[lastaddridx:lastaddridx+numaddresses]:
                                    if address.timestamp < mapassignment.assigned_date:
                                        addressestoassign.append(address.address_id)
                            else:
                                startedsecondvisit = True
                        if startedsecondvisit: # We do an explicit check if value is True as it may've been set above
                            for idx, address in enumerate(addresses, start=1):
                                if len(addressestoassign) == numaddresses:
                                    break

                                alreadydone = False
                                secondvisit = False
                                for mappage in mappages:
                                    if mappage.returned_date is None: # Map page is still checked out
                                        if address.address_id in mappage.address_ids: # Still doing this address
                                            alreadydone = True
                                            break
                                    else: # Map page has been checked back in
                                        if address.address_id in mappage.address_ids:
                                            addridx = mappage.address_ids.index(address.address_id)
                                            if mappage.visit_results[addridx] == 'Done':
                                                alreadydone = True
                                                break
                                            elif mappage.visit_results[addridx] == 'Left Tract':
                                                # We only do houses twice
                                                alreadydone = True
                                                break
                                            elif mappage.visit_results[addridx] == 'Not Home':
                                                # Was assigned previously but not home so do again
                                                secondvisit = True
                                if alreadydone:
                                    continue # Don't want this address
                                elif secondvisit:
                                    if address.timestamp < mapassignment.assigned_date:
                                        addressestoassign.append(address.address_id)
                                        newaddridx = addressestoassign.index(address.address_id)
                                        visitresults[newaddridx] = SECONDVISIT
                                else:
                                    # Maybe a newly added address
                                    if address.timestamp < mapassignment.assigned_date:
                                        addressestoassign.append(address.address_id)             
                    
                    # Check how many addresses left to do and update property
                    if len(mappages) == 0:
                        # First map page to be checked out
                        addr_to_do = len(addresses) - len(addressestoassign)
                    else:
                        addr_to_do = self.get_left_to_do(addresses, mapassignment)
                        # Need to take off the addr we're assigning
                        addr_to_do = addr_to_do - len(addressestoassign)
                            
                    if mapassignment.left_to_do != addr_to_do:
                        mapassignment.left_to_do = addr_to_do
                        mapassignment.put()
                    
                    if len(addressestoassign) == 0:
                        alertclass = 'alert-error'
                        alert = "No addresses left to assign"
                    else:
                        del visitresults[len(addressestoassign):] # Ensure visit results is same length as our addresses
                        pageassignment = MapPageAssignment(folcom_cong=user.folcom_cong,
                                                           map_assignment=mapassignment,
                                                           assignee_email=emailaddress,
                                                           address_ids=addressestoassign,
                                                           visit_results=visitresults)
                        pageassignment.put()                        
                        email_mappage_link(mapnumber, emailaddress, user)
                        querystring = {'alert': 'Emailed link to map page to %s' % emailaddress,
                                       'alert_class': 'alert-success',
                                       }
                        nexturl = self.process_query_string(self.request.current_route_url(), querystring)
                        return HTTPFound(location=nexturl)

        return {'alert': alert,
                'alert_class': alertclass,
                'checkedoutmappages': checkedoutmappages,
                'left_to_do': mapassignment.left_to_do,
                'map_assignment': mapassignment.key().id(),
                'map_name': '%s: %s' % (map[0].map_number, map[0].map_name),
                'page_header': 'Assigned Pages',
                'user': user,
                }


    @view_config(renderer='string', name="assign_page")
    def assign_page(self):

        emailaddress = self.request.headers.get('emailaddress', None)
        addrtocheckout = self.request.headers.get('addrtocheckout', None)
        mapassignmentid = self.request.headers.get('mapassignment', None)
        
        mapassignment = MapAssignment.get_by_id(long(mapassignmentid))
        
        addrids = addrtocheckout.split(',')
        visitresults = [''] * len(addrids)
        
        
        for addrid in addrids:
            
            for mappage in mapassignment.map_pages.order('-assigned_date'):
                
                if addrid in mappage.address_ids:
                    
                    if mappage.visit_results[mappage.address_ids.index(addrid)] == 'Not Home':
                    
                        visitresults[addrids.index(addrid)] = SECONDVISIT
                        break
                        
        
        mappage = MapPageAssignment(folcom_cong=mapassignment.folcom_cong,
                                    map_assignment=mapassignment,
                                    assignee_email=emailaddress,
                                    address_ids=addrids,
                                    visit_results=visitresults
                                    )
        mappage.put()
        
        self.email_mappage(mapassignment, emailaddress)
        
        return {'status':'OK'}
    

    @view_config(renderer='string', name="delete_page")
    def delete_page(self):
        
        emailaddress = self.request.headers.get('emailaddress', None)
        addrtodelete = self.request.headers.get('addrtodelete', None)
        mapassignmentid = self.request.headers.get('mapassignment', None)
        
        mapassignment = MapAssignment.get_by_id(long(mapassignmentid))
        
        addrids = addrtodelete.split(',')

        mappages = GqlQuery('SELECT * FROM MapPageAssignment '
                            'WHERE map_assignment = :1 '
                            'ORDER BY assigned_date DESC', mapassignment).fetch(DEFAULT_FETCH)
                            
        for mappage in mappages:
            
            if ((mappage.assignee_email == emailaddress) and (mappage.address_ids == addrids)):
                mappage.delete()
                return {'status':'OK'}
            
        return {'status':'ERROR'}


    @view_config(renderer='string', name="resend_page")
    def resend_page(self):
        
        emailaddress = self.request.headers.get('emailaddress', None)
        addrtoresend = self.request.headers.get('addrtoresend', None)
        mapassignmentid = self.request.headers.get('mapassignment', None)
        
        mapassignment = MapAssignment.get_by_id(long(mapassignmentid))
        
        addrids = addrtoresend.split(',')

        mappages = GqlQuery('SELECT * FROM MapPageAssignment '
                            'WHERE map_assignment = :1 '
                            'ORDER BY assigned_date DESC', mapassignment).fetch(DEFAULT_FETCH)
                            
        for mappage in mappages:
            
            if ((mappage.assignee_email == emailaddress) and (mappage.address_ids == addrids)):
                
                self.email_mappage(mapassignment, emailaddress)
                
                return {'status':'OK'}
            
        return {'status':'ERROR'}


    def email_mappage(self, mapassignment, emailaddress):
        data = '%s:%s' % (mapassignment.folcom_cong.fcong_id+str(mapassignment.map.map_number), emailaddress)
        hash = hashlib.md5(data).hexdigest()[:12]
        encodedurl = urlsafe_b64encode(hash+data).strip('=')
        host_url = 'https://folcom.net'
        requests.post("https://api.mailgun.net/v3/folcom.net/messages",
                      auth=("api", "key-da18b426c52c0f9c39a7bb8c506fd77d"),
                      data={"from": "noreply@emergetec.com",
                            "to": [emailaddress],
                            "subject": "[Folcom] Link to map page",
                            "text": """Dear Publisher,

Please click on the link below to view the map page:

%s/p/%s

Don't forget to click the "Check In" button when you've finished the map page.

You can also save what you've done by clicking the "Save" button and then reopen the map page by clicking the above link again. 

Kind regards,
%s Territory Servant
""" % (host_url, encodedurl, mapassignment.folcom_cong.fcong_name)})
    
    @view_config(renderer='json', name="get_addresses")
    def get_addresses(self):
        
        mapassignmentid = self.request.params.get('map_assignment', None)
        mapassignment = MapAssignment.get_by_id(long(mapassignmentid))
        
        addresses = GqlQuery('SELECT * FROM Address '
                             'WHERE map_number = :1 '
                             'AND folcom_cong = :2 '
                             'AND do_not_call = False '
                             'AND visible = True '
                             'ORDER BY sortorder, street_name, street_number, unit_number', mapassignment.map.map_number, mapassignment.folcom_cong).fetch(DEFAULT_FETCH)                             
        
        addr_left_to_do = self.get_addr_left_to_do(addresses, mapassignment)
        
        return {'addresses': [addr.address_id for addr in addr_left_to_do]}
 
 
    @view_config(renderer='json', name="get_mappages")
    def get_mappages(self):
        
        mapassignmentid = self.request.params.get('map_assignment', None)
        mapassignment = MapAssignment.get_by_id(long(mapassignmentid))
        
        mappages = GqlQuery('SELECT * FROM MapPageAssignment '
                             'WHERE map_assignment = :1 '
                             'ORDER BY assigned_date', mapassignment).fetch(DEFAULT_FETCH)
        
        return {'mappages': [{'assigneddate': mappage.assigned_date.strftime("%Y/%m/%d"), 'emailaddress': mappage.assignee_email, 'addrtocheckout': mappage.address_ids} for mappage in mappages if not mappage.returned_date]}
       

    def get_addr_left_to_do(self, addresses, mapassignment):
        
        addr_left_to_do = []
        
        mappages = GqlQuery('SELECT * FROM MapPageAssignment '
                            'WHERE map_assignment = :1 '
                            'ORDER BY assigned_date DESC', mapassignment).fetch(DEFAULT_FETCH)
                             
        for addr in addresses:
            
            add_to_list = True
            
            if addr.timestamp > mapassignment.assigned_date:
                continue
            
            for mappage in mappages:
                
                    if addr.address_id in mappage.address_ids:
                    
                        if mappage.returned_date:
                        
                            visitresult = mappage.visit_results[mappage.address_ids.index(addr.address_id)]
                    
                            if visitresult == 'Done' or visitresult == 'Left Tract' or visitresult == 'Delete':
                                add_to_list = False
                                break
                            
                        else:
                                                        
                            add_to_list = False
                            break

            if add_to_list == True:
                addr_left_to_do.append(addr)
            
        return addr_left_to_do
            

    def get_left_to_do(self, addresses, mapassignment):
        
        addr_left_to_do = self.get_addr_left_to_do(addresses, mapassignment)
                
        return len(addr_left_to_do)
        

    @view_config(renderer='templates/mymaps.pt', name='my_maps', permission='can assign map page')
    def my_maps(self):
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        mymaps = []
        today = datetime.datetime.fromtimestamp(time.time(), NZTZ).date()
        mapassignments = GqlQuery('SELECT * FROM MapAssignment '
                                  'WHERE folcom_cong = :1 '
                                  'AND conductor = :2 '
                                  'ORDER BY due_date', user.folcom_cong, user.name).fetch(DEFAULT_FETCH)
        if len(mapassignments) > 0:
            for mapassignment in mapassignments:
                if not mapassignment.returned_date:
                    # We want maps that are currently checked out
                    mymaps.append((mapassignment, datetime.datetime.fromtimestamp(time.mktime(mapassignment.due_date.timetuple()), NZTZ).strftime("%d/%m/%Y")))
        return {'alert': None,
                'alert_class': None,
                'mymaps': mymaps,
                'page_header': 'My Maps',
                'today': today,
                'user': user,
                }
    
    @view_config(renderer="string", route_name="otherlanguage", permission='edit')
    def otherlanguage(self):
        alert = ''
        alertclass = 'alert-success'
        nexturl = ''
        if self.request.params.get('language', None):
            language = self.request.params['language']
            nexturl = self.request.application_url + '/search_address'
            alert = 'Successfully updated the address to be a %s address' % language
            addressid = self.request.matchdict['objectid']
            address = GqlQuery("SELECT * FROM Address where address_id = :1", addressid).get()
            if not address is None:
                address.visible = False
                if language != 'English':
                    # We don't specify a language if they're English
                    address.language = language
                address.put()
        else:
            alert = 'There was an error when attempting to file the address under another language'
            alertclass = 'alert-error'
            nexturl = self.request.referer
        querystring = {'alert': alert,
                       'alert_class': alertclass
                       }
        nexturl = self.process_query_string(nexturl, querystring)
        return HTTPFound(location=nexturl)

    @view_config(renderer='string', name='process_items', permission='edit')
    def process_items(self):
        """
        Batch archive/delete items via key.
        Also checks if user has admin permission prior to deleting items.
        """
        processed = 0
        nexturl = self.request.referer
        operation = self.request.subpath[-1]
        if operation in ['archive', 'delete']:
            for param_item in self.request.params.iteritems():
                # Might be more than 1 selected
                if param_item[0] == 'process_item':
                    item = get(param_item[1])
                    if not item is None:
                        if operation == 'archive' and item.kind() == 'Address':
                            item.visible = False
                            item.put()
                            processed += 1
                        elif operation == 'delete' and has_permission('admin', self.request.context, self.request):
                            item.delete()
                            processed += 1
        if processed > 0:
            querystring = {'alert': 'Successfully %sd %s %s(s)' % (operation, processed, self.split_name(item.kind()).lower()),
                           'alert_class': 'alert-success'
                           }
            nexturl = self.process_query_string(nexturl, querystring)
        return HTTPFound(location=nexturl)

    @view_config(renderer='string', name='queue_bulk_import')
    def queue_bulk_import(self):
        addresses = self.request.json_body
        savedfile = UploadedTextFile(data=str(addresses))
        savedfile.put()
        #deferred.defer(make_addresses_picklable(blahblah, _url='/bulk_addresses_import/')
        try:
            taskqueue.add(params={'saved_file_id': savedfile.key().id()},
                          #name='bulk_addresses_import',
                          url='/bulk_addresses_import/'
                          )
        except taskqueue.TombstonedTaskError:
            logger.error('The bulk_addresses_import task has already been queued')
        return {'status':'200 OK'}
    
    @view_config(renderer='string', name='queue_update_schema_task', permission='admin')
    def queue_update_schema_task(self):
        """Queues a task to start updating the model schema."""
        deferred.defer(update_schema_task)
        return {'status':'200 OK'}

    @view_config(renderer='templates/reports.pt', name='reports', permission='admin')
    def report_view(self):
        # TODO: memcache data
        def _update_map_coverage_row(mapcoverage, map_category, coveragemonths, mapsdone, roundnumber, row=None):
            if row:
                row[map_category] = coveragemonths
                row[map_category+'_maps_done'] = str(len(mapsdone))
                row[map_category+'_maps_done_text'] = 'Maps Done'
                return row
            else:
                mapcoverage.append({'round': str(roundnumber),
                                    map_category: coveragemonths,
                                    map_category+'_maps_done': str(len(mapsdone)),
                                    map_category+'_maps_done_text': 'Maps Done'})
                return mapcoverage
            
        def _get_map_assignments(non_biz_maps, roundnumberstart, mapcoverage=None, map_category=None):
            """
            If the site has map categories, we only process the map assignments for that category
            and update the table data accordingly. 
            """
            if mapcoverage is None:
                # No map categories
                have_map_categories = False
                mapcoverage = []
            else:
                # Have map categories
                have_map_categories = True
            mapassignments_by_round = {}
            # We only report on the non-business maps
            for non_biz_map in non_biz_maps:
                roundnumber = roundnumberstart # Need to ensure we start from first round with each map
                # Store map assignments by round number
                while roundnumber <= currentroundnumber:
                    # Maps that aren't checked back in aren't included as they haven't been completed for that round yet
                    map_assignments = non_biz_map.mapassignment_set.filter('round_number =', roundnumber).filter('returned_date !=', None).fetch(DEFAULT_FETCH)
                    if len(map_assignments) > 0:
                        if mapassignments_by_round.get(roundnumber, None): # Have we already got map_assignments for this round?
                            mapassignments_by_round[roundnumber].extend(map_assignments)
                        else:
                            mapassignments_by_round[roundnumber] = map_assignments
                    roundnumber += 1
            # Now that we've got our map assignments by round, cycle thorugh them
            # and gather the stats
            roundnumber = roundnumberstart
            while roundnumber <= currentroundnumber:
                if mapassignments_by_round.get(roundnumber, None):
                    map_assignments = mapassignments_by_round[roundnumber]
                    self.sort_by_attr_inplace(map_assignments, 'returned_date')
                    finishdate = map_assignments[-1].returned_date # Get last one as sorted ASC
                    self.sort_by_attr_inplace(map_assignments, 'assigned_date')
                    startdate = map_assignments[0].assigned_date
                    coveragemonths = (finishdate - startdate.date()).days / 7 / 4 # This assumes 4 weeks in a month
                    already_seen = set()
                    mapsdone = []
                    for mapassignment in map_assignments:
                        if mapassignment.map.map_number not in already_seen:
                            already_seen.add(mapassignment.map.map_number)
                            mapsdone.append(mapassignment)
                    if not have_map_categories:
                        mapcoverage.append(dict(round=str(roundnumber), months=coveragemonths, maps_done=str(len(mapsdone)), maps_done_text='Maps Done'))
                    else:
                        if len(mapcoverage) > 0:
                            # We've got existing values so lets see if we've already got a row for the current
                            # round number and update it if we do
                            found_row = False
                            for row in mapcoverage:
                                if row['round'] == str(roundnumber):
                                    _update_map_coverage_row(mapcoverage, map_category, coveragemonths, mapsdone, roundnumber, row)
                                    found_row = True
                                    break
                            if not found_row:
                                mapcoverage = _update_map_coverage_row(mapcoverage, map_category, coveragemonths, mapsdone, roundnumber)
                        else:
                            # No existing rows so throw our value in there
                            mapcoverage = _update_map_coverage_row(mapcoverage, map_category, coveragemonths, mapsdone, roundnumber)
                roundnumber += 1
            return mapcoverage

        alert = None
        alert_class = None
        checkedoutmappages = []
        dncresults = []
        jscode = None
        mapassignments = []
        mapcoverage = []
        maps_overdue = []
        maps_upcoming = []
        numbersbymap = []
        tosortaddresses = []
        totalmaps = None
        updatesentresults = []
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        
        if 'form.submitted' in self.request.params and 'reportname' in self.request.params:
            reportname = self.request.params['reportname']
            if reportname == 'Territory Information':
                totalmapsresults = GqlQuery('SELECT * FROM Address WHERE folcom_cong = :1 AND visible = True', user.folcom_cong).fetch(30000, keys_only=True)
                totalmaps = len(totalmapsresults)
                for i, mapname in enumerate(self.get_map_names):
                    if i != 0:
                        mapresults = GqlQuery('SELECT * FROM Address WHERE folcom_cong = :1 and map_number = :2 AND visible = True', user.folcom_cong, int(mapname[1])).fetch(DEFAULT_FETCH, keys_only=True)
                        numbersbymap.append((mapname[0], len(mapresults)))
            elif reportname == 'Maps To Do':
                mapassignments = self.get_maps_todo(user.folcom_cong)
            elif reportname == 'Maps Checked Out':
                mapassignments = self.get_maps_checked_out(user.folcom_cong)
                maps_overdue, maps_upcoming = self.get_maps_overdue(mapassignments)       
            elif reportname == 'Territory Assignment Record':
                for mapname in self.get_map_names:
                    if mapname[1]: # We don't want the first default value which is a boolean False
                        mapresults = GqlQuery('SELECT * FROM Map WHERE folcom_cong = :1 AND map_number = :2', user.folcom_cong, mapname[1]).fetch(1)
                        if len(mapresults) > 0:
                            mapassignmentresults = GqlQuery('SELECT * FROM MapAssignment '
                                                            'WHERE folcom_cong = :1 '
                                                            'AND map = :2 '
                                                            'ORDER BY round_number DESC', user.folcom_cong, mapresults[0]).fetch(DEFAULT_FETCH)
                            if len(mapassignmentresults) > 0:
                                mapassignments += mapassignmentresults
            elif reportname == 'Map Coverage':
                roundnumberstart = 1
                if user.folcom_cong.round_number:
                    currentroundnumber = user.folcom_cong.round_number
                    if currentroundnumber > 10:
                        # We only ever report on the last 10 rounds
                        roundnumberstart = currentroundnumber - 9
                table_schema = {'round': ('string', 'Round')}
                if user.folcom_cong.map_categories:
                    columns_order = ['round']
                    for map_category in user.folcom_cong.map_categories:
                        non_biz_maps = Map.gql('WHERE folcom_cong = :1 AND business_map = False AND map_category = :2', user.folcom_cong, map_category).fetch(DEFAULT_FETCH)
                        mapcoverage = _get_map_assignments(non_biz_maps, roundnumberstart, mapcoverage, map_category)
                        columns_order.append(map_category)
                        columns_order.append(map_category+'_maps_done')
                        columns_order.append(map_category+'_maps_done_text')
                        table_schema[map_category] = ('number', map_category+' maps')
                        table_schema[map_category+'_maps_done'] = ('string', map_category+' Maps Done', {'role': 'annotation'})
                        table_schema[map_category+'_maps_done_text'] = ('string', 'Maps Done Text', {'role': 'annotationText'})
                else:
                    non_biz_maps = Map.gql('WHERE folcom_cong = :1 AND business_map = False', user.folcom_cong).fetch(DEFAULT_FETCH)
                    mapcoverage = _get_map_assignments(non_biz_maps, roundnumberstart)
                    columns_order = ('round', 'months', 'maps_done', 'maps_done_text')
                    table_schema['months'] = ('number', 'Months')
                    table_schema['maps_done'] = ('string', 'Maps Done', {'role': 'annotation'})
                    table_schema['maps_done_text'] = ('string', 'Maps Done Text', {'role': 'annotationText'})
                data_table = gviz_api.DataTable(table_schema)
                data_table.LoadData(mapcoverage)
                jscode = data_table.ToJSCode("jscode_data",
                                             columns_order=columns_order,
                                             order_by="round")
            elif reportname == 'Do Not Calls':
                dncresults = GqlQuery('SELECT * FROM Address '
                                      'WHERE folcom_cong = :1 '
                                      'AND do_not_call = True '
                                      'AND visible = True '
                                      'ORDER BY map_number', user.folcom_cong).fetch(DEFAULT_FETCH)
            elif reportname == 'Territory Update Sent':
                updatesentresults = Congregation.gql('WHERE folcom_cong = :1 '
                                               'ORDER BY cong_name', user.folcom_cong).fetch(DEFAULT_FETCH)
            elif reportname == 'Addresses To Sort':
                tosortaddresses = GqlQuery('SELECT * FROM Address '
                                      'WHERE folcom_cong = :1 '
                                      'AND sortorder = :2 '
                                      'AND visible = True '
                                      'ORDER BY map_number', user.folcom_cong, 9999.0).fetch(DEFAULT_FETCH)
            elif reportname == 'Map Pages Checked Out':
                mappages = GqlQuery('SELECT * FROM MapPageAssignment '
                                              'WHERE folcom_cong = :1 '
                                              'AND returned_date = NULL '
                                              'ORDER BY assignee_email', user.folcom_cong).fetch(DEFAULT_FETCH)
                for mappage in mappages:
                    try:
                        mapnumber = mappage.map_assignment.map.map_number
                    except ReferencePropertyResolveError:
                        continue
                    else:
                        # Only include page if the associated map is still checked out
                        if mappage.map_assignment.returned_date is None:
                            data = '%s:%s' % (user.folcom_cong.fcong_id+str(mapnumber), mappage.assignee_email)
                            hash = hashlib.md5(data).hexdigest()[:12]
                            encodedurl = urlsafe_b64encode(hash+data).strip('=')
                            checkedoutmappages.append([mappage.assignee_email, mappage.map_assignment.map.map_number, self.request.application_url+'/p/'+encodedurl])
                            
        elif 'gen.s13' in self.request.params:
                
                zipstream = StringIO()
                zf = zipfile.ZipFile(zipstream, "w", zipfile.ZIP_DEFLATED)
                
                mapCount = 0
                pdfCount = 0
                
                for i, mapname in enumerate(self.get_map_names[1:]):
                    
                    if mapCount == 0:

                        template = pdfrw.PdfReader('static/s-13.pdf')
                        template.Root.AcroForm.update(pdfrw.PdfDict(NeedAppearances=pdfrw.PdfObject('true')))
                    
                    mapresults = GqlQuery('SELECT * FROM Map WHERE folcom_cong = :1 AND map_number = :2', user.folcom_cong, mapname[1]).fetch(1)
                    
                    if len(mapresults) > 0:
                        
                        for page in template.Root.Pages.Kids:
                            for annot in page.Annots:
                                if annot['/Subtype'] == '/Widget':
                                    if annot['/T']:
                                        if annot['/T'][1:-1] == "Terr_{}".format((mapCount + 1)):
                                            print "{}=={}".format(*(annot['/T'][1:-1], (mapCount + 1)))
                                            # If you want map names: annot.update(pdfrw.PdfDict(V='{}'.format((mapname[0][mapname[0].find(':'):mapname[0].find('(') - 1] if (mapname[0].find('(') != -1) else mapname[0][(mapname[0].find(':') + 1):])), Ff=pdfrw.PdfObject('false')))
                                            annot.update(pdfrw.PdfDict(V='{}'.format(mapname[1]), Ff=pdfrw.PdfObject('false')))
                        
                        mapassignmentresults = GqlQuery('SELECT * FROM MapAssignment '
                                                        'WHERE folcom_cong = :1 '
                                                        'AND map = :2 '
                                                        'ORDER BY round_number', user.folcom_cong, mapresults[0]).fetch(DEFAULT_FETCH)
                        
                        for j, ma in enumerate((mapassignmentresults[-25:] if len(mapassignmentresults) > 25 else mapassignmentresults)):
                            
                            nameIndex = 1 + (mapCount) + (j * 5) + (120 if mapCount > 4 else 0)
                            dateIndex = 1 + (mapCount * 2) + (j * 10) + (240 if mapCount > 4 else 0)

                            for page in template.Root.Pages.Kids:
                                for annot in page.Annots:
                                    if annot['/Subtype'] == '/Widget':
                                        if annot['/T']:
                                            if annot['/T'][1:-1] == "Name_%03d" % nameIndex:
                                                annot.update(pdfrw.PdfDict(V='{}'.format(ma.conductor), Ff=pdfrw.PdfObject('false')))

                            for page in template.Root.Pages.Kids:
                                for annot in page.Annots:
                                    if annot['/Subtype'] == '/Widget':
                                        if annot['/T']:
                                            if annot['/T'][1:-1] == "Date_%03d" % dateIndex:
                                                annot.update(pdfrw.PdfDict(V='{}'.format(ma.assigned_date.strftime("%d/%m/%y") if ma.assigned_date else ""), Ff=pdfrw.PdfObject('false')))                                

                            for page in template.Root.Pages.Kids:
                                for annot in page.Annots:
                                    if annot['/Subtype'] == '/Widget':
                                        if annot['/T']:
                                            if annot['/T'][1:-1] == "Date_%03d" % (dateIndex + 1):
                                                annot.update(pdfrw.PdfDict(V='{}'.format(ma.returned_date.strftime("%d/%m/%y") if ma.returned_date else ""), Ff=pdfrw.PdfObject('false')))

                        mapCount += 1
 
                    if (mapCount == 10) | (i == (len(self.get_map_names[1:]) - 1)):

                        outputStream = StringIO()
                        pdfrw.PdfWriter().write(outputStream, template)
                        
                        zf.writestr("S-13_{}.pdf".format(pdfCount), outputStream.getvalue())

                        mapCount = 0
                        pdfCount += 1

                response = self.request.response
                response.content_disposition = 'attachment; filename="s-13.zip"'
                response.content_type = 'application/zip'
                
                zf.close()
                zipstream.seek(0)
                response.body = zipstream.getvalue()

                return response
                
        return {'alert': alert,
                'alert_class': alert_class,
                'checked_out_map_pages': checkedoutmappages,
                'confirm_box_header': None,
                'confirm_box_body': None,
                'dncresults': dncresults,
                'has_permission': has_permission,
                'is_super_user': getattr(user, 'super_user', False),
                'jscode': jscode,
                'mapassignments': mapassignments,
                'mapcategories': user.folcom_cong.map_categories,
                'mapcoverage': mapcoverage,
                'maps_overdue': maps_overdue,
                'maps_upcoming': maps_upcoming,
                'numbers_by_map': numbersbymap,
                'page_header': 'Reports',
                'report_names': ['----------------',
                                 'Addresses To Sort',
                                 'Do Not Calls',
                                 'Maps Checked Out',
                                 'Map Coverage',
                                 'Map Pages Checked Out',
                                 'Maps To Do',
                                 'Territory Assignment Record',
                                 'Territory Information',
                                 'Territory Update Sent',
                                 ],
                'tosortaddresses': tosortaddresses,
                'total_maps': totalmaps,
                'updatesentresults': updatesentresults,
                'userid': userid,
                }

    @view_config(renderer='templates/emailcongupdate.pt', name='email_cong_update', permission='admin')
    def email_cong_update(self): 
        alert = None
        alert_class = None
        is_foreign_lang_cong = False
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        rows_written = 0
        bcc_admin = True
        email_subject = None
        email_body = None
        email_update_data = TerritoryEmailUpdate.gql("WHERE folcom_cong = :1", user.folcom_cong).get()
        if not email_update_data is None:
            email_subject = email_update_data.email_subject
            email_body = email_update_data.email_body
            bcc_admin = email_update_data.bcc_admin
        else:
            email_update_data = TerritoryEmailUpdate(folcom_cong=user.folcom_cong)
            email_update_data.put()
            logger.info("Created new TerritoryEmailUpdate entity")
        if (email_subject == '') or (email_subject is None):
            email_subject = '%s Territory Update' % user.folcom_cong.fcong_name
        if (email_body == '') or (email_body is None):
            email_body = DEFAULT_EMAIL_BODY
        if 'congterritory' in self.request.params:
            cong_code = self.request.params['congterritory']
            cong_data = Congregation.gql("WHERE cong_code = :1 AND folcom_cong = :2", cong_code, user.folcom_cong).get()
            if not cong_data.contact_email is None:
                if cong_data.cong_language:
                    # Is a foreign language congregation/group
                    is_foreign_lang_cong = True
                    # All foreign language addresses should be in the 'recycle bin'
                    cong_addresses = Address.gql("WHERE language = :1 AND folcom_cong = :2 AND visible = False ORDER BY street_name, street_number", cong_data.cong_language, user.folcom_cong).fetch(5000) # Hopefully no 1 cong exceeds this
                else:
                    # When sending to English congregations we don't include archived addresses
                    cong_addresses = Address.gql("WHERE cong_territory = :1 AND folcom_cong = :2 AND visible = True ORDER BY street_name, street_number", cong_code, user.folcom_cong).fetch(5000) # Hopefully no 1 cong exceeds this
                if len(cong_addresses) > 0:
                    if is_foreign_lang_cong:
                        email_update_lang = cong_data.cong_language
                        file_name = '%s addresses.csv' % cong_data.cong_language
                    else:
                        email_update_lang = 'Chinese'
                        file_name = 'Chinese DNCs.csv'
                    csv_file = StringIO()
                    csv_writer = csv.writer(csv_file, dialect='excel')
                    csv_writer.writerow(['Unit', 'Number', 'Street', 'Suburb', 'Language', 'Business Name'])
                    for cong_address in cong_addresses:
                        if not cong_address.unit_number is None:
                            unit = '%s%s' % (cong_address.unit_number, cong_address.unit_letter)
                        else:
                            unit = cong_address.unit_letter
                        number = '%s%s' % (cong_address.street_number, cong_address.street_letter) # We'll always have a street number
                        street = '%s %s' % (cong_address.street_name, cong_address.street_type)
                        suburb = cong_address.suburb
                        language = email_update_lang
                        business_name = cong_address.business_name.encode('utf-8')
                        csv_writer.writerow([unit, number, street.strip().encode('utf-8'), suburb.encode('utf-8'), language, business_name])
                        rows_written += 1
                    logger.info("Wrote %s lines to csv file" % rows_written)
                    rendered_body_text = self.request.params['email_body'] % {'contact_name': cong_data.contact_name,
                                                                      'admin_name': '%s Territory Servant' % user.folcom_cong.fcong_name
                                                                      }
                    if self.request.params.get('bcc_admin', None):
                        bcc_admin = True
                        mail.send_mail('noreply@emergetec.com',
                                       cong_data.contact_email,
                                       self.request.params['email_subject'],
                                       rendered_body_text,
                                       attachments=[(file_name, csv_file.getvalue())],
                                       bcc=[user.folcom_cong.admin_email, DEV_ADMIN_EMAIL],
                                       )
                    else:
                        bcc_admin = False
                        mail.send_mail('noreply@emergetec.com',
                                       cong_data.contact_email,
                                       self.request.params['email_subject'],
                                       rendered_body_text,
                                       attachments=[(file_name, csv_file.getvalue())],
                                       bcc=DEV_ADMIN_EMAIL,
                                       )
                    if is_foreign_lang_cong:
                        # Now we delete the foreign language addresses
                        num_addresses = len(cong_addresses)
                        delete(cong_addresses)
                        logger.info('Successfully deleted %s %s address(es)' % (num_addresses, email_update_lang))
                    alert = "Successfully sent email to %s" % cong_data.contact_email
                    alert_class = 'alert-success'
                    cong_data.last_emailed_update = datetime.date.today()
                    cong_data.put()
                    updated = False
                    if email_update_data.email_subject != self.request.params['email_subject']:
                        email_update_data.email_subject = self.request.params['email_subject']
                        updated = True
                    if email_update_data.email_body != self.request.params['email_body']:
                        email_update_data.email_body = self.request.params['email_body']
                        updated = True
                    if email_update_data.bcc_admin != bcc_admin:
                        email_update_data.bcc_admin = bcc_admin
                        updated = True
                    if updated == True:
                        email_update_data.put()
                        logger.info("Saved updated email update data")
                else:
                    alert = "We don't currently have any addresses for %s" % cong_data.cong_name
                    alert_class = 'alert-error'
            else:
                alert = "We don't currently have a contact email address for this congregation. Please enter one."
                alert_class = 'alert-error'
        return {'alert': alert,
                'alert_class': alert_class,
                'bcc_admin': bcc_admin,
                'confirm_box_header': 'Email Congregation?',
                'confirm_box_body': 'Are you sure you want to email the congregation update through?',
                'email_body': email_body,
                'email_subject': email_subject,
                'has_permission': has_permission,
                'page_header': 'Email Congregation Update',
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False)
                }

    @view_config(renderer="json", name="email_master_map", permission='admin')
    def email_master_map(self):
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        email_address = None
        map_urls = []
        if (self.request.params.get('email_address', None) and self.request.params.get('email_body', None)):
            map_url = EMAIL_MASTER_MAP_BASE_URL
            map_boundaries = self.get_boundary_coords('Map', 'map_number')
            for i, map_boundary in enumerate(map_boundaries['boundariesCoords']):
                enc_results = encode_pairs(map_boundary)
                enc_path = urllib.quote_plus(enc_results[0])
                map_boundary_url = '&path=color:0x1B22FC|weight:1|fillcolor:0xF1CBCD88|enc:%s' % enc_path
                if len(map_url) + len(map_boundary_url) > URL_MAX_LENGTH:
                    shortened_url = self.shorten_url(map_url)
                    if shortened_url:
                        map_urls.append(shortened_url)
                    map_url = EMAIL_MASTER_MAP_BASE_URL + map_boundary_url
                    if i == len(map_boundaries['boundariesCoords'])-1:
                        # If we're at the last iteration then we need to append this url too
                        shortened_url = self.shorten_url(map_url)
                        if shortened_url:
                            map_urls.append(shortened_url)
                else:
                    map_url += map_boundary_url
            email_address = self.request.params['email_address']
            email_body = self.request.params['email_body'] + "\n(Due to limitations in Google Maps the master map may be spread over a number of URLs)\n" + '\n'.join(map_urls)
            mail.send_mail('noreply@emergetec.com',
                           email_address,
                           '%s Master Territory Map' % user.folcom_cong.fcong_name,
                           email_body,
                           bcc=DEV_ADMIN_EMAIL,
                           )
            logger.info('Successfully emailed %s' % email_address)
        if email_address:
            alert = 'Successfully emailed master map to %s' % email_address
        else:
            alert = 'Successfully emailed master map'
        querystring = {'alert': alert,
                       'alert_class': 'alert-success'
                       }
        nexturl = self.request.application_url+'/master_map?' + urlencode(querystring)
        return {'location': nexturl}

    @view_config(renderer='string', name='email_weekly_report')
    def email_weekly_report(self):
        userid = authenticated_userid(self.request)
        if userid == DEV_ADMIN_EMAIL:
            testing_cron_job = True
        else:
            testing_cron_job = False
        if not 'x-appengine-cron' in self.request.headers and not testing_cron_job:
            return HTTPForbidden()
        today = datetime.datetime.fromtimestamp(time.time(), NZTZ).date()
        folcom_congs = FolcomCong.all().fetch(DEFAULT_FETCH)
        for folcom_cong in folcom_congs:
            # Delete all addresses that were archived more than 1 month ago
            addresses_to_delete = Address.gql('WHERE folcom_cong = :1 AND visible = False '
                                                    'ORDER BY timestamp', folcom_cong).fetch(DEFAULT_FETCH) # ORDER BY defaults to ASC
            if addresses_to_delete > 0:
                num_deleted = 0
                for address_to_delete in addresses_to_delete:
                    if address_to_delete.timestamp: # Some addresses don't have timestamp
                        if today > (address_to_delete.timestamp.date() + datetime.timedelta(days=30)):
                            address_to_delete.delete()
                            num_deleted += 1
                        else:
                            break # Exit as oldest archived address isn't more than 1 month old so rest will all be newer
                if num_deleted > 0:
                    logger.info('Successfully deleted %s address(es) for %s' % (num_deleted, folcom_cong.fcong_name))
                else:
                    logger.info('No archived addresses were deleted for %s' % folcom_cong.fcong_name)

            if folcom_cong.email_weekly_report:
                maps_checked_out = self.get_maps_checked_out(folcom_cong)
                maps_overdue = []
                maps_upcoming = []
                territory_updates_overdue = []
                for map_assignment in maps_checked_out:
                    if today >= map_assignment.due_date:
                        maps_overdue.append(map_assignment)
                    else:
                        maps_upcoming.append(map_assignment)
                cong_results = Congregation.gql('WHERE folcom_cong = :1 '
                                                       'ORDER BY cong_name', folcom_cong).fetch(DEFAULT_FETCH)
                for cong_result in cong_results:
                    if cong_result.cong_language:
                        if cong_result.cong_language != '':
                            # Will be a blank string if it isn't a foreign language cong
                            foreign_addresses = Address.gql('WHERE folcom_cong = :1 AND language = :2 AND visible = False',
                                                                folcom_cong, cong_result.cong_language).fetch(DEFAULT_FETCH)
                            if len(foreign_addresses) > 0:
                                territory_updates_overdue.append((cong_result, len(foreign_addresses)))
                                                                    
#                    if cong_result.last_emailed_update:
#                        if today >= (cong_result.last_emailed_update + datetime.timedelta(days=folcom_cong.email_territory_update_days)):
#                            territory_updates_overdue.append(cong_result)
#                    else:
#                        # If there is no last_emailed_update attribute, means one has never been sent
#                        territory_updates_overdue.append(cong_result)

                if maps_overdue or maps_upcoming or territory_updates_overdue:
                    totalmapsresults = GqlQuery('SELECT * FROM Address WHERE folcom_cong = :1 AND visible = True', folcom_cong).fetch(30000, keys_only=True)
                    totalmaps = len(totalmapsresults)
                    
                    tosortaddressesresults = GqlQuery('SELECT * FROM Address '
                                          'WHERE folcom_cong = :1 '
                                          'AND sortorder = :2 '
                                          'AND visible = True '
                                          'ORDER BY map_number', folcom_cong, 9999.0).fetch(DEFAULT_FETCH)
                    tosortaddresses = len(tosortaddressesresults)

                    variables = {'today': today,
                                 'maps_overdue': maps_overdue,
                                 'maps_upcoming': maps_upcoming,
                                 'territory_updates_overdue': territory_updates_overdue,
                                 'to_sort_addresses': tosortaddresses,
                                 'total_maps': totalmaps,
                                 }
                    rendered_template = render('templates/emailweeklyreport.pt', variables)
                    if testing_cron_job:
                        mail.send_mail('noreply@emergetec.com',
                                       to=DEV_ADMIN_EMAIL,
                                       subject='Folcom Weekly Report (%s)' % today.strftime('%A, %d/%m/%Y'),
                                       body=rendered_template,
                                       html=rendered_template,
                                       )
                    else:
                        if folcom_cong.extra_weekly_report_recipients:
                            mail.send_mail('noreply@emergetec.com',
                                           to=folcom_cong.admin_email,
                                           cc=folcom_cong.extra_weekly_report_recipients,
                                           subject='Folcom Weekly Report (%s)' % today.strftime('%A, %d/%m/%Y'),
                                           body=rendered_template,
                                           html=rendered_template,
                                           #TDKbcc=[DEV_ADMIN_EMAIL],
                                           )
                        else:
                            mail.send_mail('noreply@emergetec.com',
                                           to=folcom_cong.admin_email,
                                           subject='Folcom Weekly Report (%s)' % today.strftime('%A, %d/%m/%Y'),
                                           body=rendered_template,
                                           html=rendered_template,
                                           #TDKbcc=[DEV_ADMIN_EMAIL],
                                           )
                    logger.info('Weekly report email successfully sent for %s' % folcom_cong.fcong_name)
                else:
                    logger.info('No weekly report email sent for %s as no maps checked out or overdue territory updates' % folcom_cong.fcong_name)
            else:
                logger.info('Weekly report email disabled for %s' % folcom_cong.fcong_name)

            checkedoutpages = MapPageAssignment.gql("WHERE folcom_cong = :1 AND returned_date = NULL", folcom_cong).fetch(DEFAULT_FETCH)
            for checkedoutpage in checkedoutpages:
                try:
                    # We only send the reminder email if the map is still checked out 
                    if checkedoutpage.map_assignment.returned_date is None:
                        data = '%s:%s' % (folcom_cong.fcong_id+str(checkedoutpage.map_assignment.map.map_number), checkedoutpage.assignee_email)
                        hash = hashlib.md5(data).hexdigest()[:12]
                        encodedurl = urlsafe_b64encode(hash+data).strip('=')
                        recipient = checkedoutpage.assignee_email
                        if testing_cron_job:
                            recipient = DEV_ADMIN_EMAIL
                        mail.send_mail('noreply@emergetec.com',
                                       to=recipient,
                                       subject="[Folcom] Reminder to check in map page",
                                       body="""Dear Publisher,
    
    This is a reminder to please check back in your map page.
    
    Please click on the link below to view the map page:
    
    %s/p/%s
    
    Click the "Check In" button to check the page back in.
    
    Kind regards,
    %s Territory Servant
    """ % ('https://folcom.net', encodedurl, folcom_cong.fcong_name))
                        logger.info('Sent reminder to check in map page to %s' % checkedoutpage.assignee_email)
                except ReferencePropertyResolveError:
                    continue

            zipstream = StringIO()
            zf = zipfile.ZipFile(zipstream, "w", zipfile.ZIP_DEFLATED)
            
            mapresults = GqlQuery("SELECT * FROM Map where folcom_cong = :1 AND visible = True ORDER BY map_number", folcom_cong).fetch(DEFAULT_FETCH)
            
            for map in mapresults:
            
                addressresults = GqlQuery("SELECT * FROM Address WHERE map_number = :1 AND folcom_cong = :2 AND visible = True ORDER BY sortorder, street_name, street_number, unit_number",
                                          map.map_number, folcom_cong).fetch(DEFAULT_FETCH)
                if len(addressresults) > 0:
                    file = StringIO()
                    csvfile = csv.writer(file)
                    csvfile.writerow(["Map: " + map.map_name])
                    csvfile.writerow(["Map number: " + str(map.map_number)])
                    csvfile.writerow(['Remark', 'Unit', 'House', 'Street', 'Suburb', 'Lang', 'Sort Order'])
                    for address in addressresults:
                        unit = ''
                        house = ''
                        remark = ''
                        if address.unit_number:
                            unit += str(address.unit_number)
                        if address.unit_letter:
                            unit += address.unit_letter
                        if address.street_number:
                            house += str(address.street_number)
                        if address.street_letter:
                            house += address.street_letter
                        if address.do_not_call:
                            remark += DEFAULT_DNC_TEXT
                        if address.business and address.business_name:
                            if remark != '':
                                remark += ", B:" + address.business_name
                            else:
                                remark = "B:" + address.business_name
                        if address.remark:
                            if remark != '':
                                remark += ", " + address.remark
                            else:
                                remark = address.remark
                        csvfile.writerow([remark.encode('utf-8'),
                                          unit,
                                          house,
                                          address.street_name+" "+address.street_type,
                                          address.suburb,
                                          address.language,
                                          address.sortorder
                                          ])
                    csvfilename = "Map{}.csv".format(map.map_number)
                    zf.writestr(csvfilename, file.getvalue())
                    #file.close()
                    
            if len(zf.namelist()) == 0:
                zf.close()
                logger.info('No addresses to backup for {}'.format(folcom_cong.fcong_name))
            else:
                zf.close()
                zipstream.seek(0)
                zipfilename = "%s_Backup.zip" % (folcom_cong.fcong_name.replace(" ", "_"))
                recipient = folcom_cong.admin_email
                if testing_cron_job:
                    recipient = DEV_ADMIN_EMAIL
                mailgunoutput = requests.post(
                        "https://api.mailgun.net/v3/folcom.net/messages",
                        auth=("api", "key-da18b426c52c0f9c39a7bb8c506fd77d"),
                        files=[("attachment", (zipfilename, zipstream.getvalue()))],
                        data={"from": "noreply@emergetec.com",
                              "to": recipient,
                              #TDK"bcc": DEV_ADMIN_EMAIL,
                              "subject": "[Folcom] Weekly Backup",
                              "text": """Dear Folcom Admin,
      
Please find attached a zip file containing all your congregation/group's addresses. 
      
Created by Folcom"""})
                if mailgunoutput.text.find('Queued') > -1:
                    logger.info('Sent weekly backup to %s' % folcom_cong.admin_email)
                else:
                    logger.error('Failed to send weekly backup to %s' % folcom_cong.admin_email)
        
        return {'status':'200 OK'}
    
    @view_config(renderer='string', name='queue_gmaps_geocode_addr', permission='admin')
    def queue_gmaps_geocode_addr(self):
        userid = authenticated_userid(self.request)
        try:
            taskqueue.add(params={'userid': userid},
                          url='/gmaps_geocode_addr/'
                          )
        except taskqueue.TombstonedTaskError:
            logger.error('The gmaps_geocode_addr task has already been queued')
        querystring = {'alert': 'Successfully queued  gmaps_geocode_addr task. Please check the logs to confirm whether the task was successful or not',
                       'alert_class': 'alert-success'
                       }
        nexturl = self.request.application_url + '/admin?' + urlencode(querystring)
        return HTTPFound(location=nexturl)

    @view_config(renderer='templates/searchaddress.pt', name='search_address', permission='edit')
    def search_view(self):
        alert = None
        alert_class = None
        streetaddress = ''
        unitnumber = None
        unitletter = ''
        streetnumber = None
        streetletter = ''
        streetname = ''
        streettype = ''
        suburb = ''
        addressresults = []
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        if 'form.submitted' in self.request.params:
            if self.request.params.get('addresstosearch', None):
                searchaddress = self.request.params['addresstosearch']
                if searchaddress.find(',') == -1:
                    # They didn't enter a suburb
                    streetaddress = searchaddress
                else:
                    # They entered a suburb
                    commaindex = searchaddress.find(',')
                    suburb = searchaddress[commaindex+2:]
                    streetaddress = searchaddress[:commaindex]
                streetaddressparts = streetaddress.split()
                if len(streetaddressparts) < 2:
                    # Some street names might have more than 1 word in their name
                    # Also some street names don't have a street type
                    alert = "Searches must be in the form: '4 First Street'. Please re-enter your search and click the Search button again."
                    alert_class = 'alert-error'
                elif len(streetaddressparts) >= 2:
                    if len(streetaddressparts) == 2:
                        # Doesn't have a street type
                        streettype = ''
                        unitnumber, unitletter, streetnumber, streetletter = self.get_address_numbers(streetaddressparts.pop(0))
                        streetname = streetaddressparts[0]
                    else:
                        unitnumber, unitletter, streetnumber, streetletter = self.get_address_numbers(streetaddressparts.pop(0))
                        streettype = streetaddressparts.pop()
                        if streettype not in STREETTYPES.values(): # this means users can enter the abbreviated streettype
                            streettype = STREETTYPES.get(streettype, streettype) # or the full name - clever huh :)
                        streetname = ' '.join(streetaddressparts)
                    if suburb != '':
                        if streetnumber == '*':
                            # User is doing a wild-card search
                            addressresults = GqlQuery("SELECT * FROM Address "
                                                      "WHERE street_name = :1 "
                                                      "AND street_type = :2 "
                                                      "AND suburb = :3 "
                                                      "AND region = :4 "
                                                      "AND country = :5 "
                                                      "AND visible = True "
                                                      "ORDER BY street_name, street_number, unit_number", \
                                                      streetname, streettype, suburb, user.folcom_cong.region, user.folcom_cong.country).fetch(DEFAULT_FETCH)
                        else:
                            addressresults = GqlQuery("SELECT * FROM Address "
                                                      "WHERE unit_number = :1 "
                                                      "AND unit_letter = :2 "
                                                      "AND street_number = :3 "
                                                      "AND street_letter = :4 "
                                                      "AND street_name = :5 "
                                                      "AND street_type = :6 "
                                                      "AND suburb = :7 "
                                                      "AND region = :8 "
                                                      "AND country = :9 "
                                                      "AND visible = True "
                                                      "ORDER BY street_name, street_number, unit_number", \
                                                      unitnumber, unitletter, streetnumber, streetletter, streetname, streettype, suburb, user.folcom_cong.region, user.folcom_cong.country).fetch(10)
                    else:
                        if streetnumber == '*':
                            # User is doing a wild-card search
                            addressresults = GqlQuery("SELECT * FROM Address "
                                                      "WHERE street_name = :1 "
                                                      "AND street_type = :2 "
                                                      "AND region = :3 "
                                                      "AND country = :4 "
                                                      "AND visible = True "
                                                      "ORDER BY street_name, street_number, unit_number", \
                                                      streetname, streettype, user.folcom_cong.region, user.folcom_cong.country).fetch(DEFAULT_FETCH)
                        else:
                            addressresults = GqlQuery("SELECT * FROM Address "
                                                      "WHERE unit_number = :1 "
                                                      "AND unit_letter = :2 "
                                                      "AND street_number = :3 "
                                                      "AND street_letter = :4 "
                                                      "AND street_name = :5 "
                                                      "AND street_type = :6 "
                                                      "AND region = :7 "
                                                      "AND country = :8 "
                                                      "AND visible = True "
                                                      "ORDER BY street_name, street_number, unit_number", \
                                                      unitnumber, unitletter, streetnumber, streetletter, streetname, streettype, user.folcom_cong.region, user.folcom_cong.country).fetch(10)
        if self.request.params.get('alert', None) and self.request.params.get('alert_class', None):
            # Just archived an address
            alert = self.request.params.get('alert', None)
            alert_class = self.request.params.get('alert_class', None)
        return {'address_results': addressresults,
                'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': "Archive Address?",
                'confirm_box_body': "Are you sure you want to archive this address?",
                'has_permission': has_permission,
                'page_header': 'Search For Address',
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False),
                }
    
    @view_config(renderer="string", name="gmaps_geocode_addr")
    def gmaps_geocode_addr_view(self):
        #today = datetime.datetime.fromtimestamp(time.time(), NZTZ).date()
        numupdated = 0
        if self.request.params.get('userid', None):
            userid = self.request.params['userid']
            user = self.get_folcom_user(userid)
#             maps = Map.gql("WHERE folcom_cong = :1", user.folcom_cong).fetch(DEFAULT_FETCH, keys_only=True)
#             nummaps = len(maps)
#             for mapnumber in range(1, nummaps+1):
#                addressresults = Address.gql("WHERE map_number = :1 AND folcom_cong = :2 and visible = True and timestamp < DATETIME(2017,05,27)", int(mapnumber), user.folcom_cong).fetch(DEFAULT_FETCH)
            addressresults = GqlQuery("SELECT * FROM Address where visible = True and geopt = NULL AND folcom_cong = :1", user.folcom_cong).fetch(DEFAULT_FETCH)
            if len(addressresults) > 0:
                for addr in addressresults:
                    if not addr.unit_number is None or addr.unit_letter != '':
                        # Not all addresses will have a unit number/letter
                        geocode_query = '%s%s/%s%s %s %s' % (addr.unit_number, addr.unit_letter, addr.street_number, addr.street_letter, addr.street_name, addr.street_type)
                    else:
                        geocode_query = '%s%s %s %s' % (addr.street_number, addr.street_letter, addr.street_name, addr.street_type)
                    geocode_query = geocode_query.strip() + ', ' + addr.suburb + ', ' + addr.region
                    geocode_response = self.get_geocode(geocode_query)
                    if not geocode_response is None:
                        lat = geocode_response["geometry"]["location"]["lat"]
                        lng = geocode_response["geometry"]["location"]["lng"]
                        addr.geopt = GeoPt(lat=float(lat), lon=float(lng))
                        addr.put()
                        logger.info('Updated geopt for address: %s' % addr.address_id)
                        numupdated += 1
        if numupdated > 0:
            logger.info('Successfully updated %s addresses' % numupdated)
        else:
            logger.info('No addresses were updated')
        return {'status': '200 OK'}

    @view_config(renderer="string", route_name="unarchiveaddress", permission='admin')
    def unarchive_view(self):
        querystring = {'alert': 'Successfully unarchived address',
                       'alert_class': 'alert-success'
                       }
        nexturl = self.process_query_string(self.request.referer, querystring)
        addressid = self.request.matchdict['objectid']
        addressresults = GqlQuery("SELECT * FROM Address where address_id = :1", addressid).fetch(1)
        if len(addressresults) > 0:
            address = addressresults[0]
            address.visible = True
            address.put()
        return HTTPFound(location=nexturl)
    
    @view_config(renderer='templates/admin.pt', name='update_app', permission='admin')
    def update_app(self):
        alert = 'Successfully updated Folcom'
        alert_class = 'alert-success'
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        
        processed = 0
        to_put = []

#         if user.user_id != DEV_ADMIN_EMAIL:
#             raise Exception('You are not authorised to do this.')
#         else:
        mappages = GqlQuery("select * from MapAssignment where folcom_cong = :1 AND returned_date = '2018-12-20' and round_number = 1", user.folcom_cong).fetch(DEFAULT_FETCH)
#         for addr in mappages:
#             if processed == 100:
#                 break
#             if not addr.unit_number is None or addr.unit_letter != '':
#                 # Not all addresses will have a unit number/letter
#                 geocode_query = '%s%s/%s%s %s %s' % (addr.unit_number, addr.unit_letter, addr.street_number, addr.street_letter, addr.street_name, addr.street_type)
#             else:
#                 geocode_query = '%s%s %s %s' % (addr.street_number, addr.street_letter, addr.street_name, addr.street_type)
#             geocode_query = geocode_query.strip() + ', ' + addr.suburb + ', ' + addr.region
#             geocode_response = self.get_geocode(geocode_query)
#             if not geocode_response is None:
#                 lat = geocode_response["geometry"]["location"]["lat"]
#                 lng = geocode_response["geometry"]["location"]["lng"]
#                 addr.geopt = GeoPt(lat=float(lat), lon=float(lng))
#                 to_put.append(addr)
#                 processed += 1
        
#        alert = alert + ', updated %s address(es)' % processed
        
        #db.put(to_put)
        
        return {'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': None,
                'confirm_box_body': None,
                'folcom_cong_id': user.folcom_cong.key(),
                'has_permission': has_permission,
                'page_header': 'Site Admin',
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False)
                }

    @view_config(renderer='json', name='update_sort_order')
    def update_sort_order_view(self):
        # TODO: Add in security (i.e. lookup username, etc)
        result = ''
        alert = ''
        alertclass = ''
        if 'addressid' in self.request.params and 'sortorder' in self.request.params:
            addressresults = GqlQuery('SELECT * FROM Address WHERE address_id = :1', self.request.params['addressid']).fetch(DEFAULT_FETCH)
            if len(addressresults) > 0:
                address = addressresults[0]
                sortorder = self.request.params['sortorder']
                try:
                    sortorder = float(sortorder)
                except ValueError:
                    alert = 'You must enter a numeric value in the sort order field'
                    alertclass = 'alert-error'
                else:
                    if address.sortorder != sortorder:
                        address.sortorder = sortorder
                        address.put()
                        result = 'Success'
                        alert = 'Successfully updated sort order'
                        alertclass = 'alert-success'
        return {'result': result,
                'alert': alert,
                'alert_class': alertclass
                }

    @view_config(renderer="templates/uploadcsv.pt", name='upload_csv', permission='admin')
    def upload_csv(self):
        def get_business_name(remark):
            if remark.find('B:') > -1:
                businessnamestart = remark.find('B:') + 2
                businessname = remark[businessnamestart:]
                return (True, businessname)
            return (False, remark)

        def is_dnc(remark):
            if remark.lower().find('do not call') > -1:
                remark = remark.replace('DO NOT CALL', '')
                return (True, remark.strip())
            return (False, remark)
        
        def get_remark(remark):
            if remark != '':
                dnc, remark = is_dnc(remark)
                isbiz, businessname = get_business_name(remark)
                if isbiz:
                    remark = remark.replace('B:'+businessname, '')
                    remark = remark.strip()
                return remark
            return None
        
        filereader = []
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        possibleduplicateaddresses = []
        foo = None # We use this to notify the user which address(es) errored
        if 'form.submitted' in self.request.params:
            numimported = 0
            csvfile = self.request.POST['csvfile'].file
            filereader = csv.reader(csvfile)
            if self.request.params.get('mapname') == 'Assignment Sheet':
                # Import assignment sheet
                mapassignments = {}
                roundnumber = 1
                for i, row in enumerate(filereader):
                    # Format of dict is:
                    # 'RoundNumber': [Conductor, Date Started, Date Due, Date Finished]
                    # E.g. 1: [Tosh, 1/1/2012, 1/3/2012, 10/4/2012]
                    if i == 0:
                        cell = 0
                        while cell < 152:
                            mapnumber = row[cell].replace('Map ', '')
                            mapnumber = int(mapnumber)
                            mapassignments[mapnumber] = {}
                            cell += 4
                    elif i in [7, 13, 16, 19, 22]:
                        # Conductor
                        mapnumber = 1
                        cell = 0
                        while cell < 152:
                            if row[cell] != '':
                                if mapassignments[mapnumber] == {}:
                                    mapassignments[mapnumber] = {roundnumber: [row[cell]]}
                                else:
                                    mapassignments[mapnumber][roundnumber] = [row[cell]]
                            cell += 4
                            mapnumber += 1
                    elif i in [8, 14, 17, 20, 23]:
                        mapnumber = 1
                        cell = 0
                        while cell < 152:
                            if cell % 4 == 0 or cell == 0:
                                # Date Started
                                if row[cell] != '':
                                    datestartedparts = row[cell].split('/')
                                    if len(datestartedparts) == 3:
                                        # We assign a default time of 12:00AM
                                        datestarted = datetime.datetime(int(datestartedparts[2]), int(datestartedparts[1]), int(datestartedparts[0]), 00, 00)
                                        mapassignments[mapnumber][roundnumber].append(datestarted)
                            elif (cell - 2) % 4 == 0:
                                # Date Due
                                if row[cell] != '':
                                    datedueparts = row[cell].split('/')
                                    if len(datedueparts) == 3:
                                        datedue = datetime.date(int(datedueparts[2]), int(datedueparts[1]), int(datedueparts[0]))
                                        mapassignments[mapnumber][roundnumber].append(datedue)
                                mapnumber += 1
                            cell += 1
                    elif i in [9, 15, 18, 21, 24]:
                        # Date Finished
                        mapnumber = 1
                        cell = 2
                        while cell < 152:
                            if row[cell] != '':
                                datefinishedparts = row[cell].split('/')
                                if len(datefinishedparts) == 3:
                                    datefinished = datetime.date(int(datefinishedparts[2]), int(datefinishedparts[1]), int(datefinishedparts[0]))
                                    mapassignments[mapnumber][roundnumber].append(datefinished)
                            cell += 4
                            mapnumber += 1
                        roundnumber += 1
                for maphistories in mapassignments.items():
                    existingmap = GqlQuery('SELECT * FROM Map WHERE folcom_cong = :1 AND map_number = :2', user.folcom_cong, maphistories[0]).get()
                    if existingmap:
                        for maphistory in maphistories[1].items():
                            returneddate = None
                            if len(maphistory[1]) == 4:
                                # Map assignment includes a finished date
                                returneddate = maphistory[1][3]
                            mapassignment = MapAssignment(folcom_cong = user.folcom_cong,
                                                          map = existingmap,
                                                          conductor = maphistory[1][0],
                                                          assigned_date = maphistory[1][1],
                                                          due_date = maphistory[1][2],
                                                          returned_date = returneddate,
                                                          round_number = maphistory[0]
                                                          )
                            mapassignment.put()
                            numimported += 1
                if numimported > 0:
                    alert = 'Successfully imported %s map assignments' % str(numimported)
                    alert_class = 'alert-success'
            else:
                # Import addresses
                mapnumber = int(self.request.params.get('mapname'))
                mapaddresses = self.get_map_addresses(mapnumber, user.folcom_cong)
                addressstart = None
                visibility = None
                importstartline = 7
                if self.request.params.get('importstartline', None):
                    # Can set the import start line via the query string
                    importstartline = int(self.request.params.get('importstartline'))
                if self.request.params.get('num_visits', None) == '2 Visits':
                    addressstart = 5
                elif self.request.params.get('num_visits', None) == '3 Visits':
                    addressstart = 6
                if addressstart:
                    for i, row in enumerate(filereader):
                        if i >= importstartline:
                            address = ''
                            streettype = None
                            streetname = ''
                            suburb = ''
                            sortorder = None
                            businessname = ''
                            if row[addressstart] != '':
                                # Street number and unit number
                                address = row[addressstart]+'/'+row[addressstart+1]
                            else:
                                address = row[addressstart+1]
                            if address == '':
                                # Some addresses are just street name placeholders so we skip them
                                continue
                            else:
                                visibility = True
                            unitnumber, unitletter, streetnumber, streetletter = self.get_address_numbers(address)
                            streetname, streettype = self.get_street_name_type(row[addressstart+2])
                            # TODO: the below code can be deleted once above tested
#                            streetparts = row[addressstart+2].split()
#                            if len(streetparts) > 0:
#                                if len(streetparts) == 1:
#                                    # Some streets don't have a street type
#                                    # as part of their name
#                                    streettype = ''
#                                    streetname = streetparts[0]
#                                elif streetparts[0] == 'The':
#                                    # Some streets are called 'The Something'
#                                    streettype = ''
#                                    streetname = ' '.join(streetparts)
#                                else:
#                                    streettype = streetparts.pop(len(streetparts)-1)
#                                    # Fixing up the streettype data
#                                    if streettype == 'Plc':
#                                        streettype = 'Pl'
#                                    elif streettype == 'Av':
#                                        streettype = 'Ave'
#                                    elif streettype == 'Drv':
#                                        streettype = 'Dr'
#                                    elif streettype == 'Clse':
#                                        streettype = 'Cl'
#                                    elif STREETTYPES.get(streettype, None):
#                                        streettype = STREETTYPES.get(streettype)
#                                    streetname = ' '.join(streetparts)
                            if row[addressstart+3] != '':
                                suburb = row[addressstart+3]
                            addressexists = self.check_address_exists(mapaddresses,
                                                                      unitnumber,
                                                                      unitletter,
                                                                      streetnumber,
                                                                      streetletter,
                                                                      streetname,
                                                                      streettype,
                                                                      suburb
                                                                      )
                            if addressexists is False:
                                # Add that fulla!
                                if row[addressstart+7] != '':
                                    sortorder = float(row[addressstart+7])
                                else:
                                    # No sort order set so lets get the sort order
                                    sortorder = self.get_sort_order(mapaddresses, streetname, streettype)
                                donotcall, remark = is_dnc(row[0])
                                isbiz, remark = get_business_name(row[0])
                                if isbiz:
                                    businessname = remark
                                # Doesn't exist
                                createdaddress = self.add_address(user.folcom_cong,
                                                                  unitnumber,
                                                                  unitletter,
                                                                  streetnumber,
                                                                  streetletter,
                                                                  streetname,
                                                                  streettype,
                                                                  suburb,
                                                                  mapnumber,
                                                                  row[addressstart+5],
                                                                  isbiz,
                                                                  businessname,
                                                                  '',
                                                                  '',
                                                                  donotcall,
                                                                  get_remark(row[0]),
                                                                  visibility,
                                                                  sortorder,
                                                                  True)
                                if not createdaddress is None:
                                    numimported += 1
                                    if self.request.params.get('check_for_duplicates') == 'yes':
                                        # Check if there are 2 addresses for this street address now
                                        existingstreets = Address.gql("WHERE unit_number = :1 "
                                                                      "AND unit_letter = :2 "
                                                                      "AND street_number = :3 "
                                                                      "AND street_letter = :4 "
                                                                      "AND street_name = :5 "
                                                                      "AND street_type = :6 "
                                                                      "AND folcom_cong = :7 "
                                                                      "AND visible = True", \
                                                                      unitnumber, unitletter, streetnumber, streetletter, streetname, streettype, user.folcom_cong).fetch(10, keys_only=True)
                                        if len(existingstreets) > 1:
                                            streetaddress = '%s%s/%s%s %s %s' % (unitnumber, unitletter, streetnumber, streetletter, streetname, streettype)
                                            streetaddress = streetaddress.replace('None/', '').strip() # Get rid of 'None' string if there's no unit number & trim space
                                            possibleduplicateaddress = (streetaddress + ', ' + suburb, streetaddress)
                                            possibleduplicateaddresses.append(possibleduplicateaddress)
                                else:
                                    # There was an error adding the address
                                    foo = [unitnumber, unitletter, streetnumber, streetletter, streetname, streettype, suburb]
                if numimported > 0:
                    alert = 'Successfully imported %s addresses' % str(numimported)
                    alert_class = 'alert-success'
        return {'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': None,
                'confirm_box_body': None,
                'has_permission': has_permission,
                'page_header': 'Upload CSV',
                'userid': userid,
                'foo': foo,
                'is_super_user': getattr(user, 'super_user', False),
                'possible_duplicate_addresses': possibleduplicateaddresses,
                }

    def add_address(self, folcom_cong, unitnumber, unitletter, streetnumber, streetletter, streetname, streettype, suburb, mapnumber, language, business, businessname, householdername, phonenumber, donotcall, remark, visibility, sortorder, imported=False):
        geopt = None
        addressid = generate_id('Address', 'address_id')

        if not unitnumber is None or unitletter != '':
            # Not all addresses will have a unit number/letter
            geocode_query = '%s%s/%s%s %s %s' % (unitnumber, unitletter, streetnumber, streetletter, streetname, streettype)
        else:
            geocode_query = '%s%s %s %s' % (streetnumber, streetletter, streetname, streettype)
        geocode_query = geocode_query.strip() + ', ' + suburb + ', ' + folcom_cong.region
        geocode_response = self.get_geocode(geocode_query)
        if not geocode_response is None:
            lat = geocode_response["geometry"]["location"]["lat"]
            lng = geocode_response["geometry"]["location"]["lng"]
            geopt = GeoPt(lat=float(lat), lon=float(lng))

        try:
            address = Address(folcom_cong=folcom_cong,
                              address_id=addressid,
                              unit_number=unitnumber,
                              unit_letter=unitletter,
                              street_number=streetnumber,
                              street_letter=streetletter,
                              street_name=streetname,
                              street_type=streettype,
                              suburb=suburb,
                              region=folcom_cong.region,
                              country=folcom_cong.country,
                              map_number=mapnumber,
                              language=language,
                              business=business,
                              business_name=businessname,
                              householder_name=householdername,
                              phone_number=phonenumber,
                              do_not_call=donotcall,
                              remark=remark,
                              visible=visibility,
                              sortorder=sortorder,
                              imported=imported,
                              geopt=geopt,
                              )
        except BadValueError:
            return None
        else:
            address.put()
            return address

    def check_address_exists(self, mapaddresses, unitnumber, unitletter, streetnumber, streetletter, streetname, streettype, suburb):
        # Check if already exists
        addressexists = False
        for existingaddress in mapaddresses:
            if existingaddress.unit_number == unitnumber and \
               existingaddress.unit_letter == unitletter and \
               existingaddress.street_number == streetnumber and \
               existingaddress.street_letter == streetletter and \
               existingaddress.street_name == streetname and \
               existingaddress.street_type == streettype and \
               existingaddress.suburb == suburb:
                addressexists = True
        if not addressexists:
            # Now lets check if the unit number and street number
            # were accidentally transposed, e.g. A/8 should be 8A
            if unitletter and streetletter == '':
                streetletter = unitletter
                unitletter = ''
                for existingaddress in mapaddresses:
                    if existingaddress.unit_number == unitnumber and \
                       existingaddress.unit_letter == unitletter and \
                       existingaddress.street_number == streetnumber and \
                       existingaddress.street_letter == streetletter and \
                       existingaddress.street_name == streetname and \
                       existingaddress.street_type == streettype and \
                       existingaddress.suburb == suburb:
                        addressexists = True
        return addressexists

    def check_if_items_to_batch_process(self):
        done_batch_run = False
        last_batch_processed = 0
        last_batch_operation = None
        for batch_operation in [('form.archive_batch', 'archive'),
                                ('form.delete_batch', 'delete'),
                                ('form.restore_batch', 'restore'),
                                ('update_batch_sortorder', 'sort'),
                                ('update_batch_map_number', 'move')
                                ]:
            if batch_operation[0] in self.request.params:
                if done_batch_run is False:
                    done_batch_run = True
                processed = self.process_batch(batch_operation[1])
                if processed > 0:
                    last_batch_processed = processed
                    last_batch_operation = batch_operation[1]
        return (done_batch_run, last_batch_processed, last_batch_operation)

    def get_map_addresses(self, mapnumber, folcomcong):
        # Skipping the memcache as was getting the following error:
        # cache value size [359518] too large: False
        # Memcache could not store the addresses for map 27FE37_UPLOADCSV_40
        mapaddresses = Address.gql("WHERE map_number = :1 "
                           "AND folcom_cong = :2 "
                           "AND visible = True", \
                           mapnumber, folcomcong).fetch(DEFAULT_FETCH)
        return mapaddresses
        # Here we get all the map addresses just once so
        # as not to repeatedly hit the datastore
        key = folcomcong.fcong_id+'_UPLOADCSV_'+str(mapnumber)
        mapaddresses = memcache.get(key)
        if mapaddresses is not None:
            return mapaddresses
        else:
            mapaddresses = Address.gql("WHERE map_number = :1 "
                                       "AND folcom_cong = :2 "
                                       "AND visible = True", \
                                       mapnumber, folcomcong).fetch(DEFAULT_FETCH)
            if len(mapaddresses) > 0:
                if not memcache.set(key, mapaddresses): #FIXME: Did have time of 120
                    logger.error('Memcache could not store the addresses for map %s' % key)
                return mapaddresses
        return []
        
    def get_sort_order(self, mapaddresses, streetname, streettype):
        sortorder = None
        for existingaddress in mapaddresses:
            if existingaddress.street_name == streetname and \
               existingaddress.street_type == streettype:
                # Ensure we give our new address same sortorder as
                # existing addresses on same street
                sortorder = existingaddress.sortorder
                break
        if not sortorder:
            sortorder = 9999.0
        return sortorder

    @view_config(renderer="templates/uploadjson.pt", name='upload_json', permission='admin')
    def upload_json(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        mapboundaries = self.get_boundary_coords('Map', 'map_number')
        return {'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': None,
                'confirm_box_body': None,
                'has_permission': has_permission,
                'page_header': 'Upload JSON',
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False),
                'map_boundaries': json.dumps(mapboundaries),
                'fcong_id': user.folcom_cong.fcong_id,
                }

    @view_config(renderer='templates/tablelisting.pt', name='manage_congregations', permission='admin')
    def manage_congregations(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        rows_data = []
        results = Congregation.gql("WHERE folcom_cong = :1 ORDER BY cong_name", user.folcom_cong).fetch(DEFAULT_FETCH)
        column_headings = [['Name', True], ['Code', True], ['Contact', True], ['Contact Email', True]] # 2nd value in list is the column view permission
        for result in results:
            # This enables us to keep the row values in order
            rows_data.append({1: result.cong_name,
                              2: result.cong_code,
                              3: result.contact_name,
                              4: result.contact_email,
                              'key': result.key(),
                              })
        if self.request.params.get('alert', None) and self.request.params.get('alert_class', None):
            alert = self.request.params['alert']
            alert_class = self.request.params['alert_class']
        return {'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': "Delete Congregation(s)?",
                'confirm_box_body': "Are you sure you want to delete the selected congregation(s)?",
                'has_permission': has_permission,
                'page_header': 'Manage Congregations',
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False),
                'column_headings': column_headings,
                'rows_data': rows_data,
                'model_name': 'Congregation',
                }

    @view_config(renderer='templates/tablelisting.pt', name='manage_maps', permission='admin')
    def manage_maps(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        rows_data = []
        results = Map.gql("WHERE folcom_cong = :1 ORDER BY map_number", user.folcom_cong).fetch(DEFAULT_FETCH)
        
        if user.folcom_cong.map_categories:
            # Adding in category support
            column_headings = [['No.', True], ['Visible', True], ['Category', True], ['Name', True]] # 2nd value in list is the column view permission
            for result in results:
                # This enables us to keep the row values in order
                rows_data.append({1: result.map_number,
                                  2: result.visible,
                                  3: result.map_category,
                                  4: result.map_name,
                                  'key': result.key(),
                                  })
        else:
            column_headings = [['No.', True], ['Visible', True], ['Name', True]] # 2nd value in list is the column view permission
            for result in results:
                # This enables us to keep the row values in order
                rows_data.append({1: result.map_number,
                                  2: result.visible,
                                  3: result.map_name,
                                  'key': result.key(),
                                  })
        if self.request.params.get('alert', None) and self.request.params.get('alert_class', None):
            alert = self.request.params['alert']
            alert_class = self.request.params['alert_class']
        return {'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': "Delete Map(s)?",
                'confirm_box_body': "Are you sure you want to delete the selected map(s)?",
                'has_permission': has_permission,
                'page_header': 'Manage Maps',
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False),
                'column_headings': column_headings,
                'rows_data': rows_data,
                'model_name': 'Map',
                }
        
    @view_config(renderer='templates/tablelisting.pt', name='manage_users', permission='admin')
    def manage_users(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        rows_data = []
        results = FolcomUser.gql("WHERE folcom_cong = :1 ORDER BY user_id", user.folcom_cong).fetch(DEFAULT_FETCH)
        column_headings = [['User ID', True], ['Name', True], ['Groups', True], ['Super User?', True]] # 2nd value in list is the column view permission
        for result in results:
            # This enables us to keep the row values in order
            rows_data.append({1: result.user_id,
                              2: result.name,
                              3: result.groups,
                              4: result.super_user,
                              'key': result.key(),
                              })
        if self.request.params.get('alert', None) and self.request.params.get('alert_class', None):
            alert = self.request.params['alert']
            alert_class = self.request.params['alert_class']
        return {'alert': alert,
                'alert_class': alert_class,
                'confirm_box_header': "Delete User(s)?",
                'confirm_box_body': "Are you sure you want to delete the selected user(s)?",
                'has_permission': has_permission,
                'page_header': 'Manage Users',
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False),
                'column_headings': column_headings,
                'rows_data': rows_data,
                'model_name': 'Folcom User',
                }

    @view_config(renderer='templates/viewmap.pt', name='view_map', permission='edit')
    def view_map(self):
        alert = None
        alert_class = None
        userid = authenticated_userid(self.request)
        user = self.get_folcom_user(userid)
        downloadurl = None
        baseurl = None
        addressresults = []
        conductor = None
        duedate = None
        months_to_finish = user.folcom_cong.default_months_finish_map
        nexturl = self.request.referer
        roundnumber = user.folcom_cong.round_number # TODO is this good? Default to the round number in settings
        # Start batch processing
        done_batch_run, last_batch_processed, last_batch_operation = self.check_if_items_to_batch_process()
        if done_batch_run:
            return self.display_batch_processed_message(last_batch_processed, last_batch_operation)
        # End batch processing
        if 'form.submitted' in self.request.params:
            map = int(self.request.params['mapname'])
            addressresults = Address.gql("WHERE map_number = :1 AND folcom_cong = :2 AND visible = True ORDER BY sortorder, street_name, street_number, unit_number", \
                                      map, user.folcom_cong).fetch(DEFAULT_FETCH)
            # TODO: Removing caching code until I'm happy with the implementation
#            viewmapmapkey = user.folcom_cong.fcong_id+'_VIEWMAP_MAP_'+str(map)
#            mapresults = memcache.get(viewmapmapkey) # TODO: Invalidate if add/edit map
#            if not mapresults:
#                mapresults = GqlQuery("SELECT * FROM Map WHERE map_number = :1 AND folcom_cong = :2", map, user.folcom_cong).get()
#                if mapresults:
#                    memcache.set(viewmapmapkey, mapresults)
            mapresults = GqlQuery("SELECT * FROM Map WHERE map_number = :1 AND folcom_cong = :2", map, user.folcom_cong).get()
            if mapresults:
                baseurl = self.request.application_url + '/download_map/' + mapresults.map_id
                mapassignment = self.get_latest_map_assignment(mapresults, user.folcom_cong)
                if mapassignment:
                    if not mapassignment.returned_date is None:
                        # If the map assignment has a returned date
                        # the map has been checked back in
                        roundnumber = mapassignment.round_number + 1 # increment to be next round
                    else:
                        # Map is still checked out
                        conductor = mapassignment.conductor
                        duedate = self.format_date(mapassignment.due_date)
                        roundnumber = mapassignment.round_number
                        downloadurl = baseurl + '?assignee=' + conductor + '&duedate=' + duedate + '&roundnumber=' + str(roundnumber)
        if self.request.params.get('alert', None) and self.request.params.get('alert_class', None):
            # Just archived an address
            alert = self.request.params['alert']
            alert_class = self.request.params['alert_class']

        return {'address_results': addressresults,
                'alert': alert,
                'alert_class': alert_class,
                'base_url': baseurl,
                'conductor': conductor,
                'confirm_box_header': "Archive Address?",
                'confirm_box_body': "Are you sure you want to archive this address?",
                'date_format': 'dd/mm/yy',
                'download_url': downloadurl,
                'due_date': duedate,
                'has_permission': has_permission,
                'months_to_finish': months_to_finish,
                'page_header': 'Manage Maps',
                'round_number': roundnumber,
                'userid': userid,
                'is_super_user': getattr(user, 'super_user', False),
                }


def generate_id(modelname, idname):
    id = md5.new(str(random())).hexdigest()[-6:].upper()
    # Check if id already exists
    q = "SELECT * FROM "+modelname+" WHERE "+idname+" = :1"
    results = GqlQuery(q, id).fetch(1)
    if len(results) > 0:
        # Already exists so generate another one
        return generate_id(modelname, idname)
    return id

def create_fcong(cong_name, languages, admin_email, region, country, round_number, other_languages, default_months_finish_map):
    cong = GqlQuery("SELECT * FROM FolcomCong WHERE fcong_name = :1", cong_name).get() # unique by name
    if cong is None:
        already_exists = False
        languages = [language for language in languages.split('\r\n')]
        other_languages = [other_language for other_language in other_languages.split('\r\n')]
        cong = FolcomCong(fcong_id=generate_id('FolcomCong', 'fcong_id'),
                          fcong_name=cong_name,
                          region=region,
                          country=country,
                          languages=languages,
                          admin_email=admin_email,
                          round_number=int(round_number),
                          other_languages=other_languages,
                          default_months_finish_map=int(default_months_finish_map),
                          )
        cong.put()
    else:
        already_exists = True
    return (cong, already_exists)                 

def create_user(fcong, userid, password, groups, super_user):
    user = GqlQuery("SELECT * FROM FolcomUser WHERE user_id = :1", userid).get() # Will be unique across DB as we're using email address for user_id
    if user is None:
        already_exists = False
        groups = [group for group in groups.split('\r\n')]
        super_user = True if super_user == 'on' else False
        user = FolcomUser(user_id=userid,
                          password=password,
                          folcom_cong=fcong,
                          groups=groups,
                          super_user=super_user
                          )
        user.put()
    else:
        already_exists = True
    return (user, already_exists)

def create_map(fcong, mapnumber, mapname, boundarycoords):
    mapid = generate_id('Map', 'map_id')
    mapnumber = int(mapnumber)
    new_value = []
    if boundarycoords != '': # They may've not defined any boundaries
        try:
            for value in boundarycoords.split('\r\n'):
                value = value.split(',')
                new_value.append(GeoPt(float(value[0]), float(value[1])))
        except AttributeError:
            # Running setup_folcom code
            for value in boundarycoords:
                new_value.append(value)
    map = GqlQuery("SELECT * FROM Map WHERE map_number = :1 AND folcom_cong = :2", mapnumber, fcong).get() # unique by nap_number
    if map is None:
        already_exists = False
        map = Map(folcom_cong=fcong,
                  map_id=mapid,
                  map_number=mapnumber,
                  map_name=mapname,
                  boundary_coords=new_value
                  )
        map.put()
    else:
        already_exists = True
    return (map, already_exists)

def create_cong(fcong, congname, congcode, conglanguage, contactname, contactemail, boundarycoords):
    cong = GqlQuery("SELECT * FROM Congregation WHERE cong_name = :1 AND folcom_cong = :2", congname, fcong).get() # unique by cong_name
    if cong is None:
        already_exists = False
        new_value = []
        if boundarycoords != '': # They may've not defined any boundaries
            try:
                for value in boundarycoords.split('\r\n'):
                    value = value.split(',')
                    new_value.append(GeoPt(float(value[0]), float(value[1])))
            except AttributeError:
                # Running setup_folcom code
                try:
                    for value in boundarycoords:
                        new_value.append(value)
                except TypeError:
                    pdb.set_trace()
        cong = Congregation(folcom_cong=fcong,
                            cong_name=congname,
                            cong_code=congcode,
                            cong_language=conglanguage,
                            contact_name=contactname,
                            contact_email=contactemail,
                            boundary_coords=new_value
                            )
        cong.put()
    else:
        already_exists = True
    return (cong, already_exists)

def store(key, value, chunksize=950000):
    serialized = pickle.dumps(value, 2)
    values = {}
    for i in xrange(0, len(serialized), chunksize):
        values['%s.%s' % (key, i//chunksize)] = serialized[i : i+chunksize]
    memcache.set_multi(values)

def retrieve(key):
    result = memcache.get_multi(['%s.%s' % (key, i) for i in xrange(32)])
    serialized = ''.join([v for v in result.values() if v is not None])
    return pickle.loads(serialized)

def setup_folcom():
    if not DEV:
        return
    cong, already_exists = create_fcong(
        'Panmure Chinese',
        'Ch\r\nMa\r\nCa',
        'Tim Knapp\r\nJohn Salmon\r\nOwen Chang\r\nTakMing Chung\r\nTerence Tang\r\nAbigail Yuan\r\nCherie Mehrtens\r\nLiang Li',
        'hxingli@gmail.com',
        'Auckland',
        'New Zealand',
        '1',
        'Tagalog\r\nVietnamese\r\nCambodian\r\nTongan\r\nSamoan\r\nHindi',
        '84'
    )
    user, already_exists = create_user(cong, 'tim@emergetec.com', 'secret', 'group:admin', 'on')
    create_user(cong, 'editor@emergetec.com', 'secret', 'group:editor', '')
    create_user(cong, 'viewer@emergetec.com', 'secret', '', '')
    create_user(cong, 'junkorama007@gmail.com', 'secret', 'group:admin', '')
    create_user(cong, 'hxingli@gmail.com', 'secret', 'group:admin', '')
    create_user(cong, 'me.bonnie@gmail.com', 'secret', 'group:editor', '')
    from congconfig import congcoords, mapcoords
    mapnames = ('Balmoral',
                'Mt Roskill',
                'Mt Roskill South',
                'Hillsborough',
                'Hillsborough, Waikowhai',
                'Epsom',
                'Three Kings, One Tree Hill, Royal Oak',
                'Greenlane',
                'Oranga, Onehunga, Te Papa, Penrose',
                'Mangere Bridge, Favona, Mangere East, Mangere',
                'Parnell, Remuera',
                'Ellerslie',
                'Mt Wellington',
                'Orakei, Mission Bay',
                'St Johns, St Johns Park, Meadowbank, Kohimarama',
                'St Heliers',
                'Glen Innes',
                'Pakuranga',
                'Farm Cove, Sunnyhills',
                'Pakuranga Heights',
                'Bucklands Beach, Eastern Beach',
                'Half Moon Bay',
                'Eastern Beach',
                'Howick, Mellons Bay, Cockle Bay, Shelly Park',
                'Highland Park',
                'Botany Downs',
                'Somerville',
                'Northpark',
                'Point View Reserve, Dannemora East',
                'Burswood',
                'Dannemora West',
                'Dannemora West',
                'Chapel Downs, Flat Bush, East Tamaki, Otara',
                'Wiri, Totara Hgts, Goodwood Hgts, Manukau Hgts, Clover Park, Manukau Central',
                'Otahuhu',
                'Papatoetoe, Puhinui',
                'TheGardns, Manurewa, RandwickPk, Alfriston, RandwickPk, Takanini, Hill Park, Manurewa, ConiferGrv, ClendnPk, WattleDwns, Weymouth',
                'Papakura, Red Hill, Papakura, Hingaia, Opaheke, Rosehill, Drury, Karaka, Runcinam, Bombay, Pukekohe, Waiuku'
                )
    for congcoord in congcoords:
        create_cong(user.folcom_cong, congcoord[0], congcoord[1], None, 'Fritz Renk', 'fritzanddianne@yahoo.co.uk', boundarycoords=congcoord[2])
    for mapcoord in mapcoords:
        create_map(user.folcom_cong, mapcoord[0], mapnames[mapcoord[0]-1], boundarycoords=mapcoord[1])
#     create_map(cong, 4, generate_id('Map', 'map_id'), 'Hillsborough', [GeoPt(-36.911612,174.740034),
#                                GeoPt(-36.913293,174.739863),
#                                GeoPt(-36.914426,174.739562),
#                                GeoPt(-36.916107,174.739004),
#                                GeoPt(-36.917479,174.738489),
#                                GeoPt(-36.920911,174.737073),
#                                GeoPt(-36.921905,174.738360),
#                                GeoPt(-36.922557,174.740077),
#                                GeoPt(-36.923312,174.742480),
#                                GeoPt(-36.923724,174.743425),
#                                GeoPt(-36.925439,174.748531),
#                                GeoPt(-36.926125,174.750506),
#                                GeoPt(-36.924822,174.751836),
#                                GeoPt(-36.923484,174.752780),
#                                GeoPt(-36.922454,174.753724),
#                                GeoPt(-36.921391,174.755055),
#                                GeoPt(-36.920018,174.757029),
#                                GeoPt(-36.919435,174.757672),
#                                GeoPt(-36.918543,174.757672),
#                                GeoPt(-36.917788,174.757758),
#                                GeoPt(-36.916210,174.754454),
#                                GeoPt(-36.914735,174.751021),
#                                GeoPt(-36.914186,174.746386),
#                                GeoPt(-36.913328,174.743339)
#                                ])
#     create_map(cong, 5, generate_id('Map', 'map_id'), 'Hillsborough, Waikowhai', [GeoPt(-36.920567,174.736301),
#                                GeoPt(-36.922763,174.734326),
#                                GeoPt(-36.924547,174.734841),
#                                GeoPt(-36.925714,174.735528),
#                                GeoPt(-36.926468,174.736215),
#                                GeoPt(-36.927909,174.736129),
#                                GeoPt(-36.928870,174.736129),
#                                GeoPt(-36.929213,174.735528),
#                                GeoPt(-36.930242,174.735528),
#                                GeoPt(-36.931889,174.734241),
#                                GeoPt(-36.934839,174.734326),
#                                GeoPt(-36.936554,174.734584),
#                                GeoPt(-36.932095,174.760676),
#                                GeoPt(-36.923861,174.772950),
#                                GeoPt(-36.922146,174.769260),
#                                GeoPt(-36.921391,174.767972),
#                                GeoPt(-36.920636,174.767972),
#                                GeoPt(-36.915627,174.769002),
#                                GeoPt(-36.915489,174.768487),
#                                GeoPt(-36.914597,174.768659),
#                                GeoPt(-36.914803,174.769689),
#                                GeoPt(-36.915009,174.770804),
#                                GeoPt(-36.914940,174.771148),
#                                GeoPt(-36.914597,174.770976)
#                                ])
#     create_cong(cong, 'Mount Roskill', 'MTRK', [GeoPt(-36.911612,174.740034),
#                                GeoPt(-36.913293,174.739863),
#                                GeoPt(-36.914426,174.739562),
#                                GeoPt(-36.916107,174.739004),
#                                GeoPt(-36.917479,174.738489),
#                                GeoPt(-36.920911,174.737073),
#                                GeoPt(-36.921905,174.738360),
#                                GeoPt(-36.922557,174.740077),
#                                GeoPt(-36.923312,174.742480),
#                                GeoPt(-36.923724,174.743425),
#                                GeoPt(-36.925439,174.748531),
#                                GeoPt(-36.926125,174.750506),
#                                GeoPt(-36.924822,174.751836),
#                                GeoPt(-36.923484,174.752780),
#                                GeoPt(-36.922454,174.753724),
#                                GeoPt(-36.921391,174.755055),
#                                GeoPt(-36.920018,174.757029),
#                                GeoPt(-36.919435,174.757672),
#                                GeoPt(-36.918543,174.757672),
#                                GeoPt(-36.917788,174.757758),
#                                GeoPt(-36.916210,174.754454),
#                                GeoPt(-36.914735,174.751021),
#                                GeoPt(-36.914186,174.746386),
#                                GeoPt(-36.913328,174.743339)
#                                ])
#     create_cong(cong, 'Onehunga', 'ONHG', [GeoPt(-36.920567,174.736301),
#                                GeoPt(-36.922763,174.734326),
#                                GeoPt(-36.924547,174.734841),
#                                GeoPt(-36.925714,174.735528),
#                                GeoPt(-36.926468,174.736215),
#                                GeoPt(-36.927909,174.736129),
#                                GeoPt(-36.928870,174.736129),
#                                GeoPt(-36.929213,174.735528),
#                                GeoPt(-36.930242,174.735528),
#                                GeoPt(-36.931889,174.734241),
#                                GeoPt(-36.934839,174.734326),
#                                GeoPt(-36.936554,174.734584),
#                                GeoPt(-36.932095,174.760676),
#                                GeoPt(-36.923861,174.772950),
#                                GeoPt(-36.922146,174.769260),
#                                GeoPt(-36.921391,174.767972),
#                                GeoPt(-36.920636,174.767972),
#                                GeoPt(-36.915627,174.769002),
#                                GeoPt(-36.915489,174.768487),
#                                GeoPt(-36.914597,174.768659),
#                                GeoPt(-36.914803,174.769689),
#                                GeoPt(-36.915009,174.770804),
#                                GeoPt(-36.914940,174.771148),
#                                GeoPt(-36.914597,174.770976)
#                                ])

def update_schema_task(cursor=None, num_updated=0, batch_size=200):
    """Task that handles updating the models' schema.

    This is started by
    UpdateSchemaHandler. It scans every entity in the datastore for the
    address model and re-saves it so that it has the new schema fields.
    """

    # Force ndb to use v2 of the model by re-loading it.
    #reload(models_v2)

    # Get all of the entities for this Model.
    addresses = Address.all()
    totaladdr = len(Address.all().fetch(30000, keys_only=True))
    
    if num_updated == 0:
        logging.info("Total addresses are: {}".format(totaladdr))
    
    address_cursor = memcache.get('address_cursor')
    if address_cursor:
        addresses.with_cursor(start_cursor=address_cursor)
    
    #addresses, next_cursor, more = query.fetch_page(
    #    batch_size, start_cursor=cursor)

    to_put = []
    for address in addresses.run(limit=batch_size):
        # Give the new fields default values.
        # If you added new fields and were okay with the default values, you
        # would not need to do this.
        #address.num_votes = 1
        #address.avg_rating = 5
        to_put.append(address)

    # Save the updated entities.
    if to_put:
        db.put(to_put)
        num_updated += len(to_put)
        logging.info(
            'Put {} entities to Datastore for a total of {}'.format(
                len(to_put), num_updated))

    address_cursor = addresses.cursor()
    memcache.set('address_cursor', address_cursor)

    # If there are more entities, re-queue this task for the next page.
    if num_updated < totaladdr:
        deferred.defer(
            update_schema_task, cursor=address_cursor, num_updated=num_updated)
    else:
        logging.info(
            'update_schema_task complete with {0} updates!'.format(
                num_updated))
