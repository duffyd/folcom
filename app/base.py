from pyramid.renderers import get_renderer
from pyramid.decorator import reify

class BaseView(object):

    @reify
    def global_macros(self):
        renderer = get_renderer("templates/macros.pt")
        return renderer.implementation().macros