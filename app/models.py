from google.appengine.ext import db
from pyramid.security import Authenticated, Allow, Everyone, \
    ALL_PERMISSIONS, DENY_ALL

class Root(object):
    __acl__ = [
        (Allow, Authenticated, 'view'),
        (Allow, 'group:editor', 'edit'),
        (Allow, 'group:conductor', 'can assign map page'),
        (Allow, 'group:admin', ALL_PERMISSIONS),
        DENY_ALL
    ]

    def __init__(self, request):
        self.request = request


class FolcomCong(db.Model):
    fcong_id = db.StringProperty()
    fcong_name = db.StringProperty()
    region = db.StringProperty()
    country = db.StringProperty()
    languages = db.StringListProperty()
    admin_email = db.EmailProperty()
    round_number = db.IntegerProperty()
    other_languages = db.StringListProperty()
    default_months_finish_map = db.IntegerProperty(default=4)
    map_categories = db.StringListProperty()
    email_territory_update_days = db.IntegerProperty(default=180)
    email_weekly_report = db.BooleanProperty(default=True)
    extra_weekly_report_recipients = db.StringListProperty()
    
    # Don't need fcong_id, folcom_cong as these aren't displayed anyway
    # The form_schema is in the format: field name, field label, required (either 'required' or None)
    form_schema = [('fcong_name', 'Congregation name', 'required'),
                   ('region', 'Region', 'required'),
                   ('country', 'Country', 'required'),
                   ('languages', 'Languages', 'required'),
                   ('admin_email', 'Admin email', 'required'),
                   ('round_number', 'Round number', 'required'),
                   ('other_languages', 'Other languages', 'required'),
                   ('default_months_finish_map', 'Months to finish map', 'required'),
                   ('map_categories', 'Map categories', None),
                   ('email_territory_update_days', 'Email territory update interval (days)', 'required'),
                   ('email_weekly_report', 'Send weekly report email?', None),
                   ('extra_weekly_report_recipients', 'Additional weekly report recipients', None),
                   ]


class FolcomUser(db.Model):
    folcom_cong = db.ReferenceProperty(FolcomCong)
    user_id = db.StringProperty()
    password = db.StringProperty()
    groups = db.StringListProperty()
    super_user = db.BooleanProperty(default=False)
    name = db.StringProperty()
    
    # Don't need folcom_cong as this isn't displayed anyway
    form_schema = [('name', 'First & last name', 'required'),
                   ('user_id', 'User ID', 'required'),
                   ('password', 'Password', 'required'),
                   ('groups', 'Groups', None),
                   ('super_user', 'Super user?', None),
                   ]
    

class Address(db.Model):
    folcom_cong = db.ReferenceProperty(FolcomCong)
    address_id = db.StringProperty()
    unit_number = db.IntegerProperty()
    unit_letter = db.StringProperty()
    street_number = db.IntegerProperty()
    street_letter = db.StringProperty()
    street_name = db.StringProperty()
    street_type = db.StringProperty()
    suburb = db.StringProperty()
    region = db.StringProperty()
    country = db.StringProperty()
    map_number = db.IntegerProperty()
    language = db.StringProperty()
    business = db.BooleanProperty()
    business_name = db.StringProperty()
    householder_name = db.StringProperty()
    do_not_call = db.BooleanProperty()
    remark = db.StringProperty()
    visible = db.BooleanProperty()
    sortorder = db.FloatProperty()
    phone_number = db.StringProperty()
    imported = db.BooleanProperty(default=False)
    timestamp = db.DateTimeProperty(auto_now=True, indexed=True)
    geopt = db.GeoPtProperty(default=None)
    

class Map(db.Model):
    """ 
    Ensure map_number is unique for FCong.
    When a map is not visible it's addresses still
    show up in the 'Territory Information' report
    """
    folcom_cong = db.ReferenceProperty(FolcomCong)
    map_id = db.StringProperty()
    map_number = db.IntegerProperty()
    map_name = db.StringProperty()
    boundary_coords = db.ListProperty(db.GeoPt)
    business_map = db.BooleanProperty(default=False)
    map_category = db.StringProperty()
    visible = db.BooleanProperty(default=True)
    
    # Don't need folcom_cong as this isn't displayed anyway
    form_schema = [('visible', 'Visible?', None),
                   ('map_number', 'Number', 'required'),
                   ('map_name', 'Name', 'required'),
                   ('boundary_coords', 'boundary_coords', None),
                   ('business_map', 'Business map?', None),
                   ('map_category', 'Map category', None),
                   ]
    

class MapAssignment(db.Model):
    folcom_cong = db.ReferenceProperty(FolcomCong)
    map = db.ReferenceProperty(Map)
    conductor = db.StringProperty()
    assigned_date = db.DateTimeProperty(auto_now_add=True)
    due_date = db.DateProperty()
    returned_date = db.DateProperty()
    round_number = db.IntegerProperty()
    left_to_do = db.IntegerProperty()

    # Don't need folcom_cong as this isn't displayed anyway    
    form_schema = [('map', 'Map', 'required'),
                   ('conductor', 'Conductor', 'required'),
                   ('assigned_date', 'Assigned date', 'required'),
                   ('due_date', 'Due date', 'required'),
                   ('returned_date', 'Returned date', 'required'),
                   ('round_number', 'Round number', 'required'),
                   ('left_to_do', 'Houses left to do', None)
                   ]
    

class Congregation(db.Model):
    # Ensure cong_name is unique for FCong
    folcom_cong = db.ReferenceProperty(FolcomCong)
    cong_name = db.StringProperty()
    cong_code = db.StringProperty()
    boundary_coords = db.ListProperty(db.GeoPt)
    contact_name = db.StringProperty()
    contact_email = db.EmailProperty()
    last_emailed_update = db.DateProperty()
    cong_language = db.StringProperty()
    
    # Don't need folcom_cong and last_emailed_update as
    # these aren't displayed anyway
    form_schema = [('cong_name', 'Name', 'required'),
                   ('cong_code', 'Code', 'required'),
                   ('cong_language', 'Language', None),
                   ('contact_name', 'Contact', 'required'),
                   ('contact_email', 'Contact email', 'required'),
                   ('boundary_coords', 'boundary_coords', None),
                   ]
    

class UploadedTextFile(db.Model):
    data = db.TextProperty()
    

class TerritoryEmailUpdate(db.Model):
    folcom_cong = db.ReferenceProperty(FolcomCong)
    bcc_admin = db.BooleanProperty(default=True)
    email_subject = db.StringProperty()
    email_body = db.TextProperty()
    
    
class MapPageAssignment(db.Model):
    folcom_cong = db.ReferenceProperty(FolcomCong)  
    map_assignment = db.ReferenceProperty(MapAssignment, collection_name='map_pages')
    assignee_email = db.EmailProperty()
    address_ids = db.StringListProperty()
    visit_results = db.StringListProperty()
    assigned_date = db.DateProperty(auto_now_add=True, indexed=True)
    returned_date = db.DateProperty()