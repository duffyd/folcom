import logging

logging.getLogger().setLevel(logging.INFO)

import sys, os

#sys.path.insert(0,'lib/dist')
app_dir = 'lib/dist'
# make sure app_dir is the first in import order
# this ensures that webob reimport will use our version
if app_dir in sys.path:
   sys.path.remove(app_dir)
sys.path.insert(0, app_dir)

# remove preimported webob
for name in sys.modules.keys():
   if name.split('.')[0] == 'webob':
       del sys.modules[name]

# reimport webob
import webob
# and make sure it was imported from app_dir
if not webob.__file__.startswith(app_dir):
   raise ValueError("%s / %s" % (webob.__file__, app_dir))

try:
    import pkg_resources
except ImportError:
    pass
else:
    if hasattr(os, '__loader__'):
        # This only seems to apply to the SDK
        pkg_resources.register_loader_type(type(os.__loader__), pkg_resources.DefaultProvider)

from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.config import Configurator
from models import Root
from security import groupfinder
from views import create_fcong, create_user, setup_folcom

DEV = False 

# Load local settings/overrides
try:
    from locals import *
except ImportError:
    pass

settings = {
    'reload_templates': 'true',
    'debug_authorization': 'true',
    'debug_notfound': 'false',
    'debug_templates': 'false',
    'default_locale_name': 'en',
}

def main():
    """ This function runs a Pyramid WSGI application.
    """
    authn_policy = AuthTktAuthenticationPolicy(secret='sosecret', callback=groupfinder)
    authz_policy = ACLAuthorizationPolicy()
    config = Configurator(settings=settings,
                          authentication_policy=authn_policy,
                          authorization_policy=authz_policy,
                          root_factory=Root)
    config.add_route('archiveaddress', '/archive_address/{objectid}')
    config.add_route('deleteaddress', '/delete_address/{objectid}')
    config.add_route('deletemapassignment', '/delete_mapassignment/{objectid}')
    config.add_route('downloadmap', '/download_map/{objectid}')
    config.add_route('editaddress', '/edit_address/{objectid}')
    config.add_route('mappages', '/map_pages/{mapnumber}')
    config.add_route('mappage', '/p/{encodedurl}')
    config.add_route('mappageaddaddress', '/a/{encodedurl}')
    config.add_route('otherlanguage', '/assign_other_language/{objectid}')
    config.add_route('unarchiveaddress', '/unarchive_address/{objectid}')
    config.scan('views')
    setup_folcom()
    return config.make_wsgi_app()

app = main()