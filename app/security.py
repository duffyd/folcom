from google.appengine.ext.db import GqlQuery

def groupfinder(userid, request):
    q = GqlQuery("SELECT * FROM FolcomUser WHERE user_id = :1", userid)
    results = q.fetch(1)
    if len(results) > 0:
        return results[0].groups
    return []