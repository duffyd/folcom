var markersArray = [];
var mapBoundariesArray = [];
var origBoundary = null;
var geocoder = null;
var map = null;
var eventListeners = [];
var clickListener = null;
var map_labels = []
var timerIds = []
var t;
var max_attempts = 50; // Total times to attempt to geocode address

$(document).ready(function() {
  function get_map(latlng, map) {
    $.getJSON(
      '/get_map_boundaries',
      function(json) {
        $.each(json.boundariesCoords, function(boundaryCounter, boundary) {
          var boundaryCoords = [];
          $.each(boundary, function(boundaryCoordCounter, boundaryCoord) {
            boundaryCoords[boundaryCoordCounter] = new google.maps.LatLng(boundaryCoord[0], boundaryCoord[1]); 
          });
          var polygon = new google.maps.Polygon({
            paths: boundaryCoords,
            strokeColor: "#000000",
            strokeOpacity: 1,
            strokeWeight: 1,
            fillColor: "#336699",
            fillOpacity: 0.3,
            visible: false
            });
          polygon.setMap(map);
          if (polygon.containsLatLng(latlng)) {
            // Exit if we found a map this address belongs to
            $('#mapname').val(json.names[boundaryCounter]);
            return false;
          }
        });
        display_alert('Could not lookup the map for this address. Please manually specify one', 'alert-error');
      }
    );
  }

  $('#lookup_map_cong_button').click(function(e) {
    // Geocode address and then get map name, and suburb
    // Trigger validation of data
    var fieldids = ['#streetnumber', '#streetname', '#suburb', '#streettype'];
    var isvalid = true; // Default to is valid
    var returnval = null;
    $.each(fieldids, function(i, fieldid) {
      var inputs = $(fieldid).validator();
      if (inputs.data('validator').checkValidity() == false) {
        isvalid = false;
      }
    });
    if (isvalid == false) {
      return false; // Exit if 1-or-more of the fields weren't valid
    }
    var address = create_address_string();
    update_map(address);
  });

  $('#unitletter').blur(function() {
    // Uppercase the unit letter
    var unitletter = $('#unitletter').val();
    unitletter = unitletter.toUpperCase();
    $('#unitletter').val(unitletter);
  });

  $('#streetletter').blur(function() {
    // Uppercase the street letter
    var streetletter = $('#streetletter').val();
    streetletter = streetletter.toUpperCase();
    $('#streetletter').val(streetletter);
  });
});

// The below code was ripped from here:
// http://stackoverflow.com/questions/2177055/how-do-i-get-google-maps-to-show-a-whole-polygon
google.maps.Polygon.prototype.getBounds = function() {
  var bounds = new google.maps.LatLngBounds();
  var paths = this.getPaths();
  var path;        
  for (var i = 0; i < paths.getLength(); i++) {
      path = paths.getAt(i);
      for (var ii = 0; ii < path.getLength(); ii++) {
          bounds.extend(path.getAt(ii));
      }
  }
  return bounds;
}

function update_map(address) {
  var found_map = null;
  var suburb = null;
  map.setZoom(13);
  geocoder.geocode({'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      // Doing everything in here as async madness really
      // causes havoc otherwise
      // Update the suburb to be the same as per Google Maps
      $.each(results[0].address_components, function(addrpartidx, addrpart) {
        if ($.inArray('sublocality', addrpart.types) > -1) {
          suburb = addrpart.long_name;
        }
      });
      console.log('Google Maps suburb: '+suburb+', our current suburb: '+$('#suburb').val());
      if (($('#suburb').val() != suburb) && (suburb != null)) {
        $('#suburb').val(suburb);
        console.log('The suburb is now: '+$('#suburb').val());
      }
      found_map = false; // Reset the found flag
      var boundariesArray = mapBoundariesArray;
      var json = $.parseJSON($('#map_boundaries').val());
      if (results[0].geometry.location != null) {
        $.each(boundariesArray, function(i, polygon) {
          if (polygon.containsLatLng(results[0].geometry.location)) {
            // Found the map!
            console.log('The map is: '+json.names[i]);
            $('#mapname').val(json.names[i]);
            found_map = true;
            return false;
          }
        });
        if (found_map == false) {
          display_alert('Could not lookup the map for this address. Please manually specify one', 'alert-error');
        }
      }
      deleteOverlays(); // Delete existing markers
      var marker = new google.maps.Marker({
        map: map,
        position: results[0].geometry.location,
        title: results[0].formatted_address
      });
      markersArray.push(marker);
    } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
      t = setTimeout(function() { update_map(address); }, (timeout * 3));
      timerIds.push(t);
    } else {
      display_alert("Geocode was not successful for the following reason: " + status, 'alert-error');
    }
  });
}

function geocode_address(address) {
  // This is run upon opening the Edit Address page
  // as well as from the Upload JSON page
  var timeout = 600;
  var addressidx = null;
  var bulkimport = false;
  var suburb = null;
  if (arguments.length > 1) {
    // this is being run from '/uploadjson'
    var addressidx = arguments[1];
    bulkimport = true;
    console.log('Inside geocode_address, address='+address+', attempt='+addressesToImport[addressidx]['num_attempts']);
  } else {
    console.log('Inside geocode_address, address='+address);
  }
  map.setZoom(13);
  geocoder.geocode({'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      console.log('latlng: '+results[0].geometry.location);
      if (bulkimport == true) {
        // Update the suburb to be the same as per Google Maps
        $.each(results[0].address_components, function(addrpartidx, addrpart) {
          if ($.inArray('sublocality', addrpart.types) > -1) {
            suburb = addrpart.long_name;
          }
        });
        console.log('Google Maps suburb: '+suburb+', our current suburb: '+addressesToImport[addressidx]['suburb']);
        if ((addressesToImport[addressidx]['suburb'] != suburb) && (suburb != null)) {
          addressesToImport[addressidx]['suburb'] = suburb;
          console.log('The suburb is now: '+addressesToImport[addressidx]['suburb']);
        }
        var boundariesArray = mapBoundariesArray;
        var json = $.parseJSON($('#map_boundaries').val());
        //alert('json.boundariesCoords: '+json.boundariesCoords.length);
        if (results[0].geometry.location != null) {
          $.each(json.boundariesCoords, function(boundaryCounter, boundary) {
            var boundaryCoords = [];
            $.each(boundary, function(boundaryCoordCounter, boundaryCoord) {
              boundaryCoords[boundaryCoordCounter] = new google.maps.LatLng(boundaryCoord[0], boundaryCoord[1]); 
            });
            var polygon = new google.maps.Polygon({
              paths: boundaryCoords,
              strokeColor: "#000000",
              strokeOpacity: 1,
              strokeWeight: 1,
              fillColor: "#336699",
              fillOpacity: 0.3,
              visible: true
              });
            polygon.setMap(map);
            if (polygon.containsLatLng(results[0].geometry.location)) {
              // Found the map!
              addressesToImport[addressidx]['map_number'] = json.names[boundaryCounter].toString();
              console.log('The map is: '+addressesToImport[addressidx]['map_number']);
            }
          });
          if (addressesToImport[addressidx]['map_number'] == '') {
            // Obviously address isn't in our territory
            console.log(addressesToImport[addressidx]['street_name']+' '+addressesToImport[addressidx]['street_type']+' is not in our territory');
            addressesToImport[addressidx]['not_in_territory'] = 'yes';
          }
        }
        addressesToImport[addressidx]['num_attempts'] = max_attempts; // Set max attempts as we've tried to get map now
      } else {
        map.setCenter(results[0].geometry.location);
        deleteOverlays(); // Delete existing markers
        var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location,
          title: results[0].formatted_address
          });
        markersArray.push(marker);
      }
    } else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
      console.log("You're over your limit buddy!");
      if (bulkimport == true) {
        addressesToImport[addressidx]['locked'] = true;
        addressesToImport[addressidx]['num_attempts'] += 1;
      }
      t = setTimeout(function() {geocode_address(address, addressidx);}, (timeout * 3));
      timerIds.push(t);
    } else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
      // Our address has something wrong with it, let's modify it
      // Addresses are in a format like: 112 Jellicoe Road, Panmure, Auckland, New Zealand
      var region = ', Auckland';
      var regionStart = address.indexOf(region);
      var current_suburb = address.substring(address.lastIndexOf(',', regionStart), regionStart);
      address = address.replace(current_suburb, '');
      console.log("New address to search on="+address);
      addressesToImport[addressidx]['locked'] = true;
      addressesToImport[addressidx]['num_attempts'] += 1;
      t = setTimeout(function() {geocode_address(address, addressidx);}, (timeout * 3));
      timerIds.push(t);
    } else {
      display_alert("Couldn't geocode: "+address+" for the following reason: " + status, 'alert-error');    
      if (bulkimport == true) {
        addressesToImport[addressidx]['num_attempts'] = max_attempts; // Set max attempts as we've tried but encountered error
      }  
    }
  });
}

function set_map(latlng, idx) {
  console.log('set_map: '+latlng+', addidx: '+idx);
  var boundariesArray = mapBoundariesArray;
  var json = $.parseJSON($('#map_boundaries').val());
  if (latlng != null) {
    $.each(boundariesArray, function(i, polygon) {
      if (polygon.containsLatLng(latlng)) {
        // Found the map!
        addressesToImport[idx]['map_number'] = json.names[i].toString();
        alert('The map is: '+addressesToImport[idx]['map_number']);
      }
    });
  }
}

function deleteOverlays() {
  if (markersArray) {
    for (i in markersArray) {
      markersArray[i].setMap(null);
    }
    markersArray.length = 0;
  }
}

function create_address_string() {
  var region = 'Auckland';
  var country = 'New Zealand';
  var unitnumber = null;
  var unitletter = null;
  var streetnumber = null;
  var streetletter = null;
  var streetname = null;
  var streettype = null;
  var suburb = null;
  if (arguments.length > 0) {
    // this is being accessed from '/uploadjson'
    var address = arguments[0];
    streetletter = address.street_letter; 
    streetname = address.street_name; 
    streetnumber = address.street_number; 
    streettype = address.street_type; 
    suburb = address.suburb; 
    unitletter = address.unit_letter; 
    unitnumber = address.unit_number;
  } else {
    region = $('#region').val();
    country = $('#country').val();
    unitnumber = $("#unitnumber").val();
    unitletter = $("#unitletter").val();
    streetnumber = $("#streetnumber").val();
    streetletter = $("#streetletter").val();
    streetname = $("#streetname").val();
    streettype = $("#streettype").val();
    suburb = $("#suburb").val();
  }
  var address = "";
  //alert(unitnumber + ":" + unitletter + ":" + streetnumber + ":" + streetletter + ":" + streetname + ":" + streettype + ":" + suburb);
  if (unitnumber != "") address += unitnumber;
  if (unitletter != "") address += unitletter;
  // Add in unit separator
  if (address != "") address += "/";
  if (streetnumber != "") address += streetnumber;
  if (streetletter != "") address += streetletter;
  // Add some spacing
  address += " ";
  if (streetname != "") address += streetname + " ";
  if (streettype != "") address += streettype + ", ";
  if (suburb != "") address += suburb + ", ";
  address += region + ", " + country;
  return address;
}

function process_geocode_failure(status) {
  console.log('In process_geocode_failure');
  display_alert("Geocode was not successful for the following reason: " + status, 'alert-error');
}
 
function setMarkers(map, locations) {
     var image = new google.maps.MarkerImage('static/marker-panel.png',
     new google.maps.Size(24, 39),
     new google.maps.Point(0,0),
     new google.maps.Point(12, 39));
    /* var shadow = new google.maps.MarkerImage('marker-panel.png',
          new google.maps.Size(37, 32),
          new google.maps.Point(0,0),
          new google.maps.Point(0, 32));*/
     var shape = {
          coord: [1, 1, 1, 20, 18, 20, 18 , 1],
          type: 'poly'
     };
     for (var i = 0; i < locations.length; i++) {
          var beach = locations[i];
          var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
          var marker = new google.maps.Marker({
               position: myLatLng,
               map: map,
               //shadow: shadow,
               icon: image,
               shape: shape,
               title: beach[0],
               zIndex: beach[3]
          });
          var label = new Label({
               map: map
          });
          label.set('zIndex', 1234);
          label.bindTo('position', marker, 'position');
          label.set('text', beach[0]);
          //label.bindTo('text', marker, 'position');
     }
}

function display_boundaries(map) {
  // Display all map boundaries  
  var url = window.location.href;
  var showBoundaries = false;
  if (url.indexOf('master_map') > -1) {
    // We want to show them on the master territory map
    showBoundaries = true;
  }
  var json = $.parseJSON($('#map_boundaries').val());
  boundariesArray = mapBoundariesArray;
  $.each(json.boundariesCoords, function(boundaryCounter, boundary) {
    var boundaryCoords = [];
    $.each(boundary, function(boundaryCoordCounter, boundaryCoord) {
      boundaryCoords[boundaryCoordCounter] = new google.maps.LatLng(boundaryCoord[0], boundaryCoord[1]); 
    });
    var polygon = new google.maps.Polygon({
      paths: boundaryCoords,
      strokeColor: "#1b22fc",
      strokeOpacity: 1,
      strokeWeight: 1.5,
      fillColor: "#f1cbcd",
      fillOpacity: 0.35,
      visible: showBoundaries
      });
    boundariesArray.push(polygon);
    var coords = polygon.getBounds().getCenter();
    var mapCounter = boundaryCounter + 1; // Add 1 as loop counter starts from 0
    map_labels.push([mapCounter.toString(), coords.lat(), coords.lng(), mapCounter]);
    polygon.setMap(map);
  });
}

function display_boundary(map) {
  // Display individual map boundary
  // Large portion of code leached from above display_boundaries function
  var boundary = $('#boundary_coords').val();
  if (boundary != '') {
    boundary = boundary.split('\n');
    var boundaryCoords = [];
    var latLng;
    $.each(boundary, function(boundaryCoordCounter, boundaryCoord) {
      latLng = boundaryCoord.split(',');
      boundaryCoords[boundaryCoordCounter] = new google.maps.LatLng(parseFloat(latLng[0]), parseFloat(latLng[1])); 
    });
    origBoundary = boundaryCoords;
    var polygon = new google.maps.Polygon({
      paths: boundaryCoords,
      strokeColor: "#1b22fc",
      strokeOpacity: 1,
      strokeWeight: 1.5,
      fillColor: "#f1cbcd",
      fillOpacity: 0.35,
      visible: true,
      editable: true
      });
    polygon.setMap(map);
    map.fitBounds(polygon.getBounds());
    return polygon;
  }
}

function measureReset(map, polygon) {
  // If we have a polygon or a line, remove them from the map and set null
  if (measure.polygon) {
    measure.polygon.setMap(null);
    measure.polygon = null;
  }
  if (measure.line) {
    measure.line.setMap(null);
    measure.line = null
  }
  // This is for the edit view
  if (polygon) {
    polygon.setMap(null);
    polygon = null;
  }
  // Empty the mvcLine and mvcPolygon MVCArrays
  measure.mvcLine.clear();
  measure.mvcPolygon.clear();
  // Loop through the markers MVCArray and remove each from the map, then empty it
  measure.mvcMarkers.forEach(function(elem, index) {
    elem.setMap(null);
  });
  measure.mvcMarkers.clear();
  // Remove all the event listeners
  $.each(eventListeners, function(index, eventListener) {
    google.maps.event.removeListener(eventListener);
  });
  // Now setup the map for creating a new map/cong boundary
  clickListener = google.maps.event.addListener(map, "click", function(evt) {
    // When the map is clicked, pass the LatLng obect to the measureAdd function
    measureAdd(evt.latLng);
  });
  eventListeners.push(clickListener);
}

function measureAdd(latLng) {
  // Add a draggable marker to the map where the user clicked
  var marker = new google.maps.Marker({
    map: map,
    position: latLng,
    draggable: true,
    raiseOnDrag: false,
    title: "Drag me to change shape",
    icon: new google.maps.MarkerImage("/static/gmap-marker.png", new google.maps.Size(11, 11), new google.maps.Point(0, 0), new google.maps.Point(5, 5))
  });

  // Add this LatLng to our line and polygon MVCArrays
  // Objects added to these MVCArrays automatically update the line and polygon shapes on the map
  measure.mvcLine.push(latLng);
  measure.mvcPolygon.push(latLng);

  // Push this marker to an MVCArray
  // This way later we can loop through the array and remove them when measuring is done
  measure.mvcMarkers.push(marker);

  // Get the index position of the LatLng we just pushed into the MVCArray
  // We'll need this later to update the MVCArray if the user moves the measure vertexes
  var latLngIndex = measure.mvcLine.getLength() - 1;

  // When the measure vertex markers are dragged, update the geometry of the line and polygon by resetting the
  //     LatLng at this position
  clickListener = google.maps.event.addListener(marker, "drag", function(evt) {
    measure.mvcLine.setAt(latLngIndex, evt.latLng);
    measure.mvcPolygon.setAt(latLngIndex, evt.latLng);
  });
  eventListeners.push(clickListener);

  // If there is more than one vertex on the line
  if (measure.mvcLine.getLength() > 1) {
    // If the line hasn't been created yet
    if (!measure.line) {
      // Create the line (google.maps.Polyline)
      measure.line = new google.maps.Polyline({
          map: map,
          clickable: false,
          strokeColor: "#1b22fc",
          strokeOpacity: 1,
          strokeWeight: 1.5,
          path: measure.mvcLine
      });
    }
    // If there is more than two vertexes for a polygon
    if (measure.mvcPolygon.getLength() > 2) {
      // If the polygon hasn't been created yet
      if (!measure.polygon) {
        // Create the polygon (google.maps.Polygon)
        measure.polygon = new google.maps.Polygon({
            clickable: false,
            map: map,
            strokeColor: "#1b22fc",
            strokeOpacity: 1,
            strokeWeight: 1.5,
            fillColor: "#f1cbcd",
            fillOpacity: 0.35,
            paths: measure.mvcPolygon
        });
      }
    }
  }
}