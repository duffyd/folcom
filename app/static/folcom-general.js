$(document).ready(function() {
  // View map JS
  $('.delete-button').click(function(e) {
    e.preventDefault();
    $('#confirm_box_yes_button').attr('href', $(this).attr('href'));
    $('#confirmbox').modal('show');
  });
  
  $('.process-items-button').click(function(e) {
    e.preventDefault();
    $('#confirm_box_yes_button').click(function() {
      $('#table_listing_form').submit();
    });
    $('#confirmbox').modal('show');
  });
  
  $('.alert .close').live('click', function() {
    $(this).parent().hide();
  });
  
  $("#edit-item").validator();
  // TODO: maintainaddress macro - if they select 'Business'
  // they must enter a 'Business name'
});

// Display alert
function display_alert(alert, alertclass) {
  $('.alert').show();
  // Remove all classes
  $('.alert').removeClass();
  $('#alert').parent().addClass('alert ' + alertclass);
  $('#alert').text(alert); 
}

function toggle_checked(status) {
  $(".delete-checkbox").each(function() {
    $(this).attr("checked", status);
  })
}