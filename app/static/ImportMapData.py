# For testing: copy script to /Applications/LibreOffice.app/Contents/basis-link/share/Scripts/python
# on Mac OS X or C:\Program Files\LibreOffice 3.5\share\Scripts\python on Windows.
# C:\Program Files\OpenOffice.org 3\Basis\share\Scripts\python (for OpenOffice.org)
# LibreOffice 3 apparently uses python2.6, LibreOffice 4 - python 3.3
import glob, os, csv, time, datetime, re
try:
    import ConfigParser
except ImportError:
    import configparser as ConfigParser
from codecs import open
import uno
from com.sun.star.awt import WindowDescriptor
from com.sun.star.awt.VclWindowPeerAttribute import OK, OK_CANCEL, YES_NO, YES_NO_CANCEL, RETRY_CANCEL, DEF_OK, DEF_CANCEL, DEF_RETRY, DEF_YES, DEF_NO
from com.sun.star.awt.WindowClass import MODALTOP
from com.sun.star.beans import PropertyValue
from com.sun.star.table import BorderLine, TableBorder, CellRangeAddress

# Show a message box with the UNO based toolkit
def MessageBox(ParentWin, MsgText, MsgTitle, MsgType="messbox", MsgButtons=OK):
    # Shamelessly plagiarized from http://code.google.com/p/open-socemu/wiki/OpenOfficePythonMacro
    
    MsgType = MsgType.lower()
    
    #available msg types
    MsgTypes = ("messbox", "infobox", "errorbox", "warningbox", "querybox")
    
    if not ( MsgType in MsgTypes ):
        MsgType = "messbox"
    
    #describe window properties.
    aDescriptor = WindowDescriptor()
    aDescriptor.Type = MODALTOP
    aDescriptor.WindowServiceName = MsgType
    aDescriptor.ParentIndex = -1
    aDescriptor.Parent = ParentWin
    #aDescriptor.Bounds = Rectangle()
    aDescriptor.WindowAttributes = MsgButtons
    
    tk = ParentWin.getToolkit()
    msgbox = tk.createWindow(aDescriptor)
    
    msgbox.setMessageText(MsgText)
    if MsgTitle :
        msgbox.setCaptionText(MsgTitle)

    return msgbox.execute()

def is_number(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def get_config_file():
    config = None
    homedir = os.path.expanduser("~") # C:\\Documents and Settings\\UserName on Windows
    defaultdownloaddir = homedir + '/Downloads'
    configini = os.path.join(homedir, 'folcomuno.cfg')
    if os.path.isfile(configini):
        config = ConfigParser.SafeConfigParser()
        config.readfp(open(configini, 'r', encoding='utf-8')) # Just read file initially as in Python 3.3 errors otherwise
        try:
            downloaddir = config.get('Options', 'DownloadDir')
            if os.name == 'nt':
                # Remove extra ('\\') backslashes in Windows
                downloaddir = os.path.normpath(downloaddir)
            if not os.path.isdir(downloaddir):
                os.mkdir(downloaddir)
        except:
            try:
                config.add_section('Options')
            except ConfigParser.DuplicateSectionError:             
                pass
            config.set('Options', 'DownloadDir', defaultdownloaddir)
            configfile = open(configini, 'ab', encoding='utf-8') 
            config.write(configfile)
    else:
        config = ConfigParser.SafeConfigParser()
        config.add_section('Options')
        config.set('Options', 'DownloadDir', defaultdownloaddir)
        configfile = open(configini, 'ab', encoding='utf-8')
        config.write(configfile)
    return config
       
def importMapData(*args):
    pstyle = None
    value = None
    config = get_config_file()
    downloaddir = config.get('Options', 'DownloadDir')
    if os.name == 'nt':
        # Remove extra ('\\') backslashes in Windows
        downloaddir = os.path.normpath(downloaddir)
    doc = XSCRIPTCONTEXT.getDocument()
    context = XSCRIPTCONTEXT.getComponentContext()
    # TODO add this in: pagecount = doc.createInstance("com.sun.star.text.TextField.PageCount")
    # Should be this: doc.getRendererCount(selectedcells, PropertyValue())
    sheet = doc.CurrentController.getActiveSheet()
    trackingsheet = doc.getSheets().getByName('Tracking Record')
    sheetname = sheet.getName()
    # retrieve the pointer to the parent window
    parentwin = doc.CurrentController.Frame.ContainerWindow
    styles = doc.getStyleFamilies().getByName("PageStyles")
    if sheetname == 'Visit2x':
        pstyle = styles.getByName("PageStyle_Visit2x")
        addresscolstart = 5
    elif sheetname == 'Visit3x':
        pstyle = styles.getByName("PageStyle_Visit3x")
        addresscolstart = 6
    else:
        # Not being run from the map Calc sheet(s)
        MessageBox(parentwin, "This macro must be run from either the 'Visit2x' or 'Visit3x' sheets in the Map Template spreadsheet.", 'Folcom Import Macro')
        return
    header = pstyle.RightPageHeaderContent
    footer = pstyle.RightPageFooterContent
    # Delete all the current values (511 is ALL contents)
    sheet.getCellRangeByPosition(0, 7, 10, 400).clearContents(511)
    # Remove all manual page breaks
    sheet.removeAllManualPageBreaks()
    sheetrow = 7
    addressrowstart = 6
    
    csvfiles = glob.glob(os.path.join(downloaddir, 'Map*.csv'))
    if len(csvfiles) == 0:
        MessageBox(parentwin, "You haven't downloaded the map data. Please download the map data from within Folcom and try again.", 'Folcom Import Macro')
        return
    elif len(csvfiles) > 1:
        MessageBox(parentwin, "You have downloaded more than 1 map. Please run the deleteMapData macro, re-download the map data from within Folcom and try again.", 'Folcom Import Macro')
        return
    for match in csvfiles:
        csvfile = open(match, 'r', encoding='utf-8')
        reader = csv.reader(csvfile)
        for i, line in enumerate(reader):
            if i == 0:
                trackingsheet.getCellByPosition(3, 0).setFormula(line[0][5:]) # Map name
                #trackingsheet.getCellByPosition(2, 1).setFormula(str(pagecount)) # Page count
            elif i == 1:
                # Edit header
                trackingsheet.getCellByPosition(1, 0).setValue(int(line[0][12:])) # Map number
                header.getCenterText().setString('Map '+line[0][12:])
                pstyle.RightPageHeaderContent = header
            elif i == 2:
                assignee = line[0][16:]
                assigneeparts = assignee.split()
                if len(assigneeparts) == 2:
                    # First name + first initial of surname
                    assignee = assigneeparts[0] + ' ' + assigneeparts[1][:1]
                if sheetname == 'Visit2x':
                    sheet.getCellByPosition(7, 0).setFormula(assignee)
                elif sheetname == 'Visit3x':
                    sheet.getCellByPosition(8, 0).setFormula(assignee)
            elif i == 3:
                duedate = line[0][13:]
                if sheetname == 'Visit2x':
                    sheet.getCellByPosition(9, 0).setFormula("'"+duedate)
                elif sheetname == 'Visit3x':
                    sheet.getCellByPosition(10, 0).setFormula("'"+duedate)
            elif i == 4:
                # Edit footer
                footer.getRightText().setString('Round '+line[0][14:])
                pstyle.RightPageFooterContent = footer
            elif i >= addressrowstart:
                if len(line) > 0:
                    currentcol = addresscolstart
                    for addrcolidx in range(8):
                        if addrcolidx == 0:
                            sheet.getCellByPosition(0, sheetrow).setFormula(line[addrcolidx])
                        elif addrcolidx == 7:
                            sheet.getCellByPosition(currentcol+1, sheetrow).setValue(float(line[addrcolidx])) # sort order
                        else:
                            if is_number(line[addrcolidx]):
                                value = "'"+line[addrcolidx]
                            else:
                                value = line[addrcolidx]
                            sheet.getCellByPosition(currentcol, sheetrow).setFormula(value)
                            currentcol = currentcol + 1
                    sheetrow = sheetrow + 1
        # Setup the print area
        sheetrow = sheetrow - 1 # Need to take off 1 as code above adds 1 after final row
        selectedcells = sheet.getCellRangeByPosition(0, 0, addresscolstart+5, sheetrow)
        rangeaddress = selectedcells.getRangeAddress()
        bordercells = sheet.getCellRangeByPosition(0, 7, addresscolstart+5, sheetrow)

        topline = BorderLine()
        topline.Color = 0
        topline.InnerLineWidth = 0
        topline.OuterLineWidth = 10
        topline.LineDistance = 0
        bottomline = BorderLine()
        bottomline.Color = 0
        bottomline.InnerLineWidth = 0
        bottomline.OuterLineWidth = 10
        bottomline.LineDistance = 0
        leftline = BorderLine()
        leftline.Color = 0
        leftline.InnerLineWidth = 0
        leftline.OuterLineWidth = 10
        leftline.LineDistance = 0
        rightline = BorderLine()
        rightline.Color = 0
        rightline.InnerLineWidth = 0
        rightline.OuterLineWidth = 10
        rightline.LineDistance = 0
        horizontalline = BorderLine()
        horizontalline.Color = 0
        horizontalline.InnerLineWidth = 0
        horizontalline.OuterLineWidth = 10
        horizontalline.LineDistance = 0
        verticalline = BorderLine()
        verticalline.Color = 0
        verticalline.InnerLineWidth = 0
        verticalline.OuterLineWidth = 10
        verticalline.LineDistance = 0
        
        tableborder = TableBorder()
        tableborder.IsTopLineValid = True 
        tableborder.IsBottomLineValid = True 
        tableborder.IsLeftLineValid = True 
        tableborder.IsRightLineValid = True 
        tableborder.IsHorizontalLineValid = True
        tableborder.IsVerticalLineValid = True
        tableborder.IsDistanceValid = True 
        tableborder.TopLine = topline 
        tableborder.BottomLine = bottomline 
        tableborder.LeftLine = leftline 
        tableborder.RightLine = rightline 
        tableborder.HorizontalLine = horizontalline 
        tableborder.VerticalLine = verticalline
        tableborder.Distance = 0
        
        bordercells.TableBorder = tableborder
        
        # Setting up title rows as can't seem
        # to save this in file.
        titlerow = CellRangeAddress()
        titlerow.StartRow = 0
        titlerow.EndRow = 6
        sheet.setTitleRows(titlerow)
        
        sheet.setPrintAreas((rangeaddress,))
        csvfile.close()
                
def deleteMapData(*args):
    numdeleted = 0
    doc = XSCRIPTCONTEXT.getDocument()
    sheet = doc.CurrentController.getActiveSheet()
    # retrieve the pointer to the parent window
    parentwin = doc.CurrentController.Frame.ContainerWindow
    config = get_config_file()
    downloaddir = config.get('Options', 'DownloadDir')
    if os.name == 'nt':
        # Remove extra ('\\') backslashes in Windows
        downloaddir = os.path.normpath(downloaddir)
    for match in glob.glob(os.path.join(downloaddir, 'Map*.csv')):
        os.remove(match)
        numdeleted = numdeleted + 1
    if numdeleted > 0:
        MessageBox(parentwin, "Successfully deleted "+str(numdeleted)+" map file(s).", 'Folcom Import Macro', "infobox")
    else:
        MessageBox(parentwin, "There are no map files to be deleted.", 'Folcom Import Macro', "infobox")

g_exportedScripts = importMapData,deleteMapData

#import unohelper
#g_ImplementationHelper = unohelper.ImplementationHelper()
#g_ImplementationHelper.addImplementation( \
#    None, "org.openoffice.script.FolcomImportMapDataImplementation", \
#    ("org.openoffice.script.FolcomImportMapDataService",),)