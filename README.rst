Foreign Language Contact Manager - web app to manage foreign language territory for Jehovah's Witness foreign language groups/congregations.

The app can be seen in action at ``http://folcom.net``.