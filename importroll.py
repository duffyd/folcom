import glob
import os
import re
import sys
import json
from app.config import STREETTYPES, DEFAULTREGION, CHINESESURNAMES

ROLLFILESDIR = '/Users/tim/Documents/theo/Elders stuff/serviceoverseer/newaddresses/Epsom/ocrs/'
#ROLLFILESDIR = '/Users/tim/Documents/theo/Elders stuff/serviceoverseer/newaddresses/Page 001-350 txt/'

def is_number(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

def get_street_suburb(streetnameparts):
    # This is habitation index-specific
    regionidx = None
    suburb = ''
    streetnameparts = streetnameparts.split(',')
    for i, part in enumerate(streetnameparts):
        if part.find(DEFAULTREGION) > -1:
            regionidx = i
            break
    suburb = streetnameparts[regionidx-1].strip()
    streetnametype = streetnameparts[regionidx-2].strip()
    streetname, streettype = get_street_type(streetnametype)
    return streetname, streettype, suburb
    
def get_street_type(streetnametype):
    # Expects something like: Aaronville Way
    streettypeidx = None
    streetname = ''
    streettype = ''
    for stype in STREETTYPES:
        regex = re.compile(r'\b%s\b$' % stype)
        if regex.search(streetnametype):
            streettypeidx = regex.search(streetnametype).start()
            streettype = streetnametype[streettypeidx:]
            streetname = streetnametype[:streettypeidx]
            break
    if streettype == '':
        streetname = streetnametype
    return streetname.strip(), streettype

def get_street_number(line):
    # This assumes is in the following format if a unit letter/number
    # is present (note the gap): 1/ 82
    streetletter = ''
    streetnumber = ''
    unitletter = ''
    unitnumber = ''
    for word in line.split():
        word = word.strip()
        if word.find('/') > -1:
            # It is a unit number/letter
            slashidx = word.find('/')
            unitnumber = word[:slashidx]
            if not is_number(unitnumber[-1:]):
                unitletter = unitnumber[-1:].upper()
                unitnumber = unitnumber[:-1]
        else:
            if is_number(word):
                # It is a street number
                if streetnumber != '':
                    streetnumber += word
                else:
                    streetnumber = word
            elif is_number(word[:-1]): # Does address have a street letter?
                streetletter = word[-1:].upper()
                streetnumber = word[:-1]
    return unitnumber, unitletter, streetnumber, streetletter

def has_street_number(line):
    # Don't need to use this for electoral roll as always has number
    lineparts = line.strip().split()
    #print "has_street_number lineparts:%s" % ':'.join(lineparts)
    if len(lineparts) > 0:
        # Just check the first item as that is where number should be
        if lineparts[0].find('/') > -1 or \
            is_number(lineparts[0]) or \
            is_number(lineparts[0][:-1]):
            # This is a unit number (contains slash) or is number or
            # is a number with street letter
            return True
    return False

def get_surname(line):
    # Can reuse for electoral roll
    surname = ''
    firstnames = ''
    words = line.split()
    for i, word in enumerate(words):
        if word.find(',') > -1:
            commaidx = word.find(',')
            surname = word[:commaidx]
            if not i == 0:
                if not is_number(words[i-1]):
                    surname = "%s %s" % (words[i-1], surname)
            firstnames = ' '.join(words[i+1:len(words)])
    return surname.lower(), firstnames.lower()

def write_out_file(filecounter, data):
    jsonfiledata = json.dumps(data, sort_keys=True, indent=2)
    filename = '/addresses%s.json' % filecounter
    jsonfile = open(sys.path[0]+filename, 'w')
    jsonfile.write(jsonfiledata)
    jsonfile.close()
    data = [] # reset the data
    filecounter += 1
    return filecounter, data

def main():
    # TODO: remember that O46 in page2.txt needs to be changed to 046
    # page4.txt AMY, Lin -> LIN, Amy
    "Usage: importroll eroll|hindex"
    import sys
    electoral_roll = False
    if len(sys.argv) == 2:
        if sys.argv[1] == 'eroll': # allows us to specify if we're working on an electoral roll
            electoral_roll = True
        elif sys.argv[1] != 'hindex':
            print "Usage: importroll eroll|hindex"
            sys.exit()
    else:
        print "Usage: importroll eroll|hindex"
        sys.exit()
    addresslinestart = 2 # by default text starts from line 3
    filecounter = 1
    streetletter = ''
    streetnumber = ''
    unitletter = ''
    unitnumber = ''
    lastunitnumber = ''
    lastunitletter = ''
    laststreetnumber = ''
    laststreetletter = ''
    laststreetname = ''
    laststreettype = ''
    lastsuburb = ''
    comparestring = ":%s:%s:%s:%s:%s:%s:%s:"
    addressescollected = []
    numcollected = 0
    searching_for_address = False
    for match in glob.glob(os.path.join(ROLLFILESDIR, '*.txt')):
#    for match in glob.glob(os.path.join(ROLLFILESDIR, 'Page 001.txt')):
        print '***** Filename = %s' % match
        rollfile = open(match, 'rU')
        for i, line in enumerate(rollfile):
            line = line.rstrip('\n') # Get rid of the newline character
            if electoral_roll:
                if line.find('001') > -1:
                        addresslinestart = i
            if i >= addresslinestart:
                if electoral_roll:
                    suburb = 'Epsom' # this has to be hard-coded
                    # Importing electoral roll
                    address_counter_idx = None
                    search_str = line
                    if searching_for_address:
                        # We've already found the foreign surname, firstnames and am now looking for the address
                        found_address = re.search(r'[a-zA-Z0-9]', line)
                        if found_address:
                            address = line[found_address.start():line.find(',', found_address.start())]
                            print "*** Foreign language householder's address: %s" % address
                            address = address.split()
                            address_number = address.pop(0)
                            if address_number.find('/') > -1:
                                # Has a unit number/letter
                                # Need to insert a space as that is what get_street_number expexts (habitation index-related)
                                address_number = "%s %s" % (address_number[:address_number.find('/')+1], address_number[address_number.find('/')+1:])
                                #print "*** address_number: %s" % address_number
                            unitnumber, unitletter, streetnumber, streetletter = get_street_number(address_number)
                            #print "*** address: %s" % ' '.join(address)
                            streetname, streettype = get_street_type(' '.join(address)) # TODO: thinks Westbourne is a streettype. Need to ensure it doesn't finds words within street names (need to be separate words)
                            searching_for_address = False
                            print '*** linenumber=%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:' % (i, surname, firstnames, unitnumber, unitletter, streetnumber, streetletter, streetname, streettype, suburb)
                            addressescollected.append(dict(surname = surname,
                                                           firstnames = firstnames,
                                                           unit_number = unitnumber,
                                                           unit_letter = unitletter,
                                                           street_number = streetnumber,
                                                           street_letter = streetletter,
                                                           street_name = streetname,
                                                           street_type = streettype,
                                                           suburb = suburb,
                                                           map_number = '',
                                                           cong_territory = '',
                                                           )
                                                      )
                            numcollected += 1
                            if numcollected % 500 == 0:
                                # We've reached our 500 address limit
                                filecounter, addressescollected = write_out_file(filecounter, addressescollected)
                    # Find the address counter number, i.e. 001-200
                    while True:
                        has_address_num = re.search(r'([0-2][0-9][0-9])+', search_str)
                        if has_address_num:
                            if int(has_address_num.group()) > 200:
                                # Can't be larger than 200, otherwise it's a street address
                                search_str = search_str[has_address_num.end():] # continue searching the rest of the string
                                continue
                            #print "Our surname?: %s" % search_str[has_address_num.end()+1:search_str.find(',', has_address_num.end()+1)]
                            if search_str[has_address_num.end()+1:search_str.find(',', has_address_num.end()+1)].isupper():
                                # The counter number must be followed by a surname in uppercase
                                address_counter_idx = has_address_num.start()
                                #print 'This is what we found: %s' % has_address_num.group()
                                break
                            else:
                                search_str = search_str[has_address_num.end():] # continue searching the rest of the string
                                continue
                        else:
                            # If we can't find address counter number in string exit
                            break
                    if not address_counter_idx is None:
                        surname_end_idx = line.find(',', address_counter_idx+4)
                        names = line[address_counter_idx+4:line.find('.', surname_end_idx+2)]
                        surname, firstnames = get_surname(names)
                        if surname in CHINESESURNAMES:
                            # The householder speaks the language of our group/cong!!'
                            #print "*** Foreign language householder's address: %s" % line[line.find(names)+len(names):]
                            print "surname: %s AND firstnames: %s" % (surname, firstnames)
                            searching_for_address = True
                else:
                    # Importing habitation index
                    if line.find(DEFAULTREGION) > -1: # does this line contain a street/suburb?
                        streetname, streettype, suburb = get_street_suburb(line) # E.g. Aaronville Way, East Tamaki Heights, Auckland 4 GILDENHUYS, Anna Sophia
                        regionidx = line.find(DEFAULTREGION)
                        line = line[regionidx+len(DEFAULTREGION):].strip() # get rid of streetname/suburb/region
                    if has_street_number(line):
                        unitnumber, unitletter, streetnumber, streetletter = get_street_number(line)
                    surname, firstnames = get_surname(line)
                    if surname in CHINESESURNAMES:
                        # The householder speaks the language of our group/cong!!
                        if comparestring % (unitnumber, unitletter, streetnumber, streetletter, streetname, streettype, suburb) != comparestring % (lastunitnumber, lastunitletter, laststreetnumber, laststreetletter, laststreetname, laststreettype, lastsuburb) or \
                            comparestring % (unitnumber, unitletter, streetnumber, streetletter, streetname, streettype, suburb) == comparestring % ('', '', '', '', '', '', ''):
                            # Ensure we don't record the same address twice and allow the first-time recording of the data
                            print 'linenumber=%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:' % (i, surname, firstnames, unitnumber, unitletter, streetnumber, streetletter, streetname, streettype, suburb)
                            addressescollected.append(dict(surname = surname,
                                                           firstnames = firstnames,
                                                           unit_number = unitnumber,
                                                           unit_letter = unitletter,
                                                           street_number = streetnumber,
                                                           street_letter = streetletter,
                                                           street_name = streetname,
                                                           street_type = streettype,
                                                           suburb = suburb,
                                                           map_number = '',
                                                           cong_territory = '',
                                                           )
                                                      )
                            lastunitnumber = unitnumber
                            lastunitletter = unitletter
                            laststreetnumber = streetnumber
                            laststreetletter = streetletter
                            laststreetname = streetname
                            laststreettype = streettype
                            lastsuburb = suburb
                            numcollected += 1
                            if numcollected % 500 == 0:
                                # We've reached our 500 address limit
                                filecounter, addressescollected = write_out_file(filecounter, addressescollected)
    # Write out remaining data to file (as last amount wasn't exactly 500 lines)
    filecounter, addressescollected = write_out_file(filecounter, addressescollected)
    print "Number of addresses collected: %s" % numcollected

if __name__ == '__main__':
    main()