Print Map
=========

The instructions below will guide you through printing a territory map from Folcom. The instructions assume
you've already installed `LibreOffice <https://www.libreoffice.org/>`_.

Downloading Map Data
--------------------

1. If you have downloaded map data previously, you will need to delete the existing map data by following the
   instructions under *Deleting Map Data* below.
2. Now *check out* the map you wish to print by going to `Manage Maps <https://folcom.net/view_map>`_
   and then select the relevant map in the **Map** dropdown menu and click the **View** button.
3. Select the person you are checking the map out to in the **Assigned to** dropdown menu. Finally click the **Check out** button.
4. Click the **Download** button, which will download the map data to your computer (just download the map data to
   your default *Downloads* folder, usually ``C:\Documents and Settings\UserName\Downloads`` on Windows).

Printing Map
------------

1. In order to print a map, you will need to download the Map Template from `here <https://folcom.net/downloads/MapTemplate.ods>`_
   and save it somewhere, e.g. *My Documents*.
2. Within LibreOffice, click on the **Tools** menu and then click **Options**. Expand the **LibreOffice** heading and select **Security**.
   When in security, click the **Macro Security** button and then choose **Low** as your setting.
3. Now open up the Map Template and click on either the **Visit2x** or the **Visit3x** sheet tab at the bottom, depending on whether you
   want to print a map where publishers have to visit each not-at-home address 2 times or 3 times. If you can't see the sheet tabs, just
   click the arrows at the bottom left of the screen until they appear.
4. Click on the **Tools** menu, then click **Macros**, and **Run Macro...**. If you get a message regarding
   having to install a Java runtime environment (JRE), ignore this (a JRE isn't required to run our import map data macro) and click the 
   **OK** button.
5. From the *Macro Selector* window, click the plus sign (+) next to **Map Template.ods**, then click on **ImportMapData**, and then
   click on **importMapData** in the *Macro name* column on the right-hand side. Finally, click the **Run** button. The macro can take
   some time to run, so please be patient as it processes the map data and imports it into the spreadsheet. Once the import has completed
   a message box will appear indicating that the import was successful. If you would like to add a button to the toolbar that runs the
   importMapData macro, please follow the instructions `here <https://help.libreoffice.org/Common/Adding_Buttons_to_Toolbars>`__.
6. Finally, print the Tracking Record page and the Visit2x or Visit3x page, depending on which sheet tab you imported the map data into.
   
Deleting Map Data
-----------------

1. To delete any existing map data, open up the Map Template (you must open up the Map Template as the macros are contained within the
   file) and click on the **Tools** menu, then click **Macros**, and **Run Macro...**. If you get a message regarding having to install
   a Java runtime environment (JRE), ignore this (a JRE isn't required to run our delete map data macro) and click the **OK** button.
2. From the *Macro Selector* window, click the plus sign (+) next to **Map Template.ods**, then click on **ImportMapData**, and then
   click on **deleteMapData** in the *Macro name* column on the right-hand side. Finally, click the **Run** button. If you would like
   to add a button to the toolbar that runs the deleteMapData macro, please follow the instructions `here <https://help.libreoffice.org/Common/Adding_Buttons_to_Toolbars>`__. 