Folcom Overview
===============

Folcom has an administrative view where the territory servant and any helpers can manage the foreign maps and addresses in the system, a group conductor view where field service group conductors can assign houses to publishers to do, and a publisher view which is used when engaging in the house-to-house ministry. The maps can also be printed out as another option if congregation members don't have mobile devices. 
The benefits of using the online version are:

* Any updates are live instantaneously
* Publishers can directly add any new addresses they come across
* Map pages don't go missing as the system tracks who has territory and automatically reminds them to check back in their map page
* The system keeps track of how many houses are left to do on the map.

All these benefits make the territory department's job a lot easier. Something to be wary of, though, is that someone in each car group must have mobile internet access in order to use the system.