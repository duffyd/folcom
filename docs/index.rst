.. Folcom documentation master file, created by
   sphinx-quickstart on Tue Jul 17 22:35:00 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Folcom's documentation!
==================================

This site provides documentation for `Folcom <http://folcom.net>`_, or the *Foreign Language Contact Manager*,
a web application to manage foreign language territory for `Jehovah's Witness <http://www.jw.org>`_
foreign language groups and congregations.

Contents:

.. toctree::
   :maxdepth: 2

   overview
   using_in_ministry
   add_address
   edit_address
   manage_maps
   print_map
   import_addresses

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

