Add Address
===========

**A video tutorial regarding how to add an address can be viewed here:** http://youtu.be/c28pVAhOxj8

1. Folcom separates the address into unit number, unit letter, street number, street letter. An example of this is:

   ``1A/2B Good Street, Good Suburb``
   
   where *1* is the **unit number**, *A* is the **unit letter**, *2* is the **street number** and *B* is the **street letter**.
   Enter the address details into the relevant fields. All fields marked with an *** (asterisk)** are mandatory.
2. After entering the address details, click the **Lookup Map**
   button. This will populate the *Map* field. It will also lookup the address in Google
   Maps (i.e. the map on the right-hand side). If there is an error in looking up the Map, you will need
   to specify the map manually.
3. Click the **Add** button.
4. Folcom will now try and automatically sort the address in the map. If the following message appears:
   **Address created successfully. Couldn't automatically set sort order so please click on 'View Map' button
   below to set sort order**, click on the **View Map** button and manually specify the
   sort order for the address by following the :doc:`manage_maps` instructions.