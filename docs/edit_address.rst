Edit Address
============

1. Explain that they only need to enter the street number/letter (and unit number/letter if applicable)
   and the street name to lookup the address. Also, that they can enter abbreviations for street types,
   e.g. ``Rd, St, Ave, etc`` (if in doubt, enter the whole word, e.g. ``Road, Street, Avenue``).
2. One thing that would also be good to mention is that some addresses that have a *letter* in the address
   may, for example, be in the form: *A/1 Cool St*. So if they can't find the address doing a search on
   ``1A Cool St`` they could try searching on ``A/1 Cool St``.
3. Explain that they can update any of the values in any of the fields and then click the **Update** button.
4. There is also a **View Map** button next to the **Map** dropdown menu. When clicked, this will take you
   to the map that you've assigned the map to.
5. Explain what the **Other Language** button does (and the steps you need to undertake), i.e. it allows
   them to remove this address from our maps and assign it to a foreign language group/congregation to
   follow up.
6. Explain what the **Archive** button does (and screenshots/etc), i.e. it removes address from our system
   and puts it in a *recycle bin* so the territory servant can restore it again if there was a mistake
   made/etc. So no need for them to stress out that they'll accidentally delete something!