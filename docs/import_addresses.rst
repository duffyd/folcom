Import Addresses
================

The instructions below assume you have already created maps within Folcom.

1. Download the Excel Map Template file from `here <https://folcom.net/downloads/MapTemplate.xlsx>`_.
2. Begin entering addresses into the spreadsheet file for a particular map within Folcom. You can enter
   addresses into either the *Visit2x* or *Visit3x* tab - it doesn't really matter which tab you choose.
3. Enter the unit number and/or unit letter into the **Unit** column in the spreadsheet, and the street
   number and/or street letter into the **Hse** column (for further information regarding the way Folcom
   treats addresses, refer to :doc:`/add_address`). Street name is entered into the **Street** column
   and the suburb into the **Suburb** column. Please enter into the **Language** column one of the values
   that is entered into the **Languages** field in Folcom's `General settings <https://folcom.net/admin>`_.
   Nothing needs to be entered into any of the other columns in the spreadsheet.
4. Once you've finished entering the map addresses into the spreadsheet, save the spreadsheet as a CSV
   file (this is usually done by selecting *File* -> *Save As* -> *CSV*). If you are prompted to save
   the data in the active sheet, select *Yes*.
5. Go to the `Import addresses <https://folcom.net/upload_csv>`_ page and click the **Choose file** button and
   select the CSV file you saved in step 4.
6. If you entered the addresses into the *Visit2x* tab in the spreadsheet you need to specify **2 Visits** in the 
   **2 or 3 Visits?** dropdown, and if you entered them into the *Visit3x* tab you need to specify **3 Visits**.
7. Select the map you wish to import the addresses into in the **Map Name** dropdown menu.
8. Click the **Check for duplicate addresses** check-box to ensure you don’t have double-ups of addresses (N.B.
   if you're setting up Folcom for the first time, you could leave this unchecked).
9. Click the **Upload** button to begin importing the addresses into Folcom.