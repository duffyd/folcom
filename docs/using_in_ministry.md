Using in the Door-to-Door Ministry
==================================

The process is as follows:

1. Group conductor logs in to [http://folcom.net](http://folcom.net) using his regular email address as username and his assigned password.

2. Group conductor is presented with a **My Maps** page, which lists all the territory currently checked out to them. Clicking on any of the map names will open up the **Assigned Pages** page.

3. From the **Assigned Pages** page, the group conductor types in the email address of the publisher they're assigning the map page to and also the number of houses they will receive (it defaults to *25*). Then the group conductor clicks the **Assign** button.

4. After clicking the **Assign** button an email is sent to the publisher's email address with a link to the map page. The publisher then clicks on the link and is presented with the map page, which lists out the addresses and 3 options on the left-hand side.

    * When a house is called on the 1st time, the options are: **Done**, **Not Home**, **Remove**. The **Remove** option is selected if the householder doesn't speak the target language of the group/congregation or does not wish to be called on.
    * When a house is called on the 2nd time, the options are: **Done**, **Left Tract**, **Remove**.

    The publisher clicks on the appropriate option as they do the houses and when they're finished they click on the **Check In** button. If they want to continue working the map at a later time, they click on the **Save** button. They can then re-open up the map page by clicking on the link in the email at a later stage. There is also an **Add Address** button, which they can click on if they want to add a new address. When the publisher clicks on the **Add Address** button, the map page is automatically saved before presenting the Add Address screen.

5. When the publisher clicks on the **Check In** button, the map page is completed and any not-at-homes are available to be worked a second time. If the publisher doesn't check back in their territory, an automated reminder is sent out each Sunday asking them to check it back in.