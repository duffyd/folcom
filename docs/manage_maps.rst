Manage Maps
===========

1. Just briefly step them through viewing a map and mention that they can edit an address from here as well
   as archive an address (i.e. the rubbish bin icon).
2. Explain how to use the **Sort Order** feature. Give them a real-life example as follows: the street name
   doesn't currently exist in the territory so they need to work out the sort order by opening up `Google
   Maps <http://maps.google.com>`_ and typing the street name in there and looking for nearby streets that *are*
   already in the map and then entering a sort number that is higher than that so that the street is in
   the proper order. You'll then need to explain clearly how they enter the sort order, i.e. type the
   number in sort order field and click the **Sort** button.