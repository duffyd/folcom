# This script assumes that graphicsmagick and tesseract are installed
import glob
import os
import re
import subprocess
import sys

ROLLFILESDIR = '/Users/tim/Documents/theo/Elders stuff/serviceoverseer/newaddresses/Epsom/ocrs/'
STRINGS_IN_ROLL = ['EPSOM', 'PAGE', 'Composite', 'EPSQM', 'EPEQH', 'PACE']

def is_readable(line):
    readable = False
    for pattern in STRINGS_IN_ROLL:
        if line.find(pattern) > -1:
            readable = True
            break
    return readable

def main():
    roll_files_dir = ROLLFILESDIR
    image_file_ext = 'tif'
    if len(sys.argv) >= 2:
        roll_files_dir = sys.argv[1]
    if len(sys.argv) == 3:
        image_file_ext = sys.argv[2]
    filecounter = 1
    for match in glob.glob(os.path.join(roll_files_dir, 'img*.'+image_file_ext)):
        rotated_degrees = 0
        print '***** Filename = %s' % match
        readable = False
        ocr_outfile = 'page%s' % filecounter
        while not readable:
            ocr_result = subprocess.Popen(['tesseract',
                                           match,
                                           ocr_outfile,
                                           '-psm',
                                           '1'],
                                          cwd=roll_files_dir,
                                          stdout=subprocess.PIPE,
                                          stderr=subprocess.PIPE).communicate()
            # Check if the ocred file is readable
            readable = is_readable(open(roll_files_dir+ocr_outfile+'.txt').readline())
            if readable:
                print '***** Successfully ocred %s' % match
            else:
                if rotated_degrees >= 360:
                    # Have we already rotated the image
                    # a full 360 degrees?
                    print "***** Couldn't ocr the following file: %s " % match
                    break
                # Rotate the image -90 degrees and try again
                rotated_file = ocr_outfile+'.tif'
                print "This is the file we'll rotate: %s" % match[len(roll_files_dir):]
                rotate_result = subprocess.Popen(['gm',
                                                  'convert',
                                                  match[len(roll_files_dir):],
                                                  '-rotate',
                                                  '-90',
                                                  #'-path',
                                                  #ROLLFILESDIR,
                                                  rotated_file],
                                                 cwd=roll_files_dir,
                                                 stdout=subprocess.PIPE,
                                                 stderr=subprocess.PIPE).communicate()
                match = rotated_file
                print '***** Successfully rotated %s' % match
                rotated_degrees += 90
        filecounter += 1
        
if __name__ == '__main__':
    main()
